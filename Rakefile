require "time"

task :default => :favicon do
  sh "nanoc"
end

icon_src = "content/icon.svg"
[32, 64, 128, 256].map do |s|
  filename = "content/icon-#{s}.png"
  file(filename => icon_src) do |t|
    sh 'inkscape', "--export-filename=#{t.name}", "--export-type=png", "--export-width=#{s}", icon_src
  end
  task :favicon => filename
end
file 'content/favicon.ico' => 'content/icon-32.png' do |t|
  sh "convert", "content/icon-32.png", t.name
end
task :favicon => 'content/favicon.ico'

template = <<-TEMPLATE
---
title: "%{title}"
created_at: "%{timestamp}"
kind: article
tags: []
---
TEMPLATE


desc 'Adds new blog post'
task :new do
  require "readline"
  title = Readline.readline("Post title: ")
  slug = title.gsub(/\W+/, "-").downcase
  filename = "content/posts/#{slug}.md"
  time = Time.now + 3600 # 1h from now
  File.open(filename, "w") do |f|
    f.write(template % {
      title: title,
      timestamp: time.strftime("%y-%m-%d %H:%S " + time.zone.to_i.to_s)
    })
  end
  exec ENV["EDITOR"], filename
end

desc "Runs a local server"
task :server do
  sh 'nanoc', 'view', '--live-reload'
end
