---
title: Mentoring
---

# Mentoring

This is a list of people I have worked with in different mentoring programs.

## 2021

[Pavit Kaur](https://pavitkaur05.github.io/) -
[Debian Continuous Integration improvements](https://wiki.debian.org/SummerOfCode2021/ApprovedProjects/DebianCI).
Google Summer of Code.

[Abiola Ajadi](https://ajadi-abiola.github.io/) -
[Debian Continuous Integration improvements](https://salsa.debian.org/ci-team/debci/-/milestones/1).
Outreachy, December 2021 round.


## 2020

[Utkarsh Gupta](https://utkarsh2102.com/) -
[Upstream/Downstream cooperation in Ruby](https://wiki.debian.org/SummerOfCode2020/ApprovedProjects/UpstreamDownstreamCooperationInRuby).
Google Summer of Code.

## 2019

[Candy Tsai](https://stringpiggy.hpd.io/) -
[Debian Continuous Integration: user experience improvements](https://wiki.debian.org/SummerOfCode2019/ApprovedProjects/DebianContinuousIntegration).
Outreachy.

## 2016

[Lucas Moura](https://github.com/lucasmoura) -
[Improving and extending AppRecommender](https://wiki.debian.org/SummerOfCode2016/StudentApplications/LucasMoura).
Google Summer of Code.

[Luciano Prestes Cavalcanti](https://github.com/LucianoPC) -
[Improving and extending AppRecommender](https://wiki.debian.org/SummerOfCode2016/StudentApplications/LucianoPrestes).
Google Summer of Code.

## 2015

[Thiago Ribeiro](https://github.com/thiagovsk) -
[Automated configuration of packaged web applications](https://wiki.debian.org/SummerOfCode2015/StudentApplications/ThiagoRibeiro).
Google Summer of Code.

## 2014

[Brandon Fairchild](http://nerith.github.io/) -
[Debian Continuous Integration - Web Interface](https://wiki.debian.org/SummerOfCode2014/StudentApplications/BrandonF).
Google Summer of Code.

[Lucas Kanashiro](https://twitter.com/lucas_kanashiro) -
[Debian Continuous Integration](https://wiki.debian.org/SummerOfCode2014/StudentApplications/LucasKanashiro).
Google Summer of Code.
