---
title: 'toda piada tem um fundo de verdade'
created_at: "2007-03-18 21:32 -3"
kind: article
old: true
---

> [...]
> assim como o curso superior serve para dar direito a cela especial, o
> doutorado tem uma serventia muito importante: quando, em uma discussão,
> alguém lhe impuser sua posição dizendo "Eu sou doutor!", você pode responder
> "Grande coisa! Eu também sou!".

Raul Sidnei Wazlawick. [*Como fazer uma Dissertação de Mestrado em Informática na Educação: Uma Análise Reflexiva sobre a Ironia do Processo*](http://www.inf.ufsc.br/~raul/arquivos/comofazerumadissertacao.pdf).

Muito bom. :-)
