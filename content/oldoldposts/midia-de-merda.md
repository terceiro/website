---
title: 'mídia de merda'
created_at: "2005-09-13 23:51 -3"
kind: article
old: true
---

["PSL quer derrubar Hélio Costa"](http://www.infomediatv.com.br/visualiza_video.php?id_video=88),
da [Infomedia TV](http://www.infomediatv.com.br/)

Não vou fazer qualquer juízo de valor sobre a tal petição. Esse não é o ponto.

Vejo duas opções:

1.  **Matéria mal-intencionada.** Não me parece distante do linha editorial da Infomedia, sempre tentando desqualificar o debate político e social sobre o software livre e super-valorizar o "mercado", mas não acredito que seja o caso.
2.  **Jornalismo mal-feito**, com termos de impacto catados numa *hotlist*, pouca informação e nenhum, *nenhum* contato com o objeto da matéria.

Por enquanto estou na *2* ...  não fosse o caso saberia-se que PSL-Brasil nunca
foi e não é uma entidade formal e não quer nem pede nada, pois não há fórum de
deliberação. Aliás, parece difícil pra imprensa *mainstream* compreender a
dinâmica de movimentos sociais que não se enquadram no padrão
sindicato/associação de moradores/partido político.

*Cada vez mais*™ eu me surpreendo com a (falta de) conhecimento com que
"jornalistas" escrevem sobre as coisas. Parece que jornalismo se tornou *a arte
de escrever sobre o que não se sabe*. Não importa a realidade, importa o que é
interessante de se publicar.

Que me perdoem os bons jornalistas (você aí conhece algum? Eu conheço poucos...).
