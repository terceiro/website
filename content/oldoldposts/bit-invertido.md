---
title: 'bit invertido'
created_at: "2007-12-01 20:11 -3"
kind: article
old: true
---

Sempre me disseram que era possível que um bit se invertesse do nada num banco de memória ou num disco rígido, mas eu nunca tinha visto acontecer. Hoje eu vi.

Eu estava trabalhando no [noosfero](http://www.colivre.coop.br/Noosfero)
quando de repente, não mais que de repente, ao rodar um teste de integração o
Ruby começou a dar um erro de sintaxe num arquivo do
[Rails](http://www.rubyonrails.org/):

```
terceiro@sede:~/src/noosfero$ rake test:integration
(in /home/terceiro/src/noosfero)
rake aborted!
/home/terceiro/src/noosfero/config/../vendor/rails/actionmailer/lib/action_mailer/vendor/text/format.rb:1456: syntax error, unexpected ',', expecting '\n' or ';'
          def hyphenate_to8word, size, formatter)
                                ^
/home/terceiro/src/noosfero/config/../vendor/rails/actionmailer/lib/action_mailer/vendor/text/format.rb:1456: syntax error, unexpected ')', expecting '='
          def hyphenate_to8word, size, formatter)
                                                 ^
/home/terceiro/src/noosfero/config/../vendor/rails/actionmailer/lib/action_mailer/vendor/text/format.rb:1466: syntax error, unexpected kEND, expecting $end

(See full trace by running task with --trace)
```

Aquele 8 deveria ser um parêntese, ok, mas o Rails não teria saído com um erro
de sintaxe ridículo desse. Fui lá no arquivo e o 8 realmente estava lá, mas não
tava dando erro antes, e eu não tinha mudado nada que pudesse disparar a
inclusão de um arquivo que não estivesse sendo usado ainda. Fui no repositório
subversion e lá o arquivo estava ok.

Diabos, o que é que tinha acontecido? O subversion não reportava o arquivo como
alterado, e a última alteração foi exatamente a inclusão do rails no
repositório:

```
terceiro@sede:~/src/noosfero$ svn log /home/terceiro/src/noosfero/config/../vendor/rails/actionmailer/lib/action_mailer/vendor/text/format.rb
------------------------------------------------------------------------
r11 | AntonioTerceiro | 2007-07-02 13:00:11 -0300 (Seg, 02 Jul 2007) | 3 lines

adding Rails version 1.2.3 to the repository
```

"Ah, preciso ir em frente", eu pensei. Apaguei o arquivo e dei um `svn update`.

Olhei o arquivo e agora o parêntese estava lá de volta. Não podia ser um bug no
subversion. Daí eu me lembrei da história da inversão de bit e fui conferir:

```
terceiro@sede:~/src/noosfero$ irb
>> x = '8('
=> "8("
>> x[0]
=> 56
>> x[1]
=> 40
>> 32 + 16 + 2
=> 50
>> 32 + 16 + *
?> 32 + 16 + 8
SyntaxError: compile error
(irb):5: syntax error, unexpected tSTAR
        from (irb):6
        from :0
>> 32 + 16 + 8
=> 56
>> 32 + 8
=> 40
>>
```

o caracter "8" é 56 = 8 + 16 + 32, ou em binário:

`00011100`

Já o "(" é 40 = 8 + 32, ou em binário:

`00010100`.

Ou seja: por algum motivo aquele quinto bit inverteu no disco! Tomara que isso
não indique que o disco vai abrir o bico ... :-/
