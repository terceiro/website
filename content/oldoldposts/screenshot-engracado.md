---
title: 'Screenshot engraçado'
created_at: "2008-09-10 15:52 -3"
kind: article
old: true
---

Não sei porquê, mas o terminal onde eu estava rodando o
[mongrel](http://mongrel.rubyforge.org/) enlouqueceu. Não sei como ele
recebeu um caracter de controle e ficou assim:

![](/oldoldposts/images/rails-is-crazy-small.png)

Nada a ver, mas é engraçado. Matar o mongrel e dar um reset no terminal resolveu o caso.
