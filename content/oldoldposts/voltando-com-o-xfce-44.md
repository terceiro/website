---
title: 'voltando com o XFCE 4.4'
created_at: "2007-02-22 02:10 -3"
kind: article
old: true
---

Esses dias eu vi [via Newsforge](http://software.newsforge.com/article.pl?sid=07/02/13/153239&amp;from=rss) uma [notícia do Linux.com](http://www.linux.com/article.pl?sid=07/02/13/153205) sobre o lançamento do [lançamento do XFCE 4.4](http://www.xfce.org/about/news?id=11). A notícia no Linux.com fazia uma review bastante positivo do XFCE 4.4.

Eu fui por muito tempo usuário do XFCE. Praticamente durante toda minha estada em Porto Alegre eu usei XFCE. Como eu e Josy usávamos em casa a mesma máquina com uma configuração dual-head, então nem dava pra pensar em os dois usarem GNOME ao mesmo tempo. Daí eu me apeguei ao XFCE, ele conseguia ser bonito e leve ao mesmo tempo. Os tempos mudaram e na volta pra Salvador acabei me acostumando a usar GNOME. Eu adoro o GNOME, a disposição das coisas no Desktop é bem legal.

Hoje resolvi voltar a usar o XFCE. Vi que no Debian sid (e no etch também) está empacotada uma versão RC2 do XFCE 4.4 (4.3.99.2), então é praticamente a mesma coisa. Instalei.

Depois de uma customizadazinha pra colocar a disposição das coisas parecida com a do GNOME (eu realmente gosto do conceito de Desktop do GNOME), instalação de uns plugins (eu sou dependente de um monitor de CPU), e configuração de aplicações iniciais (applet do NetworkManager e xbindkeys pra funcionar as teclas multimídia) ficou jóia.

Impressões:

-   as novas opções de configuração do XFCE estão ótimas (e.g. a configuração dos painéis).
-   o novo gerenciador de arquivos do XFCE (thunar) é ótimo.
-   o artwork do Debian etch deixou o XFCE muito bonitinho.
-   meu desktop agora é muuuuito mais rápido
    -   provavelmente isso será compensado por alguns incovenientes que ainda não me atingiram. :-)

-   aparentemente tem várias coisas que são integradas com o GNOME, provavelmente através da implementação dos padrões do freedesktop.org. Por exemplo, as miniaturas de pré-visualização de arquivos (imagens, PDF's, filmes) carregaram instantaneamente quando naveguei pelos diretórios.
    -   por outro lado a visão de ícones no desktop (novidade do XFCE 4.4, cortesia do thunar) não mostra a pré-visualização dos arquivos.

-   o *composition manager* embutido no xfwm4 é muito lento e tem poucos recursos.
-   **update:**
    -   Sinto falta do [trocador de janelas do compiz](http://wiki.softwarelivre.org/Blogs/BlogPostAntonioTerceiro20061016180254). Já me peguei colocando o ponteiro do mouse no canto direito da tela e esperando as janelas se arrumarem num plano. Essa parece ser a única funcionalidade *realmente* útil do compiz. :-)
    -   Precisei habilitar as configurações de inicialização do XFCE para iniciar os serviços do GNOME, pra que o applet do NetworkManager consiga pegar as chaves das redes *wireless* que eu uso (i.e. o nm-applet precisa do gnome-keyring-daemon).
    -   Tinha esquecido algo fundamental: um applet que mostre o estado da bateria.


*Utilidade pública:* algo que não deu pra configurar pelas vias normais foram as teclas multimídia. Como escrevi acima, tive que apelar pro xbindkeys. Graças a [essa página no Gentoo Wiki](http://www.gentoo-wiki.com/HOWTO_Use_Multimedia_Keys), consegui facilmente configurar as teclas de controle de playback e de volume. As teclas "DVD" e "Música" aparentemente não geram nenhum evento no X.org. A configuração do xbindkeys ficou assim (é um HP Pavilion dv1000):

```
##############################
# Playback controls
##############################

"rhythmbox-client --previous"
    m:0x0 + c:144
    NoSymbol
"rhythmbox-client --play-pause"
    m:0x0 + c:162
    NoSymbol
"rhythmbox-client --next"
    m:0x0 + c:153
    NoSymbol
"rhythmbox-client --stop"
    m:0x0 + c:164
    NoSymbol

##############################
# Volume controls
##############################
"amixer sset Master 2-"
    m:0x0 + c:174
    NoSymbol
"amixer sset Master toggle"
    m:0x0 + c:160
    NoSymbol
"amixer sset Master 2+"
    m:0x0 + c:176
    NoSymbol
```
