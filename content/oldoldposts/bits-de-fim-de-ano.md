---
title: 'bits de fim de ano'
created_at: "2005-12-15 19:16 -3"
kind: article
old: true
---

## migração do `twiki.softwarelivre.org`

Foi [quase indolor](http://wiki.softwarelivre.org/Main/Migra%e7%e3oParaDakarRelease). No último final de
semana resolvi que não dava mais pra ficar com a [CairoRelease](http://twiki.org/cgi-bin/view/Codev/CairoRelease "'Codev/CairoRelease' on TWiki.org"), principalmente pelo problema do número absurdo de WikiSpam que estava acontecendo. Tinha um maldito robô se registrando a cada minuto com um nome diferente, editando os tópicos e incluindo links aleatórios pra sites de gosto duvidoso, o que me fez fechar o registro por mais ou menos um mês.

Mas agora isso acabou. A [DakarRelease](http://twiki.org/cgi-bin/view/Codev/DakarRelease "'Codev/DakarRelease' on TWiki.org"), além de [muitas outras melhorias](http://twiki.org/cgi-bin/view/Codev/DakarReleaseNotes "'Codev/DakarReleaseNotes' on TWiki.org"), exige confirmação do registro através de um código de ativação enviado por e-mail. Isso atenua muito o problema, pois registros precisarão agora de 1) um endereço de e-mail válido e 2) algum dissernimento pra localizar o código de ativação dentro da mensagem. Pelo menos enquanto algum desgraçado não fizer um robô que "lê" as mensagens do registro, a coisa está tranquila.

## trabalho

A dissertação está indo pra frente, num ritmo razoável. Preciso ficar verme em
[mônadas](http://homepages.inf.ed.ac.uk/wadler/topics/monads.html#marktoberdorf) e
converter meu *parser* em [Happy](http://www.haskell.org/happy/) em monádico,
de preferência com um analisador léxico monádico também ... aff.

Não vou pra Salvador no fim de ano, e *preciso* terminar tudo até fevereiro.

##  saudade de casa ...

Ontem foi aniversário dum primo meu e eu liguei pra Salvador. Enquanto eu
estava em casa comento pão integral no microondas, a família estava comendo
caranguejo.  :-p Me deu uma coisa por dentro .... argh!
