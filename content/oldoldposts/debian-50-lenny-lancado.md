---
title: 'Debian 5.0 (Lenny) lançado'
created_at: "2009-02-16 00:18 -3"
kind: article
old: true
---

<img src="/oldoldposts/images/lenny-banner.png" style="float: right"/>

Então, o Debian 5.0, codinome *Lenny*, [está pronto](http://www.debian.org/News/2009/20090214). Foi bastante tempo de desenvolvimento, e mais vários meses de teste pra essa versão ser estável como uma rocha.

É muito bom poder participar desse projeto e fazer o melhor sistema operacional disponível, que é preparado pra rodar em todo tipo de equipamento, desde equipamentos pequeninos até mainframes do tamanho de uma geladeira. Isso sem falar na comunidade sensacional e absurdamente competente.

Como a versão estável foi lançada e o congelamento acabou, começou o ciclo de desenvolvimento da próxima versão (codinome *Squeeze*). Isso quer dizer que já podemos começar a subir novas versões dos pacotes. A nova versão da [mocha](http://mocha.rubyforge.org), uma biblioteca de *mocking* e *stubbing* para Ruby, usada em conjunto com frameworks de teste automatizado, já está a caminho.

A julgar pelo [número de pacotes na área de upload](http://incoming.debian.org/), muita gente estava se coçando pra começar a subir versões novas. :-)
