---
title: 'atualizando o Planeta PSL-BA'
created_at: "2007-12-04 13:05 -3"
kind: article
old: true
---

Acabei de instalar o novo feed de [Aurium](http://www.colivre.coop.br/Aurium).

Além disso consertei o template do index.html pra gerar corretamente a URL dos
feeds ali na barra que lista os blogs todos.

Queria com esse post convidar todos a ajudar na manutenção do planeta. A configuração
dele fica num repositório subversion em <https://svn.softwarelivre.org/svn/psl-ba/planeta/trunk>

Quem quiser fazer alguma mudança de endereço de feed, alterar templates, fazer um design novo,
qualquer coisa, usa esse repositório e me manda um patch.

Dica: enquanto a configuração do planeta estiver usando [tecnologia obsoleta](http://subversion.tigris.org/),
vocês podem fazer modificações com commits locais antes de me mandar usando
[svk](http://svk.bestpractical.com/) ou
[git-svn](http://www.kernel.org/pub/software/scm/git/docs/git-svn.html). Eles
estão disponíveis nos melhores sistemas operacionais, basta digitar
`apti<Tab> inst<Tab> svk git-svn` . !;-)
