---
title: 'métricas para Ruby'
created_at: "2008-05-10 16:47 -3"
kind: article
old: true
---

tomando nota ...

-   [saikuro](http://saikuro.rubyforge.org/): ferramenta que calcula complexidade ciclomática para código Ruby
-   [flog](http://ruby.sadi.st/Flog.html): ferramenta que calcula uma métrica de complexidade, digamos, interessante para código Ruby.
-   [rcov](http://rubyforge.org/projects/rcov), ferramenta que calcula cobertura do código pelos testes
-   [metric-fu](http://metric-fu.rubyforge.org/), plugin para Rails usa as ferramentas pra gerar relatórios de métricas para projetos Rails

