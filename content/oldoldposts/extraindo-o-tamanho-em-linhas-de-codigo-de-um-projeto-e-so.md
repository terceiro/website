---
title: 'extraindo o tamanho em linhas de código de um projeto, e só'
created_at: "2008-05-13 16:07 -3"
kind: article
old: true
---

o [sloccount](http://www.dwheeler.com/sloccount/) é bem legal, mas a saída dele
é muito detalhada pro que eu preciso no momento. Por hora eu preciso ter apenas
um número que indica o tamanho de um projeto de software em termos de linhas de
código.

Um hack rápido resolve o caso:

```
terceiro@morere:/tmp/inkscape-0.46$ sloccount . | grep 'Total.*SLOC' | cut -d = -f 2 | sed -e 's/,//; s/\s//'
369021
terceiro@morere:/tmp/inkscape-0.46$ cd ../libmocha-ruby-0.5.6/
terceiro@morere:/tmp/libmocha-ruby-0.5.6$ sloccount . | grep 'Total.*SLOC' | cut -d = -f 2 | sed -e 's/,//; s/\s//'
4924
```
