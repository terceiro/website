---
title: 'Wagner, diga não às drogas!'
created_at: "2008-04-30 22:08 -3"
kind: article
old: true
---

O governador está mal assessorado, coitado. Como é que pode fazer um protocolo
de intenções onde se coloca a estrutura pública de educação a serviço de uma
empresa privada, multinacional, pra formar usuários de seus produtos?

[![](http://wiki.dcc.ufba.br/pub/PSL/WagnerDigaNaoAsDrogas/pamnmpt.png)](http://wiki.dcc.ufba.br/bin/view/PSL/WagnerDigaNaoAsDrogas)

Em reação a isso, o [PSL-BA](http://wiki.dcc.ufba.br/PSL/), através de vários de seus militantes, fez uma visita nessa terça-feira à Assembléia Legislativa do Estado da Bahia e consegui aprovar na Comissão de Educação, Ciência e Tecnologia, Cultura e Serviço Pública [uma audiência pública para tratar do tema](https://listas.dcc.ufba.br/pipermail/psl-ba/2008-April/018422.html).

Estamos de olho!
