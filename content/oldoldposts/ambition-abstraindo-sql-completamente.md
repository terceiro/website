---
title: 'ambition: abstraindo SQL completamente'
created_at: "2007-09-07 18:44 -3"
kind: article
old: true
---

"A simple experiment and even simpler query library."

<https://rubygems.org/gems/ambition>

Sensacional, permite fazer consultas com ActiveRecord expressando as condições com código Ruby
de verdade!

Falando sério agora: todo desenvolvedor de aplicações que usam bancos de dados relacionais
para armazenamento que se respeite tem que saber SQL. Mesmo que ele vá usar bibliotecas geniais
como ambition.

**UPDATE:** o cara começou esse projeto ***9 dias atrás***. PQP ... :-)

**UPDATE 2:** os outros ~~projetos dele~~ também parecem interessantes ...

**UPDATE 3 (2020):** site de "projetos dele" não existe mais; linkando pra rubygems.org
