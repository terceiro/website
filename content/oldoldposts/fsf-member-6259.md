---
title: 'FSF member #6259'
created_at: "2008-01-16 15:12 -3"
kind: article
old: true
---

![FSF Associate Member #6259 Badge](/oldoldposts/images/fsf-member.png)

[Colabore você também](http://www.fsf.org/register_form?referrer=6259).

(os links acima irão levar a informação de que você se registrou por indicação minha e vão me dar pontos
que eventualmente eu posso usar pra [ganhar brindes](https://www.fsf.org/associate/referral) da FSF. Se você não quiser isso siga [esse link](http://member.fsf.org) aqui)
