---
title: 'travando o painel do GNOME'
created_at: "2009-03-05 21:44 -3"
kind: article
old: true
---

De vez em quanto meu pai me ligava perguntando porquê a barra de cima ou de baixo tinha sumido no GNOME dele. O que acontecia era que ele sem querer arrastava um painel pra cima do outro, ou mesmo removia o painel sem querer. Isso deve ter acontecido umas três vezes já.

Nunca mais: pesquisando um pouco na internet, descobri uma forma de travar o painel de forma que o usuário não consegue quebrar o desktop (pelo menos não no que diz respeito aos painéis do GNOME): basta abrir o gconf-editor ("Ferramentas do Sistema" → "Editor de Configurações) e marcar a chave booleana `/apps/panel/global/locked_down`. Com essa chave marcada o usuário não consegue nem mover os painéis nem incluir novos lançadores, nem nada. Bastante útil.

