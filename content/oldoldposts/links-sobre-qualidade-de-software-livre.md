---
title: 'links sobre qualidade de software livre'
created_at: "2008-10-29 14:39 -3"
kind: article
old: true
---

-   ["Quality Matters"](https://fossbazaar.org/content/quality-matters/), um post no site [FOSSBazar](https://fossbazaar.org/) (que tem outros posts interessantes também).
-   ["Alitheia: The Truth about OSS Quality Goes Alpha"](http://www.linuxpromagazine.com/online/news/alitheia_the_truth_about_oss_quality_goes_alpha/\(kategorie\)/0), Linux Magazine

