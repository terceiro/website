---
title: 'vim + rails'
created_at: "2007-01-12 16:15 -3"
kind: article
old: true
---

Eu já sabia que existia, mas ontem esbarrei com um [post do TaQ](http://www.eustaquiorangel.com/blog/show/339) na blogosfera da comunidade [Ruby on Br](http://forum.rubyonbr.org/blogosfera) e vi que não podia ficar sem isso.

A combinação matadora é: [rails.vim](http://www.vim.org/scripts/script.php?script_id=1567) + [project.vim](http://www.vim.org/scripts/script.php?script_id=69).
