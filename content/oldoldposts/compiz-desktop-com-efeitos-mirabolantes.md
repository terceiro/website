---
title: 'compiz: desktop com efeitos mirabolantes'
created_at: "2006-10-16 18:02 -3"
kind: article
old: true
---

Hoje vi na internet que o compiz tinha entrado na `sid` outro dia ... resolvi testar esse bicho. Me surpreendi com a facilidade de configuração, e resolvi postar pra quem quiser testar também (sei de gente que vai ter orgasmos com isso). :-)

Graças ao *Debian X Strike Force*, a instalação do compiz é ridícula.

    # aptitude install compiz

Além do empacotamento do compiz, a DXSF também incluiu no pacote do x.org os patches necessários pro compiz funcionar. As (poucas) configurações necessárias numa instalação razoavelmente atualizada são (todas no `xorg.conf`):

-   Os módulos `glx` e `dri` devem estar habilitados (no meu xorg.conf eles já estavam), assim como o suporte a aceleração glx na placa de vídeo deve estar configurado.

-   incluir uma seção `Extensions` assim:

             Section "Extensions"       Option "Composite" "Enable"     EndSection     


-   incluir na seção `Device` (a da placa de vídeo), o opção: `Option          "XAANoOffscreenPixmaps"`

Feito isso, basta reiniciar o X e num terminal, rodar:

    $ compiz --replace

o `--replace` diz pro compiz que ele deve substituir o gerenciador de janela atual.

Daí é só brincar com os efeitos disponíveis através de plugins. No [wiki do Gentoo](http://gentoo-wiki.com/Compiz#Using_plugins) tem uma listinha de atalhos de teclados pra fazer as firulas.
