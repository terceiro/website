---
title: 'links do dia'
created_at: "2007-08-30 17:22 -3"
kind: article
old: true
---

Sobre o [modelo do ditador benevolente](http://en.wikipedia.org/wiki/Benevolent_dictator):

-   [The Myth of the Benevolent Dictator](http://blogs.ittoolbox.com/database/soup/archives/the-myth-of-the-benevolent-dictator-18668), por Josh Berkus.
-   [On the benevolent dictator model](http://tytso.livejournal.com/30681.html), comentário do Ted Tso sobre o post do Josh Berkus.

Sobre negócios e a GPL:

-   [GPL and Business FAQ](http://www.enricozini.org//2007/tips/gpl-and-business-faq.html), por Enrico Zini.
-   [Selling Free Software](http://www.gnu.org/philosophy/selling.html) (gnu.org)

Sobre Debian e Perl:

-   [Integrating Perl in a wider distribution: The Debian pkg-perl group](http://vienna.yapceurope.org/ye2007/talk/513), por Gunnar Wolf.

