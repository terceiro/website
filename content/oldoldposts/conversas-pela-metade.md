---
title: 'conversas pela metade'
created_at: "2006-04-13 22:28 -3"
kind: article
old: true
---

*(escrevi isso ontem, quando a rede do II saiu do ar...)*

Uma e pouco da tarde. Estou passando pela Praça da Matriz em Porto Alegre, indo
pegar o ônibus pra ir pro campus. Um pregador duma dessas igrejas
neo-apocalípticas esbraveja no meio da praça. Eu não estava prestando muita
atenção. Estava tentando distrair a minha cabeça do
[grande stress](http://fisl.softwarelivre.org/7.0/) por que eu estou passando, e me vieram *flashes* do que o cara tava falando.

– Por que o Senhor não-sei-quê-lá ... e aqueles que não-sei-quê-lá ... vão conhecer ... não-sei-quê-lá ... do Senhor!

Por reflexo, larguei um prazeroso "tsc, tsc", acompanhado pelo gesto
correspondente — e inevitável — com a cabeça. Já seguindo o meu
caminho, peguei mais uns flashes da conversa do cara.

– Aquele ali, ó, balançou a cabeça! Ele deve ... *([efeito doppler](http://pt.wikipedia.org/wiki/Efeito_Doppler))*.

Depois pensando, me dei conta que teria sido engraçado ter ouvido o que ele
falou de mim.

Entro no ônibus. No começo do trajeto,
[mais um pepino](http://fisl.softwarelivre.org/7.0/papers/pub/) me chega por
telefone. Passo o resto da viagem pensando como é chato ter que lidar com um
determinado tipo de gente ... (isso é um desabafo, é pra ficar no ar mesmo. Se
eu der detalhes eu posso ter problemas ;-))

Já subindo pro [Instituto](http://www.inf.ufrgs.br/), passando pelo
Departamento de Genética ... mais uma conversa pelo meio. Dessa vez eu não
estava envolvido de forma nenhuma.

– aí o pessoal do MST disse ...  
– [MST](http://www.mst.org.br/) ou [MCT](http://www.mct.gov.br/)?  
– ah não, não, hahaha, M**C**T, M**C**T ... sai pra lá ... hahaha  
– é ... por que se você é amigo desse pessoal aí ... hehehe ...  

A mídia do deus-mercado conseguiu. Eu senti o preconceito na minha espinha. Se
as pesoas na universidade pensam assim, imagina ...
