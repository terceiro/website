---
title: 'Controle de Versão e Fluxo de Trabalho em projetos de desenvolvimento de software'
created_at: "2008-11-08 11:53 -3"
kind: article
old: true
---

Este é o material do minicurso que eu dei no [V Empirical Software Engineering Latin American Workshop](http://www.eselaw.unifacs.br/), realizado nos últimos dias 5, 6 e 7 em Salvador.

**Título:** Controle de versão e fluxo de trabalho em projetos de desenvolvimento de software  
**Resumo:**

> Será apresentado um breve histórico dos sistemas de controle de versão
> livres, tanto do ponto de vista tecnológico quanto do modelo de fluxo de
> trabalho pressuposto pelos mesmos. Será dada ênfase em sistemas de controle
> de versão distribuído, em especial [git](http://git.or.cz/), e como eles
> podem suportar diferentes *workflows* em desenvolvimento de software.

Você pode baixar os [slides em PDF](/oldoldposts/files/curso-vcs.pdf). O
código-fonte (LaTeX + beamer) está [no github](http://github.com/terceiro/curso-vcs).

[![Creative Commons License](http://i.creativecommons.org/l/by-nc-sa/2.5/br/88x31.png)](http://creativecommons.org/licenses/by-nc-sa/2.5/br/)

Controle de versão e fluxo de trabalho em projetos de desenvolvimento de software por [Antonio Terceiro](/) é licenciado sobre uma licença [Creative Commons Atribuição-Uso Não-Comercial-Compartilhamento pela mesma Licença 2.5 Brasil](http://creativecommons.org/licenses/by-nc-sa/2.5/br/).

Em breve eu devo apresentar esse minicurso no [IM](http://www.im.ufba.br/),
interessados fiquem de olho.
