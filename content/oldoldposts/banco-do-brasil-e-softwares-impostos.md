---
title: 'Banco do Brasil e softwares impostos'
created_at: "2006-12-18 17:24 -3"
kind: article
old: true
---

Um tempo atrás eu resolvi escrever para a ouvidoria do Banco do Brasil sobre a necessidade de usar software proprietário para acessar o serviço de Internet Banking. Isso foi há várias semanas atrás, então não lembro mais as palavras exatas que escrevi, mas lembro que citava o plugin Java do projeto Classpath como alternativa livre. Pouco depois fui fuçar o *gcjwebplugin* [consegui descobrir exatamente porquê o Internet Banking do BB não funciona com ele](http://developer.classpath.org/mediation/Applets).

Enfim, nem lembrava mais que tinha enviado essa mensagem. Eis que recebo de volta essa pérola (copiada Ipsis literis):

```
From: Banco do Brasil <respondebb@bb.com.br>
To: terceiro@softwarelivre.org
Subject: Ouvidoria BB - 57331158
Date: Fri, 15 Dez 2006 16:19:55 -0300


Em resposta a sua ocorrencia numero 57331158:

Prezado cliente em atencao a sua sugestao temos a informar:

 Os requisitos de seguranca que utilizamos (teclado virtual) nos imped

 de utilizar um plugin diferente do Java da SUN. De qualquer forma, na

 entendemos o plugin da Sun como software proprietario dada a sua car-

 cteristica de livre distribuicao. No momento que a SUN mudar a sua

 politica de distribuicao, se eh que um dia isso possa acontecer, da

 sua JVM, o Banco irah implementar outro caminho para acesso seguro

 aas suas paginas.

Ficamos agredecidos por sua preocupacao.



Para novo contato, acesse a página do Banco do Brasil
na Internet e clique em Ouvidoria BB, ou telefone para 0800-7295678
```

Respondi assim:

```
Date: Mon, 18 Dec 2006 14:25:58 -0300
From: "Antonio S. de A. Terceiro" <terceiro@softwarelivre.org>
To: Banco do Brasil <respondebb@bb.com.br>
Subject: Re: Ouvidoria BB - 57331158
User-Agent: Mutt/1.5.13 (2006-08-11)

Olá,

Banco do Brasil escreveu isso aí:
>
> Em resposta a sua ocorrencia numero 57331158:
>
> Prezado cliente em atencao a sua sugestao temos a informar:
>
>  Os requisitos de seguranca que utilizamos (teclado virtual) nos imped
>  de utilizar um plugin diferente do Java da SUN. De qualquer forma, na
>  entendemos o plugin da Sun como software proprietario dada a sua car-
>  cteristica de livre distribuicao. No momento que a SUN mudar a sua
>  politica de distribuicao, se eh que um dia isso possa acontecer, da
>  sua JVM, o Banco irah implementar outro caminho para acesso seguro
>  aas suas paginas.
>
> Ficamos agredecidos por sua preocupacao.

Gostaria de pontuar algumas coisas sobre essa resposta:

1) Não foi fornecido nenhum argumento técnico convincente
para a afirmação que "Os requisitos de seguranca que utilizamos (teclado
virtual) nos imped de utilizar um plugin diferente do Java da SUN"
(sic).

Existe alguma documentação pública que mostre que o teclado virtual é
realmente seguro? Existe alguma documentação que mostre que é necessário
que ele seja implementado em Java de forma imcompatível com outras
implementações que não sejam a da Sun? Existe alguma documentação que mostre
que o teclado virtual

2) O técnico que respondeu a minha mensagem não tem conhecimento da
diferença entre software livre e software proprietário. A versão atual
do JRE Java da Sun é de fato distribuído gratuitamente, mas isso não faz
com que deixe de ser proprietário: nenhum programador no mundo, exceto
os da Sun, pode examinar o código-fonte do plugin Java da Sun e dizer
se ele é realmente seguro. Desta forma, o banco está delegando à Sun a
segurança dos seus correntistas, o que não me parece estrategicamente.

Dada essa distinção entre "grátis" e "não-proprietário", seria bom rever
a afirmação de que "No momento que a SUN mudar a sua politica de
distribuicao, se eh que um dia isso possa acontecer, da sua JVM, o Banco
irah implementar outro caminho para acesso seguro aas suas paginas." Com
essa afirmação, o Banco demonstraria estar mais preocupado com a
gratuidade da plataforma do que com a efetiva segurança de seus
correntistas. Seria melhor que o "outro caminho para acesso seguro aas
suas paginas" (sic) fosse tomado já, sem depender de instituições que
são externos à relação banco-correntista.
```

Relendo a mensagem acho que ela ficou meio confusa, mas você me entende.

Veja também a [campanha da FSFLA contra softwares impostos](http://www.fsfla.org/?q=pt/node/119).
