---
title: 'Wikis vão para a impressão'
created_at: "2007-12-15 12:51 -3"
kind: article
old: true
---

Acabei de ler a
[notícia](http://wikimediafoundation.org/wiki/Wikis_Go_Printable) no site da
Wikimedia Foundation. A [PediaPress](http://pediapress.com/), uma empresa
*startup* da Alemanha desenvolveu uma
[biblioteca livre chamada mwlib](http://code.pediapress.com/)
pra fazer parsing de conteúdo do
[MediaWiki](http://www.mediawiki.org/)
e gerar arquivos para impressão. A Wikimedia Foundation fez uma parceria com a
[Open Society Institute](http://www.soros.org/)
e a
[Commonwealth of Learning](http://www.col.org/),
que vão financiar o desenvolvimento pela PediaPress.

Aparentemente a mwlib "só" interpresa o conteúdo do MediaWiki, eu não consegui
achar o código que eles estão usando pra gerar PDF (demo no site
[WikiEducator](http://wikieducator.org/Help:Collections)), mas os resultados
são só razoáveis. Acho que os resultados poderiam ser melhores se eles
estivessem usando LaTeX pra gerar o PDF, mas pelo que eu vi no código da mwlib LaTeX parece estar
sendo usado só pra [gerar figuras para fórmulas matemáticas](http://code.pediapress.com/hg/mwlib/file/f36ab73d3c28/mwlib/htmlwriter.py).

Outro projeto relacionado interessante é o [kiwix](http://www.kiwix.org/),
desenvolvido para se distribuir versões offline da
[Wikipedia](http://www.wikipedia.org/), desenvolvido para o [Wikipedia on DVD](http://www.wikipediaondvd.com/site.php).
