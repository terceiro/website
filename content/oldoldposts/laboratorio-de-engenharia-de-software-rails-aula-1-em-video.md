---
title: 'Laboratório de Engenharia de Software: Rails, aula #1 em vídeo'
created_at: "2009-05-02 02:32 -3"
kind: article
old: true
---

Como parte do meu estágio docência, uma das atividades obrigatórias do [doutorado](http://dmcc.dcc.ufba.br/), estou compartilhando com a minha orientadora, [professora Christina](http://www.dcc.ufba.br/~flach/), a disciplina Laboratório de [Engenharia de Software](http://disciplinas.dcc.ufba.br/MATB14) na graduação em Computação da UFBA. A idéia da disciplina é trabalhar questões práticas de desenvolvimento de software, e implementar um projeto de verdade usando Ruby e Rails, usando conceitos de desenvolvimento ágil. No começo do curso, fizemos [sessões de TDD](http://disciplinas.dcc.ufba.br/MATB14/TDDComRuby) para introduzir o conceito de TDD e aumentar a familiaridade com [Ruby](http://www.ruby-lang.org/), e pensamos "a gente podia ter gravado esse negócio pra servir de referência depois".

Algumas semanas depois, chegamos no ponto de apresentar o [Rails](http://www.rubyonrails.org/) e eu resolvi que desse vez ía: gravamos a aula toda usando o [recordMyDesktop](http://packages.debian.org/sid/gtk-recordmydesktop), e o vídeo está disponível [aqui](https://disciplinas.dcc.ufba.br/pastas/MATB14/rails/).

Algumas observações:

-   O vídeo está em formato [Ogg](http://www.fsf.org/resources/formats/playogg), usando o codec [Theora](http://theora.org/), que é um codec de vídeo livre (de patentes inclusive). A grande maioria dos players livres devem tocar facilmente, em especial [mplayer](http://packages.debian.org/lenny/mplayer) e [vlc](http://packages.debian.org/lenny/vlc) são boas opções.
-   minha locução é bizarra, eu sei. Mas eu não ligo. :-)
-   [![Creative Commons License](http://i.creativecommons.org/l/by/2.5/br/88x31.png)](http://creativecommons.org/licenses/by/2.5/br/)  
   Laboratório de Engenharia de Software: Rails, aula \#1 por [Antonio Terceiro](http://people.softwarelivre.org/~terceiro/), está licenciada pela [Licença Creative Commons Atribuição 2.5 Brasil](http://creativecommons.org/licenses/by/2.5/br/).

 

**update (30/07/2010):** o local dos vídeos mudou

