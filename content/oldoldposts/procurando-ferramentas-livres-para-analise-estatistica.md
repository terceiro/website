---
title: 'procurando ferramentas livres para análise estatística'
created_at: "2007-11-08 14:52 -3"
kind: article
old: true
---

Estou cursando no doutorado uma disciplina de Engenharia de Software Experimental. ESE é, em termos gerais, o estudo da Engenharia de Software usando o método científico: observação, formulação de hipóteses, instrumentação, coleta de dados, análise estatística e interpretação de resultados. O crescimento dessa prática na comunidade de SE é uma reação ao atual estado de achismo das coisas. Adotam-se novas técnicas, métodos e ferramentas na base do "disseram que ...". Parafraseando meu amigo [Methanias](http://www.unit.br/methanias/), "alguém grita: 'É Java!', e todo mundo corre atrás". Ninguém *testa* pra saber se novas técnicas e ferramentas de fato trazem benefício. Na base do marketing e da conversa de vendedor, o "mercado" segue dançando [a verdadeira dança do patinho](http://www.estudiolivre.org/tiki-browse_freetags.php?tag=dan%C3%A7a%20do%20patinho) (letra [aqui](http://letras.terra.com.br/bnegao/133054/))

Pra fazer [análise estatística](http://pt.wikipedia.org/wiki/Estat%C3%ADstica) utiliza-se softwares estatísticos. Mas todas as opções que me foram apresentadas são proprietárias. Obviamente eu não me submeter a isso, e daí comecei a procurar alternativas livres. Elas não são poucas, e eu listo aqui as 4 que me parecerem mais promissoras (por enquanto só no achismo, sem qualquer dado experimental !;-) ):

Nome | Site | No Debian
-----|------|--------------------------------------------------------------
RKWard | <http://rkward.sourceforge.net/> | [rkward](http://packages.debian.org/sid/rkward)
R Commander | <http://socserv.mcmaster.ca/jfox/Misc/Rcmdr/> |[r-cran-rcmdr](http://packages.debian.org/sid/r-cran-rcmdr)
PSPP | <http://www.gnu.org/software/pspp/> | [pspp](http://packages.debian.org/sid/pspp)
gretl | <http://gretl.sourceforge.net/> | [gretl](http://packages.debian.org/sid/gretl)


Pra minha graça, todas a 4 estão no Debian. RKWard e R Commander são frontends para o [GNU R](http://www.r-project.org/), uma linguagem de programação feita pra análise estatística, e isso parece ser uma boa coisa.

**Atualização (07/04/2010):** no final das contas optei pelo RKWard e estou muito satisfeito. Ele fornece assistentes para fazer as análises mais básicas, que geram o código em R que vai ser executado. Se você precisar fazer algo que a interface não permite, é só pegar o código que o assistente gráfico gera e meter a mão.

