---
title: 'editando arquivos encriptados com GPG no vim'
created_at: "2006-01-24 16:58 -3"
kind: article
old: true
---

[gnupg.vim : Plugin for transparent editing of gpg encrypted files.](http://vim.sourceforge.net/scripts/script.php?script_id=661)

É por essas e outras que a gente ama esse negócio de software livre. :-)
