---
title: 'recordar é viver'
created_at: "2007-06-05 19:39 -3"
kind: article
old: true
---

Um tempo atrás meu brother Charles tinha me pedido um programinha de algoritmo genético que eu tinha feito na época da [graduação](http://www.dcc.ufba.br/), mas eu não achei no meu repositório subversion pessoal.

Esses dias eu encontrei um CD de backup antigo em casa, e nele encontrei esse programa e outras velharias, da época que eu ainda não usava controle de versão compulsivamente.

Então, esse post é provavelmente o primeiro da série "recordar é viver", onde eu vou largar aqui esses programas.

Este programa de agora resolve o [problema do caixeiro viajante](http://pt.wikipedia.org/wiki/Problema_do_caixeiro_viajante) usando um [algoritmo genético](http://pt.wikipedia.org/wiki/Algoritmo_gen%C3%A9tico). Por ser probabilístico, o algoritmo genético nem sempre chega na solução ótima, mas quase sempre variando os parâmetros e aguardando uma quantidade de tempo maior você consegue chegar numa solução satisfatória.

O programa foi feito em C++ usando [FLTK](http://www.fltk.org/). Eu tive que arrumar umas coisas nele pra fazer compilar com as versões atuais de FLTK e [GCC](http://gcc.gnu.org/).

![tela do programa](/oldoldposts/images/pcv.png)


~~O código-fonte vem acompanhado também do artigo que eu escrevi junto com o programa para a disciplina Programação Matemática, conhecida popularmente como POP 1, na época sob responsabilidade do professor Valter de Senna.~~

~~**Atenção:** o código-fonte do programa está licenciado sob a GPL versão 2, então aqui vai um recado pra você que ainda está estudando: copiar e apresentar esse programa como tendo sido feito por você é crime! !;-) Você pode, por outro lado, fazer o que quiser com o código desde que você siga os termos da licença.~~

**UPDATE (2020):** este post foi migrado de uma plataforma que quebrou e não tinha backup. a não ser que eu ainda tenha aquele CD, este código-fonte foi perdido pra sempre.
