---
title: 'como as coisas funcionam'
created_at: "2005-08-27 14:30 -3"
kind: article
old: true
---

É incrível como gente que [tenta se mostrar inteirado](http://br-linux.org/linux/?q=node/1634#comment-16659) em "linux" e não tem a menor noção de como as coisas funcionam no software livre.

Acho que é como diz o [Pablo](http://hackers.propus.com.br/~pablo/blog/), são as dores do crescimento. "Linux" virou moda, virou quase que um requisito pra se trabalhar com informática (principalmente — por enquanto — pra suporte/redes/manutenção/etc), e isso tem algumas consequências.

Ao entrar no círculo do mainstream, qualquer coisa se banaliza. Precisamos "saber" disso rápido ("o *mercado* está pedindo isso")! Compra aquele livro "Aprenda XYZ em 24 horas"! Aprenda o básico! Aprofundar, depois. Com o software livre (ou "linux") parece ter acontecido a mesma coisa.

É claro que nessa brincadeira a [ética](http://pt.wikipedia.org/wiki/%C3%89tica) fica pra depois. O cara sabe "tudo" de "linux", mas não consegue perceber como as coisas funcionam na hora de **fazer** o "linux". É o superficialismo, que já é presente em tudo ao nosso redor, chegando ao software livre.

Seja no desenvolvimento de um projeto, seja na organização de um evento comunitário, vale sempre o seguinte:

**Se você quer que algo seja feito da forma que você acredita** **ser a melhor, considere fazê-lo você próprio.** Isso é básico: a comunidade funciona, independente da nossa vontade, na base da meritocracia. Não adianta: quem faz as coisas acontecerem é respeitado e ouvido, quem só fala é ignorado. O [Linus Torvalds](http://en.wikipedia.org/wiki/Linus_Torvalds) (sem fazer juízo de valor sobre o cidadão) sintetizou isso na célebre frase *"Talk is cheap: show me the code"*. Se você não se envolve ...

**...não adianta meter o pau depois da merda feita.** Se você achava que devia ter sido diferente, não adianta ficar postando pela internet a fora que deveria ter sido assim, deveria ter sido assado. Ninguém é obrigado a saber o que você sabe, principalmente antes de você dizê-lo. Assim, ...

**... valorize o seu potencial de contribuição.** O [Maddog](http://en.wikipedia.org/wiki/John_maddog_Hall) diz uma coisa muito legal:

> *"Pra ver a pessoa mais importante da comunidade, olhe no espelho!"*

Numa rede, cada nó é importante. Por mais que você seja iniciante, sempre há algo em que contribuir. Seja reportando erros, escrevendo código, traduzindo, ajudando com página web, sugerindo palestrante, ajudando na organização do evento.

Mesmo que você não queira/possa se envolver diretamente, você pode ajudar dando suas sugestões *1)* no lugar certo, *2)* com um certo respeito pelo trabalho da pessoas, e principalmente, *3)* **antes** do troço acontecer! !;-)
