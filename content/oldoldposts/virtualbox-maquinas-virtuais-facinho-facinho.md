---
title: 'VirtualBox: máquinas virtuais facinho, facinho'
created_at: "2007-10-12 14:23 -3"
kind: article
old: true
---

Título alternativo: *uma boa interface conquista coreções e mentes*.

Como é que dá pro cidadão ser produtivo se todo dia aparece um
[novo brinquedo](http://www.virtualbox.org/) [pronto pra ser futucado](http://packages.debian.org/sid/virtualbox-ose) ?

Pois é, o [VirtualBox](http://www.virtualbox.org/) é muito legal. Além de ter uma interface de usuário bastante intuitiva e ótima pra gerenciar diferentes máquinas virtuais, o bicho parece ser bem mais rápido que o qemu. Já era.

![screenshot do VirtualBox](/oldoldposts/images/virtualbox-small.png)

Além de tudo isso ele tem funcionalidade muito legais, como o de trancar o teclado quando a janela da máquina virtual está selecionada: **todos**¹ os eventos de teclado vão para a máquina virtual. A mesma coisa com o mouse. O qemu tem isso também, mas no VB o acesso a isso é mais ... amigável.

¹ tá, nem todos. Todos os eventos de teclado que chegam pro desktop, esses sim vão pra máquina virtual. Os que o X.org trata por si só , como Control+Alt+F1 pra ir pro primeiro console de texto, nem chegam pro VB.

Outra funcionalidade legal é o gerenciador de imagens de disco. Você pode criar imagens de HD, cadastrar imagens ISO, e tal. Dái na hora de criar uma nova máquina virtual você escolhe a imagem de HD tal, a image ISO tal pra ser montada no drive de CD-ROM virtual ... tudo que já dava pra fazer com o qemu, mas ... bem mais prático.

Vale a pena testar.
