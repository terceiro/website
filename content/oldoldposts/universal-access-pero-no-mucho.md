---
title: 'Universal access, pero no mucho'
created_at: "2008-02-28 12:12 -3"
kind: article
old: true
---

Sometimes we face the contradiction ...

> \[...\]
>
> The Contest is not open to residents of Cuba, Iran, Syria, North Korea, Sudan, Myanmar (Burma), or to other individuals restricted by U.S. export controls and sanctions, and is void in any other nation, state, or province where prohibited or restricted by U.S. or local law.
>
> \[...\]
>
> from the GNOME Outreach Program: ***Accessibility*** [rules](http://www.gnome.org/projects/outreach/a11y/rules/).

US government sucks.

GNOME Foundation is in the US, but it doesn't suck. It is
constrained by the crush-the-losers policy of the US government.
