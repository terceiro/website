---
title: 'Planeta PSL-BA de volta'
created_at: "2007-01-06 13:50 -3"
kind: article
old: true
---

A gente estava esperando conseguir colocar o servidor de Debian-BA de volta no ar pra retornar com o planeta. Várias pessoas comentaram comigo que estava fazendo falta, pelo jeito o planeta se tornou uma forma muito legal de acompanhar o que os outros estão fazendo, e até de ter notícias mesmo.

Por enquanto ele está rodando temporariamente no meu `$HOME` num servidor da [ASL](http://www.asl.org.br/), mas assim que o servidor do Debian-BA voltar a gente transfere de volta pra lá. Coloquei pra atualizar a cada 2 horas.

Três feeds estão dando pau, e estão comentados na configuração:

- Carla: o feed aparentemente não informa as datas direito, o planet não se entende com ele e a entrada mais antiga fica no topo do planet.
- Aurélio: 404, a URL do feed está quebrada
- Guilherme Júnior: o www.guilhermejr.net não responde

Basta consertar e me pingar, eventualmente dizendo a nova URL do feed, que eu coloco no ar de novo.

Importante: quem mais tiver blog sobre software livre e quiser incluir no planeta, é só avisar também.

<http://planeta.psl-ba.softwarelivre.org> ou <http://planeta.ba.softwarelivre.org/>

Aproveitem! :-)
