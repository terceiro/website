---
title: 'Software Freedom Day 2008 em Salvador'
created_at: "2008-09-22 17:36 -3"
kind: article
old: true
---

Todo mundo [está falando bem](http://listas.dcc.ufba.br/pipermail/psl-ba/2008-September/019127.html).

Realmente o evento foi muito legal, e eu gostaria de agradecer às pessoas que
fizeram acontecer. Vocês sabem quem são. :-)

Minha palestra foi bem, tirando o fato de no final não tinha quase ninguém
mais. Acho que dizer "ó, quem não for desenvolvedor vai ficar voando" logo no
começo não foi uma boa estratégia. !;-)

Por outro lado quem ficou estava realmente a fim de conhecer e fez perguntas
realmente interessantes.
