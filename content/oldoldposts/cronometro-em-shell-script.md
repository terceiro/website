---
title: 'cronômetro em shell script'
created_at: "2008-07-01 15:43 -3"
kind: article
old: true
---

Pra quem não usa relógio, é mais prático do que tirar o celular do bolso e futucar os menus até conseguir iniciar o cronômetro. E é ridiculamente simples também:

 

    time read

 

ENTER inicia, ENTER para. :-)

