---
title: 'qualidade de código: mais alguns brinquedos para Ruby e Rails'
created_at: "2008-09-27 14:47 -3"
kind: article
old: true
---

-   [Roodi](http://roodi.rubyforge.org/), Ruby Object Oriented Design Inferometer, procurar maus sinais em código através de análise estática.
-   [tarantula](http://opensource.thinkrelevance.com/wiki/tarantula) é um plugin Rails pra fazer testes de spider na aplicação e achar (por exemplo) links quebrados.
-   [Shoulda](http://rubyforge.org/projects/shoulda), blibioteca que estende o `Test::Unit` com uma sintaxe bem mais agradável -- e fácil de ler -- e contextos!

Shoulda, em sua versão voltada para aplicações não-Rails, [vai estar no Debian](http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=500341) em breve.

E, pra variar, algo que não é uma ferramenta mas é muito útil: [Ruby on Rails Code Quality Checklist](http://www.matthewpaulmoore.com/articles/1276-ruby-on-rails-code-quality-checklist).
