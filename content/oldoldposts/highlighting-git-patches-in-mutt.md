---
title: 'highlighting (git) patches in mutt'
created_at: "2009-04-23 14:11 -3"
kind: article
old: true
---

This tip is useful for those who want to read the patches before even trying them into a branch. While it's possible to read them as regular text, being able to spot directly what matters helps doing it very quicker. Drop the following lines in your `~/.muttrc` to highlight the relevant elements in the diff:

 

    color body brightred default "^-.*"color body brightgreen default "^[+].*"color body brightwhite default "^diff --git.*"color body brightwhite default "^index [a-f0-9].*"color body brightyellow default "^@@.*"

Of course you can change the colors to match your preferences, I'm not even sure yet that these are the best colors for me. The result looks like this:

![Highlighted patch in mutt](http://wiki.softwarelivre.org/pub/Blogs/BlogPostAntonioTerceiro20090423141108/mutt-git-patch.png)

