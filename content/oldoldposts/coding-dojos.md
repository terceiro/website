---
title: 'Coding Dojos'
created_at: "2008-03-05 12:25 -3"
kind: article
old: true
---

Um *[Coding Dojo](http://www.codingdojo.org/cgi-bin/wiki.pl?CodingDojos)* é
um grupo de pessoas que se reune periodicamente pra resolver problemas de
programação, normalmente usando Desenvolvimento Dirigido por Testes e
Programação em Pares. Aparentemente parece que existem 3 dojos ativos no
Brasil:

-   [Floripa](http://dojofloripa.wordpress.com/)
-   [Recife](http://dojorecife.wordpress.com/)
-   [São Paulo](http://www.codingdojo.org/cgi-bin/wiki.pl?SaoPauloDojo)

Seria uma boa fazer um troço desses em Salvador ... mas no meu estado atual de
carga de trabalho seria fatal tentar organizar. Alguém aí a fim?
