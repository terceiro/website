---
title: 'mais Banco do Brasil, desta vez softwares mal feitos'
created_at: "2006-12-19 13:22 -3"
kind: article
old: true
---

[Minha resposta](/2006/12/18/banco-do-brasil-e-softwares-impostos/) voltou.

```
Subject: Re: Mensagem devolvida.
From: bbdesenvolvimento@bb.com.br
To: "Antonio S. de A. Terceiro" <terceiro@softwarelivre.org>
Date: Mon, 18 Dec 2006 18:03:55 -0300


________________________________________________________
Prezado(a) cliente,
Devolvemos a sua mensagem pois o nosso atendimento
e realizado atraves do telefone 0800 729 5678 ou da area
" Ouvidoria BB " em nosso Portal  http:\\www.bb.com.br (e-pronto).
Atenciosamente
Banco do Brasil S.A. - BB Responde
_______________________________________________________
Olá,
[... minha mensagem de resposta...]
```

O que se passa na cabeça de um desenvolvedor quando ele cria uma aplicação que envia e-mails que não podem ser respondidos?

Mas como eu sou brasileiro e não desisto nunca, fui de novo na Ouvidoria do Banco do Brasil, e colei minha resposta lá. Tive que reformular o texto pra caber no limite de caracteres do formulário, mas foi.

```
Olá,

Esta mensagem é uma resposta à resposta me dada na solicitação número 57331158.

Gostaria de pontuar algumas coisas sobre essa resposta:

1) Não foi fornecido nenhum argumento técnico convincente
para a afirmação que "Os requisitos de seguranca que utilizamos (teclado
virtual) nos imped de utilizar um plugin diferente do Java da SUN"
(sic).

Existe alguma documentação pública que mostre que o teclado virtual é
realmente seguro? Existe alguma documentação que mostre que é necessário
que ele seja implementado em Java de forma imcompatível com outras
implementações que não sejam a da Sun? Existe alguma documentação que
mostre que o teclado virtual é de fato a melhor solução para segurança da
senha dos correntistas? Como se pode que a minha senha está segura se não
tenho acesso ao código fonte do plugin Java da Sun?

2) O técnico que respondeu a minha mensagem não tem conhecimento da
diferença entre software livre e software proprietário. A versão atual
do JRE Java da Sun é de fato distribuído gratuitamente, mas isso não faz
com que deixe de ser proprietário: nenhum programador no mundo, exceto
os da Sun, pode examinar o código-fonte do plugin Java da Sun e dizer
se ele é realmente seguro. Desta forma, o banco está delegando à Sun a
segurança dos seus correntistas, o que não me parece estrategicamente correto.

Seria melhor que o "outro caminho para acesso seguro aas
suas paginas" (sic) fosse tomado já, sem depender de instituições que
são externas à relação banco-correntista.
```

Pra registro meu (e de quem mais quiser):

```
Obrigado por escrever para o Banco do Brasil!
Sua comunicação foi registrada com sucesso.
Logo você receberá uma resposta direta, por telefone ou e-mail.
Para acompanhar o atendimento de sua ocorrência utilize este número: 00058734738
Contamos com sua colaboração para melhorar sempre nossos serviços.

Volte sempre!
```
