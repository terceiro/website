---
title: 'programa de qualificação em software livre da Colivre'
created_at: "2007-10-05 13:36 -3"
kind: article
old: true
---

A [Colivre](http://www.colivre.coop.br/) lançou a poucos dias o seu
[programa de qualificação em software livre](https://www.colivre.coop.br/Colivre/ProgramaQualifica). São parceiros
do programa o [Departamento de Ciência da Computação da UFBA](http://www.dcc.ufba.br/) e
a [SaferNet Brasil](http://www.safernet.org.br/).

[![Programa de Qualificação em Software Livre da Colivre. Uma parceira com o DCC/UFBA e a SaferNet Brasil](/oldoldposts/images/qualifica.gif)](http://www.colivre.coop.br/Colivre/ProgramaQualifica)

Os primeiros cursos são
[GNU/Linux Básico](http://www.colivre.coop.br/Colivre/GnuLinuxBasico),
[Administração de Redes com GNU/Linux](http://www.colivre.coop.br/Colivre/AdminRedesGnuLinux) e
[Desenvolvimento de Sites Colaborativos com TWiki](http://www.colivre.coop.br/Colivre/DesenvolvendoSiteTWiki).

Num futuro mais ou menos próximo se tudo der certo teremos cursos de
[Ruby](http://www.ruby-lang.org/), [Rails](http://www.rubyonrails.org/),
e outros temas ligados a desenvolvimento.
