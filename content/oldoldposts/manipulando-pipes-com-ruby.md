---
title: 'manipulando pipes com Ruby'
created_at: "2008-01-13 03:05 -3"
kind: article
old: true
---

Muito bom. :-)

```
$ irb
>> pipe = IO.popen('sed -e "s/a/b/g"', 'w+')
=> #<IO:0xb7c54b8c>
>> pipe.puts('aaa')
=> nil
>> pipe.puts('antonio terceiro')
=> nil
>> pipe.close_write
=> nil
>> pipe.read
=> "bbb\nbntonio terceiro\n"
```

Eu cheguei a me bater com isso antes de descobrir que tem que chamar um
`close_write` antes de tentar ler do pipe ... ainda não descobri se dá pra ler
do pipe antes de terminar de escrever (pra poder por exemplo controlar outro
programa interativamente).
