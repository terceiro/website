---
title: 'monitorando a bateria na tora'
created_at: "2008-03-06 16:11 -3"
kind: article
old: true
---

Bom, o plugin monitor de bateria pro XFCE

[não está funcionando com o Linux 2.26.4](http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=466058), de forma que eu precisei dar um jeito de monitorar a bateria.

Eu tinha visto em algum lugar que o *Jeito Certo* e portável de se monitorar (e

usar) hardware era usando HAL, então decidi fazer um hack pra resolver meu

problema por hora. Dei uma futucada nos comandos disponíveis e saiu isso:

```language-shell
 #!/bin/sh

##################################################
# CUSTOMIZE HERE AS YOU WISH
##################################################

power_supply_alert(){
 aplay /usr/share/orage/sounds/Spo.wav
}

##################################################
# YOU SHOULDN'T NEED TO CHANGE ANYTHING BELOW HERE
##################################################

# battery
BATTERY_DEVICE=$(hal-find-by-property --key info.category --string battery | head -n 1)
BATTERY_PERCENT=$(hal-get-property --udi ${BATTERY_DEVICE} --key battery.charge_level.percentage)

# AC Adapter
AC_DEVICE=$(hal-find-by-property --key info.category --string ac_adapter | head -n 1)
AC_STATUS=$(hal-get-property --udi ${AC_DEVICE} --key ac_adapter.present | sed -e 's/true/on-line/; s/false/off-line/')

if [[ ${BATTERY_PERCENT} -lt 10 ]] && [[ ${AC_STATUS} == 'off-line' ]]; then
  power_supply_alert
fi

echo "AC: ${AC_STATUS} BAT: ${BATTERY_PERCENT}%"
```

Daí estou usando esse script com o "monitor genérico" do XFCE, que você configura com um comando e uma periodicidade, e ele mostra no painel a saída padrão do comando.
