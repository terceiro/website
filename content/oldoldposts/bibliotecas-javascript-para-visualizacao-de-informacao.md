---
title: 'bibliotecas javascript para visualização de informação'
created_at: "2008-10-21 20:49 -3"
kind: article
old: true
---

-   [js-Treemap](http://js-treemap.sourceforge.net/): visualização de Treemaps em Javascript puro.
-   [jsviz](http://code.google.com/p/jsviz): visualização de informações de árvores/grafos em geral. Parece o prefuse pra Java, mas em Javascript (e um pouco mais limitado, por consequencia).

Ambos produzem resultados bem legais. :-o
