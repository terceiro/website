---
title: 'I Workshop de Software Livre de Vitória da Conquista: eu fui'
created_at: "2007-03-18 23:06 -3"
kind: article
old: true
---

Nesse último final de semana, 15 a 17 de março de 2007, aconteceu o
[I Workshop de Software Livre de Vitória da Conquista](http://www.daccuesb.com.br/).
O evento foi uma realização do Diretório Acadêmico de Ciência da
Computação da [UESB](http://www.uesb.br/). Infelizmente só pude ficar até o
final do dia 16, pois no Sábado precisava resolver questões pessoais em
Salvador.

A convite da organização, participei de duas atividades, ambas no dia 16.

Durante o dia organizei uma oficina de [TWiki](http://twiki.org/).
Aconteceram alguns problemas, entre eles um misterioso consumo de recursos que
se repetiu travando duas máquinas diferentes, e usando versões diferentes do
TWiki. Eu nunca tinha visto isso, mas ao menos serviu pra testar minha
capacidade de resolver problemas sob pressão. :-) Ao final, apesar das interrupções não planejadas, acho que a oficina
cumpriu o seu papel de dar uma oportunidade de conhecer a ferramenta,
demonstrar algumas das possibilidades no seu uso, e possibilitar que quem tenha
interesse possa depois continuar o aprendizado sozinho. Além de dar uma
passeada pelo básico do uso do TWiki (formatação, uso e definição de
variáveis), consegui mostrar o desenvolvimento de um blog simples no TWiki e personalização de layout,
durante o quê pude abordar alguns conceitos muito básicos de
TWikiApplications, HTML, CSS, arquitetura de aplicações web. O material da oficina (um *tarball* com a web onde fiz os exemplos) está [aqui](http://wiki.softwarelivre.org/pub/Blogs/BlogPostAntonioTerceiro20070318230638/OficinaUESB.tar.gz), e para todos os efeitos está sob domínio público.

Durante a noite, fiz uma palestra sobre "Viabilidade Econômica do Software
Livre", onde além mostrar as possibilidades e oportunidades de prestação de
serviços com software livre, tentei mostrar que é possível (e necessário)
transpor a lógica da cooperação presente no desenvolvimento de software livre
para as relações de trabalho, discutindo temas fundamentais que eu aprendi com
meu amigo [Vicente](http://wiki.softwarelivre.org/Blogs/VicenteAguiar), como a contradição clássica do
capitalismo (trabalho X capital), e os princípios do cooperativismo como uma
forma de libertação do trabalhador através da cooperação com seus pares.
Ao final e apresentei também o caso da [Colivre](http://www.colivre.com.br/),
relatando nossas dificuldades e conquistas nesse primeiro ano de estrada.

Queria dar os parabéns à organização, e agradecer novamente o convite. Todos
foram muito atenciosos e solícitos comigo. O evento foi ótimo, e com certeza é
um passo importante na criação de um calendário de troca de informações e
experiências sobre software livre na região sudoeste. O desafio (como o de
todos os outros eventos organizados por voluntários) agora é conseguir mais
gente pra ajudar, de forma que fique mais fácil e menos cansativo pra cada
pessoa na organização. Procurar novas pessoas na UESB, procurar colegas e
professores de outras faculdades da cidade, profissionais interessados ...
todas as possibilidades precisam ser testadas.

Pra finalizar, uma reflexão: volta e meia na organização do
[Festival](http://festival.softwarelivre.org/) a gente discute como seria
possível levar o festival para o interior. Quase sempre chegamos a um ponto na
discussão onde se constata o fato de que é preciso que haja pessoas mobilizadas
no local do evento pra que o negócio dê certo. Mas se há pessoas mobilizadas
num local, a meu ver elas podem realizar o seu próprio evento, de acordo com
suas própria conveniência e suas características locais. Se viermos a ter um
calendário de eventos de software livre na Bahia, isso vai ser muito mais
importante para a criação de massa crítica em software livre no estado do que
ter um único evento itinerante.

Espero poder participar do II Workshop de Software Livre de Vitória da Conquista!
:-)
