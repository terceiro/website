---
title: 'leituras legais'
created_at: "2006-06-22 01:22 -3"
kind: article
old: true
---

Trazido até você (até mim, na verdade) pelo [Felipe Fonseca](http://fff.hipercortex.com/).

[The 12 Principles of Collaboration](http://www.mongoosetech.com/realcommunities/12prin.html) da Mongoose Technology, Inc. Um resumo muito legal sobre os princípios que "regem" comunidades em geral.

[The Cluetrain manifesto](http://cluetrain.com/). Um site que virou livro, o livro hoje tá todo online. Colocando na fila de leitura pra quando der ... prioridade 64532987546. 💀
