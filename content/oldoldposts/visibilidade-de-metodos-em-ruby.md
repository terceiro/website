---
title: 'visibilidade de métodos em Ruby'
created_at: "2007-08-06 15:13 -3"
kind: article
old: true
---

Em Ruby, `protected` e `private` têm um significado um pouco diferente do tradicionalmente implementado em outras linguagens (ditas) orientadas a objeto.

[Jamis Buck escreveu](http://weblog.jamisbuck.org/2007/2/23/method-visibility-in-ruby) um post bastante informativo sobre o assunto.
