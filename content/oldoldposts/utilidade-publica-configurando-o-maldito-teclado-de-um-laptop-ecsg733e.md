---
title: 'utilidade pública: configurando o maldito teclado de um laptop ECSG733E'
created_at: "2006-04-06 04:00 -3"
kind: article
old: true
---

Depois que minha bolsa acabou, comecei a pegar alguns trampos aleatórios pra tirar uma grana e me manter aqui em Porto Alegre, enquanto não volto pra Salvador.

Esses dias foi instalar um [laptop ECS G733E](http://www.ecs.com.tw/ECSWeb/Products/ProductsDetail.aspx?detailid=441&amp;amp;MenuID=0&amp;amp;LanID=0). Tudo foi tranquilíssimo. Debian etch, Gnome 2.12 (aliás, um desktop *coerentíssimo*. Muito bom).

Só o teclado do treco é que é história. Ele parece abnt2, mas não é. É aquele teclado que não dá pra quem usa muito shell: pra entrar uma barra você tem que digitar `"Atl Gr"+q`, ou `Fn+;`. Bizarro. Além disso ele tem um "teclado numérico" no meio das letras do lado direito, acionado junto com xev.

Buscando no deus Google eu cheguei na [solução desse cara](http://alexrosa.blogspot.com/2005/12/linux-no-notebook-g733.html), mas que por algum motivo desconhecido não funcionou. A solução foi estudar o `xmodmap`, capturar os keycodes com o `xev` e, depois de fazer alguns testes no método de tentativa e erro, criar um `Xmodmap` global (`/etc/X11/Xmodmap`) com o seguinte conteúdo (`!`'s iniciam comentários):



```
! q + slash
keycode 24 = q Q slash slash slash
! w + question mark
keycode 25 = w W question question question
! e + degree sign
keycode 26 = e E degree degree degree


! numbers:
keycode 90 = 0
keycode 87 = 1
keycode 88 = 2
keycode 89 = 3
keycode 83 = 4
keycode 84 = 5
keycode 85 = 6
keycode 79 = 7
keycode 80 = 8
keycode 81 = 9

! other "keypad" symbols
keycode 91 = comma
keycode 63 = asterisk

! vim: ft=xmodmap
```
