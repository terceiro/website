---
title: 'em casa de ferreiro, o espeto é de pau'
created_at: "2008-02-21 15:02 -3"
kind: article
old: true
---

Na [ACM Digital Library](http://portal.acm.org/dl.cfm), procurar por

`(A) and (B) and (C)` dá resultados diferentes de procurar por

`((A) and (B)) and (C)`,que dá resultados diferentes de procurar por

`(((A)) and (B)) and (C)`.

Genial. Eu ainda estou tentando entendar a lógica doentia por trás dessa joça.
