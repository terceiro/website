---
title: 'listando novidades sobre o twiki.softwarelivre.org'
created_at: "2007-08-06 13:46 -3"
kind: article
old: true
---

Estou criando uma listagem de informes administrativos sobre o [twiki.softwarelivre.org](http://twiki.softwarelivre.org), onde eu vou informar sobre novos plugins instalados, novas configurações, novas funcionalidades, upgrades, e coisas do tipo.

Este é um post inicial pra anunciar isso e também pra testar a listagem dos informes na página inicial. :-)

O que foi feito:

1.  instalado [TagMePlugin](http://wiki.softwarelivre.org/TWiki/TagMePlugin), pra eu poder marcar quais posts do meu blog têm a ver com o twiki.softwarelivre.org. De quebra os usuários ganham a possibilidade de usar esse plugin.
2.  alterei o template de posts pra mostrar as tags (e permitir que o usuário possa adicioná-las).
3.  incluí a listagem de informes na página inicial, com direiro a feed RSS.

Por enquanto a criação de tags é restrita. Eu criei umas genéricas, quando alguém sentir falta de poder criar tags por si só eu penso numa forma de retirar essa restrição.
