---
title: 'Pra ler quando der tempo'
created_at: "2005-08-18 21:03 -3"
kind: article
old: true
---

Tomara que dê tempo logo.

Politica e linguagem nos debates sobre o software livre, Dissertação de
mestrado em linguística de Rafael de Almeida Evangelista, área de concentração
Analise do Discurso.

> **Resumo:**
>
> Esta dissertação procura refletir discursivamente sobre os debates que
> tratam das vantagens e desvantagens da adoção de sistemas livres em
> computadores (software livre). A partir de uma concepção que considera que
> o acontecimento de linguagem é um acontecimento político, procura-se
> entender como e onde o político inscreve-se nesse debate. São objeto de
> análise nesse trabalho as licenças de software (proprietário e livre);
> artigos, entrevistas e notícias; e Projetos de Lei que postulam a adoção
> prioritária de sistemas livres em órgãos governamentais. É também
> estabelecida uma reflexão sobre o uso de certos termos e nomes (GNU/Linux,
> software livre) e não outros (Linux, código aberto) na referência aos
> objetos do debate e, para isso, investiga-se a história dos sentidos a eles
> atribuídos.

<http://libdigi.unicamp.br/document/?code=vtls000349663>
