---
title: 'post obrigatório sobre o fisl9.0'
created_at: "2008-04-24 02:52 -3"
kind: article
old: true
---

Aff, demorou mas saiu.

Bom, o [fisl9.0](http://fisl.softwarelivre.org/9.0) foi diferente de todos os
outros. Não estar mais na organização me fez ver um outro evento muito
diferente: nada de milhões de coisinhas que dão errado, correria pra todo lado,
fulaninho reclama aqui, ciclaninha reclama ali ... tudo isso é coisa do
passado. Esse ano foi só curtição: assisti todas as palestras que quis, e as
que eu perdi foi porquê conversar com alguém num estande ou simplesmente ficar
hackeando um pouquinho tava bom.

O estande do Debian estava muito bom, as coisas vendendo que nem água. Eu quero
dar efusivos parabéns ao pessoal que organizou o estande: até onde eu percebi
foi o faw, a Carol e o Frolic; se teve mais alguém foi mal aí a omissão. A
assinature de chaves esse ano foi bem legal: estou com um backlog de várias
chaves pra assinar, prometo que faço assim que der. ;-)

Fiz duas palestras:

- [Certificação: como ficam os desenvolvedores de Software Livre?](http://fisl.softwarelivre.org/9.0/papers/pub/programacao/665), onde eu tentei mostrar porque eu acho que o modelo atual de certificação, em geral, não faz sentido pra um desenvolvedor de software livre.
- [Engenharia de Software e Software Livre: tudo a ver!](http://fisl.softwarelivre.org/9.0/papers/pub/programacao/402), onde eu apresentei conceitos importantes de Engenharia de Software (Qualidade, Evolução e Arquitetura), mostrei desafios (problemas) da comunidade do Software Livre com relação a esses temas, e discuti também casos de sucesso relacionados.

No mais, foi maravilhoso rever amigos e poder trocar idéias.
