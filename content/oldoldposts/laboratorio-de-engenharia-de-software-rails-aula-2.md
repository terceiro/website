---
title: 'Laboratório de Engenharia de Software: Rails, aula #2'
created_at: "2009-05-06 01:30 -3"
kind: article
old: true
---

![Screenshot: um teste funcional](http://wiki.softwarelivre.org/pub/Blogs/BlogPostAntonioTerceiro20090506013000/aula2-small.png "Um teste funcional"){width="320" height="225"}

*(veja também: [aula \#1](http://wiki.softwarelivre.org/Blogs/BlogPostAntonioTerceiro20090502023201))*

Nesta aula:

 

-   testes de integração, testes funcionais e testes unitários
-   validações do ActiveRecord
-   renderizar ou redirecionar
-   criando layout para a aplicação

Hoje fiz uma [página de download](http://app.dcc.ufba.br/~terceiro/matb14/) mais ou menos arrumada para os vídeos, e gerei também versões em baixa resolução usando o [ffmpeg2theora](http://packages.debian.org/lenny/ffmpeg2theora). Reduzindo as duas dimensões do vídeo pela metade (de 912 x 640 para 456 x 320), consegui fazer uma versão pequena desta aula com 97 MB, sendo que o original tem 528 MB. Com certeza o vídeo é muito pior, mas dá pra assistir; praticamente não dá pra ler a barra de título das janelas, mas os demais textos com mais contraste (o conteúdo das janelas, em geral) tá tranquilo. Espero que facilite a vida de quem tiver restrições de banda ou espaço em disco/memória flash.

Espero que seja útil pra quem estiver interessado. Comentários são bem-vindos por e-mail (terceiro\@softwarelivre.org).

