---
title: 'Sinal de vida'
created_at: "2005-11-03 23:16 -3"
kind: article
old: true
---

Já tem um tempo que não posto nada ...

## Desenvolvendo

Nos últimos tempos, além de correr atrás de escrever a minha dissertação, andei
trabalhando um pouco no [TWiki](http://TWiki.org). Integrei suporte a internacionalização
de templates e tópicos, de forma que a [TWiki:Codev.DakarRelease](http://twiki.org/cgi-bin/view/Codev.DakarRelease "'Codev.DakarRelease' on TWiki.org") vai sair com suporte a 6
idiomas (até agora). Também escrevi 99% da tradução pra português, com a ajuda do pessoal
da [TWikiBrasil](http://twikibr.softwarelivre.org), principalmente do [Carlinhos Cecconi](http://wiki.softwarelivre.org/Blogs/CarlinhosCecconi).

Nessa brincadeira fui mais fundo do que nunca tinha ido em Perl, e acho que
estou apaixonado. ;-) É uma linguagem muito rica, e que implementa coisas
aparentemente muito complicadas (como orientação a objetos) de uma forma muito
simples e ortogonal.

Agora é seguir com os planos ([TWiki:Codev.PersonalRoadmapForAntonioTerceiro](http://twiki.org/cgi-bin/view/Codev.PersonalRoadmapForAntonioTerceiro "'Codev.PersonalRoadmapForAntonioTerceiro' on TWiki.org")) pra melhorar ainda mais o próximo release.

## Eventos

Começou a correria pro `fisl7.0`. Fomos outro dia conhecer o [centro de eventos da FIERGS](http://www.cexpo.com.br/), o troço é gigantesco. Tem uma infra-estrutura mostruosa.

Lançamos a [chamada de trabalhos](http://fisl.softwarelivre.org/7.0/www/?q=node/27),
e junto publicamos o [sistema usado](http://papers.softwarelivre.org) na chamada. Falta escrever uma perninha dele pra melhorar a avaliação de propostas (i.e., separá-la da administração do sistema). O resto da organização tá a milhão também, mas eu não quero nem ver. ;-)

Aproveitando o embalo, o [Festival Software Livre da Bahia](http://festival.softwarelivre.org) deve usar o mesmo sistema. Se tudo der certo, já estarei de volta [em casa](http://ba.softwarelivre.org) na época do Festival.
