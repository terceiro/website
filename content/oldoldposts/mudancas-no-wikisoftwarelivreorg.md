---
title: 'mudanças no wiki.softwarelivre.org'
created_at: "2008-11-19 02:54 -3"
kind: article
old: true
---

Conforme eu relatei [antes](/2008/11/19/goodbye-twiki-welcome-foswiki/), o projeto antes conhecido como TWiki agora chama-se [Foswiki](http://foswiki.org) (ou se você preferir, a comunidade que antes desenvolvia o que era chamado de "TWiki" agora desenvolve algo chamado "Foswiki") . Desta forma comecei a fazer algum *rebranding* no [wiki da Associação Software Livre.Org](http://wiki.softwarelivre.org/) para refletir a nova situação das coisas.

1. o antigo hostname twiki.softwarelivre.org agora é obsoleto. Usaremos daqui em diante [wiki.softwarelivre.org](http://wiki.softwarelivre.org/). O antigo ainda funciona, mas redireciona para o novo.
   - isso pode quebrar os feeds RSS da [web Blogs](http://wiki.softwarelivre.org/Blogs) com leitores de RSS que não suportem redirecionamento. Até onde pude ver o [newsbeuter](http://packages.debian.org/newsbeuter) e o [planet](http://packages.debian.org/planet) se viram com os redirecionamentos.
2. fiz algumas mudanças de logo, especialmente na [capa](http://wiki.softwarelivre.org/) e na web [Blogs](http://wiki.softwarelivre.org/Blogs).
3. o software ainda é o TWiki. Farei upgrade para o Foswiki assim que sair a primeira versão, com o *rebranding* feito.

Qualquer coisa estranha escrevam para `wikiwebmaster@softwarelivre.org`.

Não espero terminar isso tão cedo, mas o passo inicial foi dado. Quem quiser ajudar a tirar "TWiki" de todo o canto, sinta-se em casa.

**UPDATE:** s/FOSWiki/Foswiki/

**UPDATE 2:** [Valessio](https://valessiobrito.com.br/) fez a gentileza de refazer o logo de cabeçalho:

![Logo do wiki.softwarelivre.org](/oldoldposts/images/asl-foswiki.png)
