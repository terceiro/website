---
title: 'case insensitive matching em bancos relacionais livres'
created_at: "2008-04-29 22:53 -3"
kind: article
old: true
---

Umas das coisas legais de usar [Rails](http://rubyonrails.org/) é que você pode com relativamente pouco esforço testar sua aplicação com diferentes bancos de dados. Se você estiver usando construções portáveis (i.e. usando ActiveRecord em vez de SQL na mão), muito provavelmente sua aplicação vai funcionar igual em qualquer banco. Dessa forma na Colivre por exemplo a maioria de nós desenvolve usando [SQLite](http://www.sqlite.org/), mas fazemos deploy das aplicações usando [PostgreSQL](http://www.postgresql.org/). O único overhead que isso adiciona é, antes de lançar uma nova versão, rodar a suíte de testes contra o PostgreSQL pra garantir que nenhum código não-portável com relação a banco de dados foi incluído.

Dessa vez não deu certo: enquanto [SQLite](http://www.sqlite.org/lang_expr.html) e [MySQL](http://dev.mysql.com/doc/refman/5.1/en/case-sensitivity.html) são case insensitive ao processar uma cláusula LIKE, por exemplo, enquanto o [PostgreSQL](http://www.postgresql.org/docs/8.3/interactive/functions-matching.html) é case-sensitive. Graças a São Ignúcios a gente tem uma suíte de testes bem sólida e eu tinha incluído testes de case sensitiviness antes de ver a aplicação explodindo na cara dos usuários. :-)
