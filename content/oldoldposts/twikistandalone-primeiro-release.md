---
title: 'TWikiStandAlone: primeiro release!'
created_at: "2007-10-29 12:27 -3"
kind: article
old: true
---

[TWikiStandAlone](http://twiki.org/cgi-bin/view/Codev/TWikiStandAlone "'Codev/TWikiStandAlone' on TWiki.org") é o projeto de conclusão de curso de [Gilmar](http://twiki.org/cgi-bin/view/Main/GilmarSantosJr "'Main/GilmarSantosJr' on TWiki.org"), que eu estou orientando. Ele pegou idéias do [Rails](http://www.rubyonrails.org/) e principalmente do [Catalyst](http://www.catalystframework.org/), e implementou no TWiki pra torná-lo independente do mecanismo de execução, de forma que o TWiki pode ser adaptado a várias configurações. Foram implementados os seguintes mecanismos:

- CGI
- CLI (*command line interface*, usado por exemplo pra implementar *cron jobs* que manipulam dados do TWiki).
- ModPerl
- FastCGI
- HTTP

Os três últimos trazem a vantagem de manter o TWiki em memória, evitando a recompilação do código Perl a cada requisição. Os dois últimos são os que apresentam melhor desempenho, considerando número de requisições servidas por segundo (mais de 100% de melhora em relação a CGI).

O mecanismo HTTP é especialmente útil para desenvolvedores: basta fazer um checkout do código e executar o modo HTTP. Não precisa configurar apache nem nada.

UPDATE: Gilmar [fez um anúncio](http://listas.softwarelivre.org/pipermail/twikibr/2007-October/000991.html) na lista TWiki Brasil.
