---
title: 'me rendi ao github'
created_at: "2008-10-21 16:33 -3"
kind: article
old: true
---

Nunca fui simpático à idéia de hospedar código em lugares onde eu não confio cegamente e/ou eu mesmo não administro os servidores. Mas desta vez não deu pra resistir mais e eu [aderi](http://github.com/terceiro) à modinha do github. A grande vantagem é lidar com projetos que já são hospedados lá.

Como o git é distribuído, então eles nunca vão ter nada que eu não tenho também; isso me deixa mais tranquilo (sim, eu sou paranóico).

Primeiras impressões:

-   clonar repositórios é muito prático.
-   chamar "clone" de "fork" é muito tosco.
-   a funcionalidade de "pull request" é muito boa.

Uma hora dessa vou dar uma olhada no [Gitorius](http://gitorious.org/) também.
