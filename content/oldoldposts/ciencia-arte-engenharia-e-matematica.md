---
title: 'Ciência, Arte, Engenharia e Matemática'
created_at: "2007-01-03 23:05 -3"
kind: article
old: true
---

Devaneios filosóficos-wannabe ...

Hoje resolvi aproveitar um tempo de chá de cadeira pra ler um texto que eu já estava me devendo a um tempo:
["Is Computer Science Science?"](http://cs.gmu.edu/cne/pjd/PUBS/CACMcols/cacmApr05.pdf)
(*Ciência da Computação é Ciência?*). Ele foi publicado no [Communications of the ACM](http://acm.org/cacm/)
de Abril de 2005. Junto com a leitura mais ou menos recente do também excelente
[The Craft of Research](http://www.amazon.com/Research-Chicago-Writing-Editing-Publishing/dp/0226065847),
me deu uma nova visão sobre que eu estou fazendo (ou tentando fazer).

"Is Computer Science Science?" mostra, convincentemente (IMHO), que apesar da aparente descrença interna Computação é
Ciência *também*. A parte aplicada se assemelha a Engenharia e a Arte, mas que em muitos campos de pesquisa existe o uso
do método científico: realizar observações, formular hipóteses e testar essas hipóteses através de experimentos, com o objetivo
de aumentar o conhecimento de determinado campo de pesquisa.

Uma coisa que me intrigou no "Is Computer Science Science?" foi a distinção que algumas pessoas fazem entre
Engenharia, Ciência e Matemática. Diabos, e Matemática não é Ciência? Nisso cheguei no ótimo
[Is Mathematics Science?](http://andrewlias.blogspot.com/2004/08/is-mathematics-science.html), que argumenta
que apesar de não transparecer nas suas publicações, os matemáticos seguem sim o método científico. As diferenças
com relação às outras ciências (por exemplo, o fato da observação e dos experimentos não serem realizados sobre
objetos físicos palpáveis) seriam especificidades com as de outros campos.

Leituras que valem à pena. "Is Computer Science Science?" e "Is Mathematics Science?" são rápidos e fáceis de ler.
O "The Craft of Research" é muito maior, e muito mais denso. Tem que ter tempo pra ler pelo menos duas vezes, e
depois guardar ele na cabeceira ( ou colocar de volta na estante da orientadora ;-) ) pra consultas futuras.
