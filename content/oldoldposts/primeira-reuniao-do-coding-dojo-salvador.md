---
title: 'primeira reunião do Coding Dojo Salvador'
created_at: "2008-05-13 15:28 -3"
kind: article
old: true
---

Quando eu descobri o conceito de [Coding Dojo](http://www.codingdojo.org/),
me deu uma [vontade monstra](/2008/03/05/coding-dojos/) de fazer algo do tipo em Salvador.

[Depois do fisl9.0](http://fisl.softwarelivre.org/9.0/papers/pub/programacao/570) foi difícil conter essa vontade. Daí que a idéia saiu do papel, afinal. Ontem aconteceu a [primeira reunião](http://wiki.dcc.ufba.br/bin/view/DojoSalvador/Reuniao20080512) do [Coding Dojo Salvador](http://wiki.dcc.ufba.br/bin/view/DojoSalvador). Apeser de ter pouca gente, foi muito legal. No começo eu fiz uma pequena apresentação sobre o que é, como acontece, e depois partimos pra resolver o problema.

Segunda que vem tem mais.
