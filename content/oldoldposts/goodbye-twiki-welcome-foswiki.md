---
title: 'goodbye TWiki, welcome Foswiki'
created_at: "2008-11-19 02:43 -3"
kind: article
old: true
---

This post comes pretty late. I had it as a draft and didn't had the time to finish before. It's not as polished as I wanted, but here it goes anyway.

Right to the point: I left the TWiki project.

Now the history. The following is the ["Relaunch TWiki.org Project"](http://twiki.org/cgi-bin/view/Codev/RelaunchTWikiOrgProject "'Codev/RelaunchTWikiOrgProject' on TWiki.org") letter by Peter Thoeny and Tom Barton, frozen here to keep the current state of the situation and my feelings about it:

 

> There are some tremendous opportunities for our project. In order to take full advantage of these, today Peter Thoeny and the management team at TWIKI.NET decided to re-launch the TWiki.org project with a new governance model.
>
> We want to reach out to all community members to explain:
>
> - why we decided to do this
> - some thoughts on our new top level direction
> - our invitation to re-confirm your membership in the community
>
> There have been many recent positive developments in the community. We are now at 10,000 downloads a month with our latest release, which contains these major new features:
>
> - important usability improvements including a world-class WYSWIG editor
> - SQL-like query language
>
> These are great strides and everyone in the community should all be very proud of the current software. But this is just the beginning!
>
> It became obvious that to take full advantage of the new opportunities surrounding enterprise collaboration a much broader agenda is required for the project. This will attract additional participation in the community, and allow us to improve our competitive position. To address these opportunities also requires a change in governance model to establish clear project direction. The new governance model is based on the Ubuntu project and can be found at [TWikiGovernance](http://twiki.org/cgi-bin/view/Codev/TWikiGovernance "'Codev/TWikiGovernance' on TWiki.org").
>
> The new governance model also addresses branding and trademark questions. There is a delicate balance between the community needs and what is required to maintain a strong brand. TWiki has a strong brand which can be extended using the Ubuntu model. Ubuntu has clear branding guidelines, whereas under Debian, a brand cannot be protected. New [TWikiCommunity](http://twiki.org/cgi-bin/view/Codev/TWikiCommunity "'Codev/TWikiCommunity' on TWiki.org") friendly guidelines on TWiki trademark use will be worked out and announced by the newly formed [TWikiCommunityCouncil](http://twiki.org/cgi-bin/view/Codev/TWikiCommunityCouncil "'Codev/TWikiCommunityCouncil' on TWiki.org").
>
> In recent months, it became apparent that the community lacked clear leadership and was moving toward a "Debian" style of governance, which we do not believe would be healthy for the long run interests of the community. A picture is worth a thousand words:
>
> ![debian-ubuntu-s.png](/oldoldposts/images//debian-ubuntu-s.png)
>
> The charter of the project is now broadened because of the tremendous need of enterprises to go to the next level in collaboration. The major areas of focus are:
>
> - Addressing the full range of enterprise Web 2.0 collaboration needs (not just the wiki).
> - Setting clear open standards that encourage 3rd party contribution - think the enterprise equivalent of [OpenSocial](http://twiki.org/cgi-bin/view/Codev/OpenSocial "'Codev/OpenSocial' on TWiki.org")
> - Expanded enterprise features ([MySQL](http://twiki.org/cgi-bin/view/Codev/MySQL "'Codev/MySQL' on TWiki.org") backend, [SharePoint](http://twiki.org/cgi-bin/view/Codev/SharePoint "'Codev/SharePoint' on TWiki.org") integration, full internationalization)
>
> Detail on the expanded charter can be found at [TWiki.TWikiCharter](http://twiki.org/cgi-bin/view/TWiki/TWikiCharter "'TWiki/TWikiCharter' on TWiki.org") and [TWikiRoadMap](http://twiki.org/cgi-bin/view/Codev/TWikiRoadMap "'Codev/TWikiRoadMap' on TWiki.org").
>
> Because the scope and governance of our project has been substantially modified, we are asking all community members to go to [AgreeToTermsOfUse](http://twiki.org/cgi-bin/view/Main/AgreeToTermsOfUse "'Main/AgreeToTermsOfUse' on TWiki.org") to:
>
> - show support for the expanded project charter,
> - agree with the new governance model,
> - agree to the terms of use for twiki.org.
>
> Peter Thoeny & Tom Barton

Although I admire Peter's work and his people skills, I must point out that I never liked the whole history of trying to force a Ubuntu-like governance for TWiki. Being a Debian contributor for some time now, I feel pretty aware of Debian/Ubuntu situation and feel that the Ubuntu model does not apply to TWiki because:

1. The Ubuntu governance model works because in Ubuntu, Canonical employs the largest share of core Ubuntu developers. TWIKI.NET (Peter's company), on the other hand, does not employ the majority of the people who actually contribute code to TWiki. Actually, all relevant developers are leaving the TWiki project and are involved with the fork.
2. Ubuntu was -- and is -- made possible because of Debian's work. Even if currently there are important contributions that are developed in the context of Ubuntu that get back into Debian, most of the important things still goes the other way around.
3. Ubuntu and Debian have different goals: while Debian wants to be "The Universal Operating System", Ubuntu wants to be the "Linux for human beings".

The worst thing, besides the desire to apply a model that works in a completely different environment, was the way everything was put by Peter. Closing everyone out of the door and requiring them to ask to come back was too much. A relationship that were already fragile was broken.

Although I didn't follow the facts as close as I wanted to, the fact that ***all*** relevant former TWiki developers left the project makes me pretty sure that the way Peter/TWIKI.NET conducted this was the worst possible.

![Foswiki logo](/oldoldposts/images/foswiki-logo.png "Foswiki - Free and Open Source Wiki")

Then the new project was born, with a work name of "Nextwiki", which had to be changed ASAP because "TWiki" is a registered trademark of Peter Thoeny, and he demanded that the new project don't use anything that looks like "TWiki". The things are running as fast as possible, and today it was announced the new name: Foswiki. The process for choosing the new name was very democratic and was supported by a professional copywriter. The donain name for the new name is already [set up](http://foswiki.org/).

There is a [more detailed explanation](http://foswiki.org/About/WhyThisFork) of the whole process that led to the fork. Actually it's not a fork, since all relevant people that were involved in TWiki development are now involved with Foswiki development. It's the same project, now with a different name because of a trademark issues.

On the TWiki side, things seem to be not good at all. The development mailing list traffic in the last two weeks is composed almost 100% of failing unit tests messages sent automatically by the -- now probably unatended -- integration server), which looks to me as a sad, silent and unnecessary death. Too bad.

Former TWiki users are invited to migrate to Foswiki as soon as there is a first release, which will mainly include a rebranding (i.e. removing all mentions to "TWiki"). FOSWIki will be able to deal with all your current content in TWiki and will evolve much more vividly than ever before, without breaking your content and applications.

Long life to Foswiki.

**UPDATE:** the Right Way to capitalize is "Foswiki". This is interesting because people ***always*** capitalized TWiki wrong, so ... we are not repeating the mistakes from the past. !;-)

