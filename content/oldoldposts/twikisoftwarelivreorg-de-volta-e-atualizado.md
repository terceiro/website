---
title: 'twiki.softwarelivre.org de volta e atualizado'
created_at: "2008-06-26 20:44 -3"
kind: article
old: true
---

Depois de alguns dias com problemas, o [twiki.softwarelivre.org](http://twiki.softwarelivre.org) está de volta. Além de uma nova máquina, temos também uma nova versão do TWiki (4.2), que tem várias melhorias inclusive de desempenho.

O único problema visível até agora é o layout da [web Blogs](http://wiki.softwarelivre.org/Blogs/WebHome) ... Amadeu? !;-)

***update (Seg Jun 30 20:41:48 BRT 2008):*** tivemos alguns problemas com a nova
máquina mas já estamos de volta. Continuamos, no entanto, rodando a versão mais
recente do TWiki.
