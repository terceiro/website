---
title: 'servidor Jabber: faça você mesmo.'
created_at: "2006-08-03 01:33 -3"
kind: article
old: true
---

Estou fazendo um trampo que onde vamos ter que configurar chat numa aplicação web. Hoje em dia, falou chat, pensou Jabber. Olha que maravilha: o [JWChat](http://jwchat.sourceforge.net/) é um cliente Jabber via web, todo feito com Javascript e AJAX.

Ele depende de um serviço Jabber chamado *HTTP polling*, que basicamente é um gateway HTTP pra um serviço Jabber "normal". O [ejabberd](http://ejabberd.jabber.ru/) é um servidor web escrito em

[Erlang](http://en.wikipedia.org/wiki/Erlang_programming_language) e que já tem suporte *HTTP polling*. O ejabberd é o servidor livre com segundo melhor "Feature Score"

[listado no Jabber.org](http://www.jabber.org/software/servers.shtml).

O primeiro colocado eu nem cogitei, mas prefiro me abster de dizer o porquê. !;-)

A instalação do ejabberd foi tranquila, via APT mesmo.

Daí faltava configurar o JWChat. Segui [as instruções](http://jwchat.org/README),

mas não havia jeito do `http-poll` funcionar. A configuração do apache2 acabou ficando

meio diferente do README:

```
<VirtualHost *>
  ServerName localhost
  DocumentRoot /home/terceiro/src/jwchat-1.0beta2
  <Directory /home/terceiro/src/jwchat-1.0beta2>
    Options +Indexes +MultiViews
    AddDefaultCharset UTF-8
  </Directory>
  RewriteEngine On
  ProxyRequests On
  <Proxy *>
    Order Deny,Allow
    Deny from All
    Allow from 127.0.0.1
  </Proxy>
  RewriteRule http-poll/ http://127.0.0.1:5280/http-poll/ [P]
</VirtualHost>
```

Daí foi criar contas pelo Gaim mesmo (no JWChat dá também), e depois testar

falar de um pro outro.

![JWChat e Gaim lado a lado.](http://wiki.softwarelivre.org/pub/Blogs/BlogPostAntonioTerceiro20060803013344/screenshot-jwchat+gaim.png)

Funciona que é uma maravilha ... viva o software livre e os padrões abertos.

Falta customizar a autenticação no ejabberd. Ele é tranquilo de botar pra funcionar

o básico, mas tem pouca ou nenhuma documentação pra além do feijão com arroz.
