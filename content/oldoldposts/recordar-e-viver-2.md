---
title: 'recordar é viver #2'
created_at: "2007-07-20 18:55 -3"
kind: article
old: true
---

Mais um programinha das antigas.

A [curva de Koch (ou floco de neve de Koch, ou estrela de Koch)](http://pt.wikipedia.org/wiki/Curva_de_Koch)
é um [fractal](http://pt.wikipedia.org/wiki/Fractal) dos mais simples.

Esse programa é da época em que havia uma mania por fractais na UFBA (a long, long time ago), e não faz nada
além de desenhar uma curva de Koch.

![fltk-koch exibindo a curva de Koch](/oldoldposts/images/fltk-koch-curva.gif)

Ele também desenha a curva de Koch na versão "floco de neve", que é simplesmente a junção de três curvas
em forma de triângulo:

![fltk-koch exibindo a curva de Koch](/oldoldposts/images/fltk-koch-floco-de-neve.gif)

~~Código fonte aqui], licenciado sob a GPL v3. Também foi escrito em C++ usando FLTK.~~

Eu lembro que ainda nos tempos tenebrosos eu fiz um programa do
[fractal de Mandelbrot](http://pt.wikipedia.org/wiki/Conjunto_de_Mandelbrot) em [Delphi](http://en.wikipedia.org/wiki/Borland_Delphi), mas esse eu acho que se perdeu. Mesmo
que não tivesse se perdido, eu pensaria duas vezes antes de publicar algo com dependências
não-livres ... ! ;-)

**UPDATE (2020):** este post foi migrado de uma plataforma que quebrou e não tinha backup. a não ser que eu ainda tenha aquele CD, este código-fonte foi perdido pra sempre.
