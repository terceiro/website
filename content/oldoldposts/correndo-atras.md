---
title: 'correndo atrás'
created_at: "2006-02-23 18:12 -3"
kind: article
old: true
---

estou na correria pra terminar minha dissertação de mestrado ... tenho até o dia 28 de fevereiro, que por sinal é terça-feira de carnaval. Ainda bem que em Porto Alegre, diferente de Salvador, o carnaval é quase um feriado normal.

Pra completar, minha máquina de casa deu um xabu umas semanas atrás e até hoje está parada. Minha placa mãe queimou, e estou esperando o maluco da assistência receber uma placa mãe que ele encomendou. Espero que até amanhã esteja tudo certo, porque ficar sem computador em casa no feriadão que antecede o deadline mortal vai ser f\#&% ...
