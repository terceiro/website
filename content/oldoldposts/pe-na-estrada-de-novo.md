---
title: 'pé na estrada, de novo'
created_at: "2006-12-21 20:00 -3"
kind: article
old: true
---

Uau.

Acabou de sair o [resultado da seleção](http://dmcc.dcc.ufba.br/listaSelecionados2007) para a primeira turma do [Doutorado Multiinstitucional em Ciência da Computação](http://dmcc.dcc.ufba.br/).

Fiquei muito feliz de ter sido aceito, basicamente por dois motivos:

-   vou trabalhar com a [professora Christina](http://www.dcc.ufba.br/~flach/), que foi minha orientadora de TCC na graduação, com quem atualmente [trabalho](http://twiki.im.ufba.br/bin/view/Tema) como pesquisador bolsista da [FAPESB](http://www.fapesb.ba.gov.br/).
-   vou trabalhar com um tema que me motiva profundamente. Adivinha? **SOFTWARE LIVRE**. Pretendo investigar, do ponto de vista da Engenharia de Software, a Evolução de projetos de Software Livre, levando em conta questões relacionadas à Qualidade de Software (no bom sentido).

Vem aí muito trabalho pela frente.
