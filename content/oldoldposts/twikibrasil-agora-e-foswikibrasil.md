---
title: 'TWikiBrasil agora é FoswikiBrasil'
created_at: "2008-12-14 01:58 -3"
kind: article
old: true
---

![Logo do TWikiBrasil derretendo](/oldoldposts/images/TWikiBrasil.png)

[Foi criada uma nova lista](http://listas.softwarelivre.org/pipermail/foswiki-br/2008-December/000000.html), a web foi [renomeada para Foswikibr](http://listas.softwarelivre.org/pipermail/foswiki-br/2008-December/000001.html), e o Carlinhos [já está tocando o barco](http://listas.softwarelivre.org/pipermail/foswiki-br/2008-December/000002.html) como coordenador da tradução para português, que é só um nome bonito para o papel que ele já vinha desempenhando há um bom tempo no TWiki. Se tivemos o TWiki em bom português nos últimos sei lá quantos releases, devemos a ele.

Bola pra frente que com a gente vem mais gente. :-)

Espero conseguir tempo de contribuir com código para o [Foswiki](http://foswiki.org/) em breve, o projeto está num gás sensacional.
