---
title: 'links aleatórios, legais ou não: julho de 2007'
created_at: "2007-07-20 19:39 -3"
kind: article
old: true
---

[Mais valia 2.0](http://www.dicas-l.com.br/zonadecombate/zonadecombate_20070714.php), por Rafael Evangelista. O Rafael mantém uma coluna no site dicas-l chamada [Zona de Combate](http://www.dicas-l.com.br/zonadecombate/), onde ele publica textos muito legais relacionados a software livre e militância social (definição minha).

[Contact Improvisation](http://www.contactimprov.net/). Homenagem a [lucasr](http://blogs.gnome.org/lucasr). :-)

[Vim Taglist plugin](http://vim-taglist.sourceforge.net/): preciso começar a usar esse troço.
