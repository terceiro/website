---
title: 'dando manutenção: só você acessa seu site'
created_at: "2006-07-27 15:28 -3"
kind: article
old: true
---

Acabei de atualizar <http://twiki.softwarelivre.org/> para a versão 4.0.4 do TWiki.

Entre as melhorias dessa versão em relação à que estava instalada anteriormente
(4.0.0), estão vários bugs corrigidos, uma
[PatternSkin](http://wiki.softwarelivre.org/TWiki/PatternSkin) muito mais
coerente, entre outros.

Isso significa que a [página do Festival](http://festival.softwarelivre.org/)
está no ar de novo. !;-)

Para fazer essa manutenção eu fiz uma configuração bastante interessante pra
que eu pudesse acessar o TWiki pela web mas pra que todos os outros não
conseguissem, isso sem fazer o armengue de limitar o acesso no apache pelo IP.

Essa configuração (no apache2) consiste em limitar o acesso através da
máquina local, e usar o `mod_proxy` para conseguir acessar usando o browser
do seu desktop como se você estivesse na máquina local.

Seguem os trechos interessantes de configuração do apache2. Assumindo que
o módulo `mod_proxy` está habilitado.

Dentro do seu *virtualhost*, você coloca esse trecho para habilitar um proxy
autenticado:

```
ProxyRequests On
ProxyVia On
<Proxy *>
  AuthType Basic
  AuthName 'your.host.tld proxy'
  AuthUserFile /path/to/my/.htpasswd
  Require user JohnDoe
</Proxy>
```

E pra que o usuário possa ver a mensagem de "o servidor está em manutenção":

```
<Location />
  Order Deny,Allow
  Deny from all
  Allow from 127.0.0.1

  ErrorDocument 403 /maintainance/index.html
</Location>
```

Daí é só dizer no `/etc/hosts` do servidor para resolver o próprio nome para
`localhost`:

```
127.0.0.1 localhost twiki.softwarelivre.org
```

Quando você acessa pelo browser e recebe a mensagem de "servidor em
manutenção". Para conseguir acessar o servidor, você precisa habilitar acesso
via proxy, e usar o endereço do próprio servidor como proxy.

Aí é só acessar o servidor normalmente. Da primeira vez o navegador te pede
usuário e senha para autenticar o acesso ao proxy, mas daí pra frente você
passa batido (como qualquer autenticação `Basic`).

Como funciona: como você acessa o site pelo proxy que está no próprio servidor,
quando o servidor recebe a requisição pra todos os efeitos ela está vindo de
`localhost` (porque para o servidor, o *hostname* dele é resolvido para
127.0.0.1), e daí o seu acesso passa.

Uma dica é colocar essa configurações num arquivo separado, e dar um `Include`
no arquivo de configuração principal. Quando terminar a manutenção e for colocar
o site no ar, daí é só comentar a linha do `Include`.
