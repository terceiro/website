---
title: 'web Blogs funcionando'
created_at: "2005-08-20 21:07 -3"
kind: article
old: true
---

Achei bem legal [o blogzinho](http://gnosislivre.org/twiki/bin/view/Aurium/BlogAurium)
que [Aurélio](http://gnosislivre.org/twiki/bin/view/Aurium)
fez no [TWiki](http://TWiki.org). Por curiosidade eu fui dar um saque no código. Muito
criativo, mas infelizmente não dava pra compartilhar o código com outro blog. Assim eu
resolvi fazer esse aqui pra brincar. Na pior das hipóteses, testar ele me motiva a escrever
um blog em português.

Assim, está disponível a [web Blogs](http://wiki.softwarelivre.org/Blogs/WebHome) no `twiki.softwarelivre.org`, com espaço
pras pessoas fazerem blogs. As grandes vantagens, a meu ver, é poder usar todo
o poder de formatação do TWiki, e poder criar praticamente qualquer coisa a mais.

Comentários estão na *wishlist*. Depois preciso empacotar direitinho, traduzir e disponibilizar
no <http://TWiki.org> .

O endereço: <http://twiki.softwarelivre.org/bin/view/Blogs>.

Basta fazer registro no `twiki.softwarelivre.org` antes.

Se alguém tiver um feedback, me escreva: `asaterceiro` *em* `inf.ufrgs.br`.
Depois dessa tenho que correr pra terminar um artigo pro SBMF'05 (Simpósio Brasileiro de
Métodos Formais), o prazo é segunda-feira. Espero ter cabeça no próximo fim de semana
pra trabalhar um pouco no [TWiki:Plugins/BibliographyPlugin](http://twiki.org/cgi-bin/view/Plugins/BibliographyPlugin "'Plugins/BibliographyPlugin' on TWiki.org") e no [TWiki:Plugins/TopicTranslationsPlugin](http://twiki.org/cgi-bin/view/Plugins/TopicTranslationsPlugin "'Plugins/TopicTranslationsPlugin' on TWiki.org").
