---
title: 'Laboratório de Engenharia de Software: Rails, aula #3'
created_at: "2009-05-19 20:00 -3"
kind: article
old: true
---

![Aula 3: ActiveLdap](http://wiki.softwarelivre.org/pub/Blogs/BlogPostAntonioTerceiro20090519200029/aula3-small.png){width="320" height="225"}

*(veja também: [aula \#1](http://wiki.softwarelivre.org/Blogs/BlogPostAntonioTerceiro20090502023201), [aula \#2](http://wiki.softwarelivre.org/Blogs/BlogPostAntonioTerceiro20090506013000))*

Nesta aula:

 

-   [Sistema Gerenciador de Grupos](http://disciplinas.dcc.ufba.br/MATB14/SistemaGerenciadorDeGrupos)
-   Bases de dados LDAP, [OpenLAP](http://www.openldap.org/)
-   [ActiveLdap](http://ruby-activeldap.rubyforge.org/)
-   Teste de Integração

Os arquivos para baixar estão no [lugar de sempre](http://app.dcc.ufba.br/~terceiro/matb14/). Desta vez tem também o [código-fonte da aplicação](http://wiki.softwarelivre.org/pub/Blogs/BlogPostAntonioTerceiro20090519200029/sgg.tar.gz) (Copyright © 2009, Antonio Terceiro, licenciado sob os termos da [licença MIT](http://en.wikipedia.org/wiki/MIT_License)).

Como antes, comentários são bem-vindos.

