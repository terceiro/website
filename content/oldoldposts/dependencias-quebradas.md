---
title: 'dependências quebradas'
created_at: "2006-03-07 17:39 -3"
kind: article
old: true
---

Um tempo atrás eu fui furtado aqui em Porto Alegre e perdi todos os documentos. Estando a 3000 km
da origem dos meus documentos, eu imaginava que tinha me fudido, mas não imaginava que era tanto.

Eu até consegui tirar um RG gaúcho por aqui, pra poder ser alguém (senão não ia poder nem pegar o avião pra voltar). Mas vou ter que tirar uma 2ª via do da Bahia quando chegar, já que toda a minha vida está cadastrada sob aquele número.

O pior é que eu precisava tirar segunda via do resto mesmo antes de voltar, e fui conferir o que precisa. Dá um saque:

Documento | Requisitos | Onde | Por procuração
----------|------------|------|---------------------------------
[Habilitação](http://www.detran.ba.gov.br/habilitacao/passos.php?passo=8) | documento de identificação (original e fotocópia), **CPF** (original e fotocópia) e comprovante de residência | Ciretran/Detran/SAC/etc | ?
[CPF](http://poupaclique.ig.com.br/materias/000001-000500/252/252_1.html) | RG e **Título de eleitor** | Qualquer agência do BB, da Caixa ou dos Correios | Sim
[Título de eleitor](http://www.caixa.gov.br/Cidadao/Servicos/Documentos_Pessoais/Asp/Tit_Eleit.asp) | provar a identidade (RG, etc) | Cartório Eleitoral ou o Posto Eleitoral do local de sua residência | *Não*


A Habilitação depende do CPF. O CPF até dá pra ser por procuração, mas depende do título de eleitor, que não dá pra ser por procuração. Ou seja: segunda via dessa parada toda só em Salvador ... ou então eu podia trocar de domicílio eleitoral a dois meses de voltar. Seria o máximo.

Nem só na [Debian unstable](http://www.debian.org/releases/unstable/) acontecem
dependências quebradas ...
