---
title: 'covardia: TWiki + vim'
created_at: "2006-08-11 16:59 -3"
kind: article
old: true
---

Depois de ver essas instruções de como usar o [vim](http://www.vim.org) pra
editar caixas de texto no Firefox, fui catar e achei um
[arquivo de sintaxe](http://www.vim.org/scripts/script.php?script_id=1113)
para o TWiki. Genial, usar o vim pra escrever no TWiki. ![hehe!](http://wiki.softwarelivre.org/pub/TWiki/SmiliesPlugin/devil.gif "hehe!")

Para reconhecer os arquivos como TWiki você pode ou usar `:set ft=twiki`
no vim, ou então fazer um script pra chamar o vim já setando e usá-lo como o
editor no [ViewSourceWith](http://dafizilla.sourceforge.net/viewsourcewith/index.php):

```language-shell
#!/bin/sh

gvim -c 'set ft=twiki' $@
```

Daí é só ligar os bits de execução desse script (`chmod +x`) e apontar pra ele
na configuração do ViewSourceWith (em vez de `/usr/bin/gvim`).

Fiz [umas mudanças](http://twiki.org/cgi-bin/viewfile/Codev/VimEditor?rev=1;filename=twiki.vim.patch.20060811) que deram uma boa [melhorada](http://twiki.org/cgi-bin/rdiff/Codev/VimEditor?rev1=23;rev2=22)
no troço.
