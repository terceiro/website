---
title: 'Reflexão sobre o Tabuleiro Digital, e sobre projetos de inclusão digital em geral'
created_at: "2008-12-19 18:56 -3"
kind: article
old: true
---

Achei por acaso no blog [No blog do professor Nelson Pretto](http://nelsonpretto.livejournal.com/38651.html). Sensacional, vale à pena a leitura.

Pra dar um gostinho, um parágrafo no comecinho:

> Primeiro, permitam-me uma questão que balizará toda essa conversa: por que os filhos das classes média e alta podem ter acesso ao universo da internet, na privacidade de seus quartos, com banda larga, suporte via telefone e computadores poderosos para fazer um monte de coisas como baixar músicas, mixá-las, distribuí-las, jogar videogames online, conversar com amigos velhos e novos, visitar e interagir com sites às vezes não tão adequados segundo os adultos - que aliás, um dia já viram as mesmas coisas em gibis escondidos dentro dos livros escolares! -, e, os filhos dos pobres, têm que acessar internet em telecentros para serem treinados (com projetos pedagógicos) em word e excel (aliás, softwares proprietários que lhes "escravizarão" para o todo e sempre...)?!

