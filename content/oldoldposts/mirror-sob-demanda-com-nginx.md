---
title: 'mirror sob demanda com nginx'
created_at: "2008-04-01 04:28 -3"
kind: article
old: true
---

Eu estou testando a geração de imagens de CD do
[BrDesktop](http://brdesktop.org/) mas não tenho disco pra manter um mirror
local. Tentei usar apt-proxy, apt-cacher-ng, mas todos fizerem o simple-cdd dar
pau porquê pra construir o CD são necessários subdiretórios no mirror (`doc`, `tools`, etc) que essas ferramentas não suportam.

Decidi então procurar uma solução pra fazer algo parecido com o que o apt-proxy e o apt-cacher-ng fazem, mas pra todo o mirror e não só pro pool/ e dists/. Procurando no [scroogle](http://www.scroogle.org/) achei esse post sobre um [mirror sob demanda usando o nginx](http://hostingfu.com/article/nginx-and-mirror-demand). [nginx](http://nginx.net/) é mais um desses servidores web *light* que prometem pouco uso de memória e bom desempenho.

Resolvi tentar, e não é que depois de futucar um pouco e dar uma mexida na
configuração do cara, deu certo? A configuração do nginx ficou assim:


```
server {
   listen 80;
   server_name localhost;

   access_log /var/log/nginx/localhost.access.log;

   location /debian {
      root /home/terceiro/mirror;
      error_page 404 403 = /fetch$uri$args;
   }

   location /fetch {
      proxy_pass http://ftp.br.debian.org;
      proxy_store on;
      proxy_store_access user:rw group:rw all:rw;
      proxy_temp_path /home/terceiro/mirror;
      alias /home/terceiro/mirror;
   }

}
```

Tem uma limitação que é: de vez em quando eu preciso apagar os arquivos `Release`
e `Packages*` pra pegar as atualizações do repositório, e fiz um script `clean.sh`
que fica na raiz do mirror pra fazer isso:

```language-shell
 #!/bin/sh

find . -name Release\* -exec rm -f '{}' ';'
find . -name Packages\* -exec rm -f '{}' ';'
```

Daí uso `http://localhost/debian` como meu mirror Debian, e o simple-cdd se
entende com ele que é uma beleza a imagem é gerada. Ela ainda não instala, mas
vamos chegar lá. :-)
