---
title: 'ferramentas livres para gerenciamento de bibliografia'
created_at: "2009-03-31 20:42 -3"
kind: article
old: true
---

Nos últimos dias eu li o clássico *Como Se Faz Uma Tese*, de [Umberto Eco](http://pt.wikipedia.org/wiki/Umberto_eco). Ele propunha um esquema super sofisticado pra catalogar a bibliografia em fichas; ainda que eu não pretenda investir em fichas de papel hoje em dia (o livro é de 1977, então computador não estava nem próximo da realidade naquele contexto), eu percebi que organizar a bibliografia de uma forma sistemática é fundamental pra poder desenvolver qualquer trabalho direito.

Resolvi tomar jeito na vida e começar a organizar minhas referências, e fui à caça de ferramentas pra isso. Segue uma tabela que fiz pra comparar as opções que eu encontrei empacotadas no Debian:

 

*Ferramenta*

*Descrição*

*Abrir/importar .bib*

*Salvar .bib*

*Facilidade de edição*

*Possibilidade de incluir resumo/etc*

[bibcursed](http://bibcursed.sourceforge.net/)

aplicativo para console (ncurses); projeto antigo

Sim

Sim

Tosco

Não

[referencer](http://icculus.org/referencer/index.html)

aplicativo Gtk+; projeto recente

Sim

Sim

Simples e rápido

Sim

[kbibtex](http://www.unix-ag.uni-kl.de/~fischer/kbibtex/index.html)

aplicativo KDE/Qt;

Sim

Sim

Completo, mas complexo

Sim

Resolvi ficar com o referencer. Achei ele bem agradável e prático, suporta tags, suporta várias formas de importar ... vamos ver.

![Screenshot do referencer](http://wiki.softwarelivre.org/pub/Blogs/BlogPostAntonioTerceiro20090331204256/referencer.png){width="640" height="351"}  
Janela do referencer.

Eu conheço ainda o [JabRef](http://jabref.sourceforge.net/), mas nem testei porquê não está na seção main ainda.

**update:** coloquei um screenshot bem mais legal, depois de começar a usar o referencer de verdade, já com tags e tal.

