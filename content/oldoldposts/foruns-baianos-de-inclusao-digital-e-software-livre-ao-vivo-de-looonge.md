---
title: 'Fóruns Baianos de Inclusão Digital e Software Livre ao vivo de looonge ...'
created_at: "2005-09-26 18:48 -3"
kind: article
old: true
---

Heh, estou escutando a [transmissão ao vivo](http://proxy02.pop-ba.rnp.br:8000/live.m3u)
pela [Rádio FACED](http://twiki.ufba.br/twiki/bin/view/RadioFACED). Tá muito legal.

Parabéns aos organizadores do evento por mais essa realização. Com certeza a
raça danada que vocês têm dado pra fazer coisas com esses fóruns acontecerem
contribuem muito para o crescimento do Software Livre na Bahia.

Parabéns ao pessoal da Rádio FACED pela a transmissão.
