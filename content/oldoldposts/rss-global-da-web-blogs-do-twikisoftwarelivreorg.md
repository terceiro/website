---
title: 'RSS global da web blogs do twiki.softwarelivre.org'
created_at: "2006-02-23 18:40 -3"
kind: article
old: true
---

Na tentativa de me desalienar um pouco e voltar a ter algum contato com a realidade, ontem eu passei uns
15 minutos de descanso da dissertação implementando o [RSS global](http://twiki.softwarelivre.org/bin/view/Blogs/WebRss?contenttype=text/xml) da
[web Blogs do twiki.softwarelivre.org](http://twiki.softwarelivre.org/bin/view/Blogs).

Ainda falta fazer uma política de cache disso como é feita para os feeds individuais, mas a parte que
precisava de algum cérebro tá feita. Inclusive consegui incluir o texto dos posts via INCLUDE
direto do tópico em vez de incluir a resposta de uma requisição HTTP, o que melhorou o desempenho
consideravelmente. Uma hora dessa vou reproduzir isso nos feeds individuais.
