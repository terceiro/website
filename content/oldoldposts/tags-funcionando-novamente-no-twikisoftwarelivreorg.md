---
title: 'tags funcionando novamente no twiki.softwarelivre.org'
created_at: "2007-11-12 00:37 -3"
kind: article
old: true
---

um tempo atrás eu percebi que as tags pararam de funcionar no twiki.softwarelivre.org, mas não tinha tido tempo de futucar pra ver porquê.

Descobri que eu [tinha quebrado](http://wiki.softwarelivre.org/Blogs/BlogPostOldLayoutTemplate?raw=on&amp;rev=3), o barato tentando melhorar e nem tinha percebido. Acabei de [consertar](http://wiki.softwarelivre.org/Blogs/BlogPostOldLayoutTemplate?raw=on&amp;rev=6) e agora tá funcionando.

Aproveitei também pra consertar a listagem de novidades na [página inicial](http://twiki.softwarelivre.org/) que não estava ordenando com a mais recente em cima.
