---
title: About me
---

# About me

<img alt="A picture of me" src="/photo.jpg" style="float: left; margin-right: 2em; margin-bottom: 2em"/>

- Free software developer and [mentor](/mentoring/)
- [Debian](https://debian.org/) project member
- Work: Senior Software Engineer at [Linaro](https://linaro.org/)
- From: [Salvador](https://en.wikipedia.org/wiki/Salvador,_Bahia)
- [GPG key](https://keys.openpgp.org/vks/v1/by-fingerprint/B2DEE66036C40829FCD0F10CFC0DB1BBCD460BDE)

<hr style="clear: both"/>

## Formal Bio

If you need a formal bio you can use the following text:

> Antonio Terceiro is a Free Software developer and a member of the Debian
> Project. He has a Ph.D. in Computer Science from the Federal University of
> Bahia (UFBA), and a M.Sc. in Computer Science from the Federal University of
> Rio Grande do Sul (UFRGS). Antonio is currently a Senior Software Engineer at
> Linaro Limited, working on platforms for Software Quality Assurance. His
> interests include Free Software, FLOSS development, Software Engineering and
> Software Quality Assurance.

Or in Brazilian Portuguese:

> Antonio Terceiro é desenvolvedor de Software Livre e membro do projeto
> Debian.  Ele é Doutor em Ciência da Computação pela Universidade Federal da
> Bahia (UFBA) e Mestre em Ciência da Computação pela Universidade Federal do
> Rio Grande do Sul (UFRGS). Atualmente é Engenheiro de Software Sênior na
> Linaro Limited, e onde trabalha em plataformas pra Controle de Qualidade de
> Software.  Seus interesses incluem Software Livre, Desenvolvimento de
> Software Livre e Código Aberto (FLOSS), Engenharia de Software e Controle de
> Qualidade em Software.
