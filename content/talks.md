# Talks

In this page you can find slides from conference talks I have given.

## 2021

* *Debian: o que é, como funciona e como contribuir*. BOSS, Aug 2nd, 2021.
  | [Slides (PDF)](2021-08-02-debian-boss.pdf)

## 2020

* *Como você pode ajudar o Debian, programando em Python*. Nov 2nd, 2020  --
  Python Brasil 2020
  | [Slides (PDF)](2020-11-02-como-ajudar-o-debian-programando-em-python.pdf)
  | [Video (YouTube)](https://www.youtube.com/watch?v=4UWWaH0x2nU)
