---
title: "When community service providers fail"
created_at: "2020-08-07 22:00 -3"
kind: article
---

I'm starting a new blog, and instead of going into the
[technical details on how it's made](https://salsa.debian.org/terceiro/website),
or on how I migrated my content from the previous ones, I want to focus on why
I did it.

It's been a while since I have written a blog post. I wanted to get back into
it, but also wanted to finally self-host my blog/homepage because I have been
let down before. And sadly, I was not let down by a for-profit, privacy
invading corporation, but by a free software organization.

## The sad story of wiki.softwarelivre.org

My first blog that was hosted in a blog engine written by me, which was hosted
in a [TWiki, and later Foswiki](/2008/11/19/goodbye-twiki-welcome-foswiki/)
instance previously available at `wiki.softwarelivre.org`, hosted by
[ASL.org](http://softwarelivre.org/asl).

I was the one who introduced the tool to the organization in the first place.
I had come from a [previous, very fruitful
experience](http://wiki.dcc.ufba.br/) on the use of wikis for creation of
educational material while in university, which ultimately led me to become a
core TWiki, and then Foswiki developer.

In 2004, I had just moved to Porto Alegre, got involved in ASL.org, and there
was a demand for a tool like that. 2 years later, I left Porto Alegre, and some
time after that the daily operations of ASL.org when it became clear that it
was not really prepared for remote participation. I was still maintaing the
wiki software and the OS for quite some years after, until I wasn't anymore.

In 2016, the server that hosted it went haywire, and there were no backups. A
lot of people and free software groups lost their content forever.
My blog was the least important content in there. To mention just a few
examples, here are some groups that lost their content in there:

- The Brazilian Foswiki user group hosted a bunch of getting started
  documentation in Portuguese, organized meetings, and coordinated through
  there.
- GNOME Brazil hosted its homepage there until the moment the server
  broke.
- The Inkscape Brazil user group had an *amazing* space there where they shared
  tutorials, a gallery of user-contributed drawings, and a lot more.

Some of this can still be reached via the
[Internet Archive Wayback Machine](http://web.archive.org/web/20180824071149/http://wiki.softwarelivre.org/),
but that is only useful for recovering content, not for it to be used by the
public.

## The announced tragedy of softwarelivre.org

[My next blog after that](http://softwarelivre.org/terceiro) was hosted at
`softwarelivre.org`, a
[Noosfero](https://gitlab.com/noosfero/noosfero/) instance also hosted by
ASL.org.
When it was introduced in 2010, this Noosfero instance became responsible for
the main domain softwarelivre.org name. This was a bold move by ASL.org, and
was a demonstration of trust in a local free software project, led by a local
free software cooperative ([Colivre](http://colivre.coop.br/)).

I was a lead developer in the Noosfero project for a long time, and I was also
involved in maintaining that server as well.

<!-- TODO fact check -->
However, for several years there is little to no investment in maintaining that
service. I already expect that it will probably blow up at some point as the
wiki did, or that it may be just shut down on purpose.

## On the responsibility of organizations

Today, a large part of wast mot people consider "the internet" is controlled by
a handful of corporations. Most popular services on the internet might look
like they are *gratis* (free as in beer), but running those services is
definitely not without costs. So when you use services provided by for-profit
companies and are not paying for them with money, you are paying with your
privacy and attention.

Society needs independent organizations to provide alternatives.

The market can solve a part of the problem by providing ethical services and
charging for them. This is legitimate, and as long as there is transparency
about how peoples' data and communications are handled, there is nothing wrong
with it.

But that only solves part of the problem, as there will always be people who
can't afford to pay, and people and communities who can afford to pay, but
would rather rely on a nonprofit. That's where community-based services,
provided by nonprofits, are also important. We should have more of them, not
less.

So it makes me worry to realize ASL.org left the community in the dark. Losing
the wiki wasn't even the first event of its kind, as the
`listas.softwarelivre.org` mailing list server, with years and years of
community communications archived in it,
[broke with no backups in 2012](http://softwarelivre.org/portal/noticias/comunicado-sobre-as-listas-softwarelivre.org).

I do not intend to blame the ASL.org leadership personally, they are all well
meaning and good people. But as an organization, it failed to recognize the
importance of this role of service provider. I can even include myself in it: I
was member of the ASL.org board some 15 years ago; I was involved in the
deployment of both the wiki and Noosfero, the former as a volunteer and the
later professionally. Yet, I did nothing to plan the maintenance of the
infrastructure going forward.

When well meaning organizations fail, people who are not willing to have their
data and communications be exploited for profit are left to their own devices.
I can afford a virtual private server, and have the technical knowledge to
import my old content into a static website generator, so I did it. But what
about all the people who can't, or don't?

Of course, these organizations have to solve the challenge of being
sustainable, and being able to pay professionals to maintain the services that
the community relies on. We should be thankful to these organizations, and
their leadership needs to recognize the importance of those services, and
actively plan for them to be kept alive.
