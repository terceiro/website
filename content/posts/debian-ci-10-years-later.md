---
title: "Debian CI: 10 years later"
created_at: "'2023-12-28 15:00' -3"
kind: article
tags: [debian, debian-ci]
---

It was 2013, and I was on a break from work between Christmas and New Year of
2013. I had been working at [Linaro][linaro] for well over a year, on the [LAVA
project][lava]. I was living and breathing automated testing infrastructure,
mostly for testing low-level components such as kernels and bootloaders, on
real hardware.

[linaro]: https://www.linaro.org/
[lava]: https://www.lavasoftware.org/

At this point I was also a Debian contributor for quite some years, and had
become an official project members two years prior. Most of my involvement was
in the Ruby team, where we were already consistently running upstream test
suites during package builds.

During that break, I put these two contexts together, and came to the
conclusion that Debian needed a dedicated service that would test the contents
of the Debian archive. I was aware of the existance of
[autopkgtest][autopkgtest], and started working on a very simple service that
would later become [Debian CI][ci.d.n].

[autopkgtest]: https://packages.debian.org/sid/autopkgtest
[ci.d.n]: https://ci.debian.net/

In January 2014, `debci` was initially announced on that month's [Misc
Developer News][ann], and later [uploaded to Debian][itp]. It's been
continuously developed for the last 10 years, evolved from a single shell
script running tests in a loop into a distributed system with 47
geographically-distributed machines as of writing this piece, became part of
the official Debian release process gating migrations to testing, had 5 Summer
of Code and Outrechy interns working on it, and processed beyond 40 million
test runs.

[ann]: https://lists.debian.org/debian-devel-announce/2014/01/msg00007.html
[itp]: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=736416

In there years, Debian CI has received contributions from a lot of people, but
I would like to give special credits to the following:

- Ian Jackson - created autopkgtest.
- Martin Pitt - was the maintainer of autopkgtest when Debian CI launched and
  helped a lot for some time.
- Paul Gevers - decided that he wanted Debian CI test runs to control testing
  migration. While at it, became a member of the Debian Release Team and the
  other half of the permanent Debian CI team together with me.
- Lucas Kanashiro - Google Summer of Code intern, 2014.
- Brandon Fairchild - Google Summer of Code intern, 2014.
- Candy Tsai - Outreachy intern, 2019.
- Pavit Kaur - Google Summer of Code intern, 2021
- Abiola Ajadi - Outreachy intern, December 2021-2022.
