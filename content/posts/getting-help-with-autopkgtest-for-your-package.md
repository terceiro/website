---
title: "Getting help with autopkgtest for your package"
created_at: "2021-07-19 21:00 -3"
kind: article
tags:
  - debian
  - debian-ci
  - autopkgtest
  - debconf
---

If you have been involved in Debian packaging at all in the last few years, you
are probably aware that autopkgtest is now an important piece of the Debian
release process. Back in 2018, the automated testing migration process started
[considering autopkgtest test results][migration-2018] as part of its decision
making.

[migration-2018]: https://lists.debian.org/msgid-search/4cb42bdc-ea03-57ca-1623-435d562f05ff@debian.org

Since them, this process has received several improvements. For example, during
the [bullseye freeze][freeze-2021], non-key packages with a non-trivial
autopkgtest test suite could migrate automatically to testing without their
maintainers needing to open unblock requests, provided there was no regression
in theirs autopkgtest (or those from their reverse dependencies).

[freeze-2021]: https://lists.debian.org/msgid-search/a3ce6189-5306-9019-d978-dba4419d1520@debian.org

[Since 2014 when ci.debian.net was first introduced][ci-2014], we have seen an
amazing increase in the number of packages in Debian that can be automatically
tested. We went from around 100 to 15,000 today. This means not only happier
maintainers because their packages get to testing faster, but also **improved
quality assurance for Debian as a whole**.

[ci-2014]: /2014/06/01/an-introduction-to-the-debian-continuous-integration-project/

![Chart showing the number of packages tested by ci.debian.net. Starts from close to 0 in 2014, up to 15,000 in 2021. The growth tendency seems to slow down in the last year](/images/ci-stats-2021.png "Packages tested by ci.debian.net on amd64")

However, the growth rate seems to be decreasing. Maybe the low hanging fruit
have all been picked, or maybe we just need to help more people jump in the
automated testing bandwagon.

With that said, we would like to encourage and help more maintainers to add
autopkgtest to their packages. To that effect, I just created the
[autopkgtest-help repository on salsa][autopkgtest-help], where we will take
help requests from maintainers working on autopkgtest for their packages.

[autopkgtest-help]: https://salsa.debian.org/ci-team/autopkgtest-help/

If you want help, please go ahead and [create an issue in there][create-issue].
To quote the repository README:

> Valid requests:
>
> - _"I want to add autopkgtest to package X. X is a tool that [...]  and it works
>   by [...]. How should I approach testing it?"_
>
>   It's OK if you have no idea where to start. But at least try to describe your
>   package, what it does and how it works so we can try to help you.
>
> - _"I started writing autopkgtest for X, here is my current work in progress
>   [link]. But I encountered problem Y. How to I move forward?"_
>
>   If you already have an autopkgtest but is having trouble making it work as
>   you think it should, you can also ask here.
>
> Invalid requests:
>
> - _"Please write autopkgtest for my package X for me"_.
>
>   As with anything else in free software, please show appreciation for other
>   people's time, and do your own research first. If you pose your question with
>   enough details (see above) and make it interesting, it _may_ be that whoever
>   answers will write at least a basic structure for you, but as the maintainer
>   you are still the expert in the package and what tests are relevant.

[create-issue]: https://salsa.debian.org/ci-team/autopkgtest-help/-/issues/new

If you ask your question soon, you might get your answer recorded in video: we
are going to have a [DebConf21 talk][debconf21-talk] next month, where we I and
Paul Gevers (elbrus) will answer a few autopkgtest questions in video for
posterity.

[debconf21-talk]: https://debconf21.debconf.org/talks/1-autopkgtest-office-hours/

Now, if you have experience enabling autopkgtest for you own packages, please
consider watching that repository there to help us help our fellow maintainers.
