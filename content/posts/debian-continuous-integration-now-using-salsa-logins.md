---
title: "Debian Continuous Integration now using Salsa logins"
created_at: "2021-06-27 13:00 -3"
kind: article
tags: [debian, debian-ci]
---

I have just updated the [Debian Continuous Integration
platform](https://ci.debian.net) with [debci 3.1][debci-31].

[debci-31]: https://tracker.debian.org/news/1243579/accepted-debci-31-source-into-experimental/

This update brings a few database performance improvements, courtesy of adding
indexes to very important columns that were missing them. And boy, querying a
table with 13 million rows without the proper indexes is bad! :-)

Now, the most user visible change in this update is the change from Debian SSO
to Salsa Logins, which is part of [Pavit Kaur][pavit]'s GSoC work. She has been
working with me and Paul Gevers for a few weeks, and this was the first
official task in the internship.

[pavit]: https://pavitkaur05.github.io/

For users, this means that you now can only log in via Salsa. If you have an
existing session where you logged in with an SSO certificate, it will still be
valid. When you log in with Salsa, your username will be changed to match the
one in Salsa. This means that if your account on salsa gets renamed, it will
automatically be renamed on Debian CI when you log in the next time.
Unfortunately we don't have a logout feature yet, but in the meantime you can
use the developer toolbar to delete any existing cookies you might have for
`ci.debian.net`.

Migrating to Salsa logins was in my TODO list for a while. I had the impression
that it could do it pretty quick to do by using pre-existing libraries that
provide gitlab authentication integration for Rack (Ruby's de facto standard web
application interface, like uwsgi for Python). But in reality, the devil was in
the details.

We went through [several rounds of reviews][mr-salsa] to get it right. During
the entire process, Pavit demonstrated an excelent capacity for responding to
feedback, and overall I'm very happy with her performance in the internship so
far.

While we were discussing the Salsa logins, we noted a limitation in the existing
database structure, where we stored usernames directly as the test `requestor`
field, and decided it was better to normalize that relationship with a proper
foreign key to the users table, which she [also worked on][mr-requestor-id].

[mr-salsa]: https://salsa.debian.org/ci-team/debci/-/merge_requests/179
[mr-requestor-id]: https://salsa.debian.org/ci-team/debci/-/merge_requests/181

This update also include the very first (and non-user visible) step of her next
task, which is adding support for having private tests. Those will be useful
for implementing testing for embargoed security updates, and other use cases.
This was broken up into 7 or 8 seperate steps, so there is still some
work to do there. I'm looking forward to the continuation of this work.
