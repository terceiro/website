---
title: "Useful ffmpeg commands for editing video"
created_at: "2020-08-15 22:30 -3"
kind: article
tags:
  - ffmpeg
  - debconf
  - audacity
  - pitivi
---

For [DebConf20](https://debconf20.debconf.org/), we are recommending that
speakers pre-record the presentation part of their talks, and will have live
Q&A. We had a smaller online MiniDebConf a couple of months ago, where for
instance I had connectivity issues during my talk, so even though it feels too
artificial, I guess pre-recording can decrease by a lot the likelihood of a
given talk going bad.

Paul Gevers and I submitted a short 20 min talk giving an update on
`autopkgtest`, `ci.debian.net` and friends. We will provide the latest updates
on autopkgtest, autodep8, debci, ci.debian.net, and its integration with the
Debian testing migration software, britney.

We agreed on a split of the content, each one recorded their part, and I
offered to join them together. The logical chaining of the topics is such that
we can't just concatenate the recordings, so we need to interlace our parts.

So I set out to do a full video editing work. I have done this before, although
in a simpler way, for one of the MiniDebconfs we held in Curitiba. In that
case, it was just cutting the noise at the beginning and the end of the
recording, and adding beginning and finish screens with sponsors logos etc.

The first issue I noticed was that both our recordings had a decent amount of
audio noise. To extract the audio track from the videos, I resorted to
[How can I extract audio from video with ffmpeg?](https://stackoverflow.com/questions/9913032/how-can-i-extract-audio-from-video-with-ffmpeg)
on Stack Overflow:

```
ffmpeg -i input-video.avi -vn -acodec copy output-audio.aac
```

I then edited the audio with [Audacity](https://www.audacityteam.org/). I
passed a noise reduction filter a couple of times, then a compressor filter to
amplify my recording on mine, as Paul's already had a good volume. And those
are my more advanced audio editing skills, which I acquired doing [my own
podcast](https://papolivre.org/).

I now realized I could have just muted the audio tracks from the original clip
and align the noise-free audio with it, but I ended up creating new video files
with the clean audio. Another member of the Stack Overflow family came to the rescue,
in [How to merge audio and video file in ffmpeg](https://superuser.com/questions/277642/how-to-merge-audio-and-video-file-in-ffmpeg). To replace the audio stream, we can do something like this:

```
ffmpeg -i video.mp4 -i audio.wav -c:v copy -c:a aac -map 0:v:0 -map 1:a:0 output.mp4
```

Paul's recording had a 4:3 aspect ratio, while the requested format is 16:9.
This late in the game, there was zero chance I would request him to redo the
recording.  So I decided to add those black bars on the side to make it the
right aspect when showing full screen. And yet again the quickest answer I could
find came from the Stack Overflow empire:
[ffmpeg: pillarbox 4:3 to 16:9](https://video.stackexchange.com/questions/17343/ffmpeg-pillarbox-43-to-169):

```
ffmpeg -i "input43.mkv" -vf "scale=640x480,setsar=1,pad=854:480:107:0" [etc..]
```

The final editing was done with [pitivi](http://www.pitivi.org/), which is what
I have used before. I'm a very basic user, but I could do what I needed.  It
was basically splitting the clips at the right places, inserting the slides as
images and aligning them with the video, and making most our video appear small
in the corner when presenting the slides.

P.S.: all the command lines presented here are examples, basically copied from
the linked Q&As, and have to be adapted to your actual input and output
formats.
