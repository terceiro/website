---
old: true
title: "Debian Ruby Sprint 2016 - day 3: Ruby 2.3 in unstable, Reproducible Builds, and Data Structures for Dinner Booths"
created_at: "2016-03-03 13:42 -3"
kind: article
tags: [debian, ruby]
---

Day 3 was again a full of useful work. Since the beginning of the sprint, we were able to fix more than 50 FTBFS¹ bugs, alongside general quality improvements in the packages.

> ¹ in the Debian jargon, FTBFS means that a package “fails to build from source”, which in Debian is a critical bug because users need to be able to produce binary packages from their source code to fully exercise the free software principles.
>

An important milestone that was also achieved on day 3 was [the upload of ruby-defaults 1:2.3.0+1](https://tracker.debian.org/news/752105), making `ruby2.3` the new default version of Ruby. That is the version that will shipped in the next Debian release, codenamed stretch. This is the culmination of a joint effort between the Ruby team and Debian Release Team that involves rebuilding a little more than 130 packages that use the Ruby C API to make sure everything will just work on upgrades, both from the previous stable release, and from earlier snapshots of the current development release.

Another small change that will have a big impact for Debian and for free software was [an improvement to gem2deb](https://anonscm.debian.org/cgit/pkg-ruby-extras/gem2deb.git/commit/?id=d93a18490e3c9359d24ba7dcbd2d2cbe377d0271) that fixes [a reproducibility issue in Ruby packages](https://tests.reproducible-builds.org/issues/unstable/timestamps_in_gemspec_files_issue.html) and will help currently more than 100 Ruby packages become [reproducible](https://reproducible-builds.org/).

The full list of items that have been worked on is this:

-   sponsor ruby-github-linguist. Closes: \#815908. Thanks balasankar
-   rebuild remaining failing packages – 55 remaining!
-   uploaded ruby-em-synchrony (FTBFS \#800029)
-   day 2 report blog post
-   ruby-certificate-authority-0.1.6-1 (\#816506)
-   filed for ruby-grack removal
-   uploaded ruby-em-synchrony
-   uploaded ruby-mathml (FTBS \#800029)
-   uploaded new upstream version of ruby-redis which claims better ruby2.2 support
-   uploaded pry (0.10.3-2), hoping it will build correctly on the buildd
-   uploaded ruby-hashery
-   rails security issues: CVE-2016-2098, CVE-2016-2097
    -   fixed in unstable on 2:4.2.5.2-1
    -   contacted security team for jessie update
-   uploaded ruby-ruby-engine, ruby-ruby-version, ruby-rspec-pending-for to NEW
-   updated ruby-hashie to new upstream version
-   ruby-remcached (fix FTBFS)
-   made the switch to ruby2.3 in unstable \*\*\*\*\*\*\*\*\*
-   filed https://bugs.ruby-lang.org/issues/12137 as ruby-awesome-print triggers this
-   uploaded ruby-rspec 3.4
-   uploaded ruby-rspec-rails 3.4
-   uploaded ruby-grack 2.0.2-2 (fix FTBFS)
-   ruby-memcache-client (\#797256) =\> RoM ruby-memcache-client (\#816538) + https://github.com/rack/rack/issues/1025
-   upload ruby-acts-as-lists (Closes: FTBS \#816482)
-   ruby-beautify: fixed and uploaded
-   filed for ruby-foreigner removal (+ fixed diaspora dependency)
-   uploaded newer ruby-origin to fix FTBFS
-   uploaded ruby-responders
-   gem2deb hacking
    -   dh-make-ruby will not overwrite debian/copyright anymore
    -   force generated gemspecs to use date from debian/changelog. fixes https://tests.reproducible-builds.org/issues/unstable/timestamps\_in\_gemspec\_files\_issue.html
    -   uploaded version 0.27 to unstable
-   make ruby-riddle tests run and work during build
-   ruby-flexmock 2.0.4-1 -\> uploaded
-   RoM ruby-memcache-client (done \#816538)
-   upload nanoc 4.1.4 and fix tests for ruby2.3
-   Upload new upstream version of ruby-test-after-commit
-   uploaded newer upstream for ruby-origin fixing FTBFS bug

The day ended at Outback, where we had an amount of beer that led us to formulate what we will now call the One-Sided Dinner Booth Problem.

![](/oldposts/images/outback.jpg)

In a party arranged like above, when the people closest to wall need to go alleviate themselves of some beer, you basically have to perform a removal from the bottom of a stack, which requires popping all the elements at the top. When they come back, you have to options:

-   place them back at their original position, in which case
    -   the people who stayed have to either stay standing or get up all over again (not sure what is worse)
    -   the odds are that the next person to leave will require less effort from the others
-   shift everyone to places against the wall and place the one coming back at the very end of the bench away from the wall, in which case
    -   the people who stayed are perhaps less annoyed, but you have to rearrange plates and pints
    -   it is less likely that the next time will require less effort from the others

The **One-Sided Dinner Booth Problem** is finding the optimal data structure and algorithm for this situation. It is postulated that this is an NP-complete problem, and that only probabilistic solutions are cost-effective.

