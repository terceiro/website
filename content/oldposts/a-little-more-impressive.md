---
old: true
title: "A little more impressive"
created_at: "2010-05-16 12:51 -3"
kind: article
---

[Impressive](http://impressive.sourceforge.net/) is a slide presentation application. Not a presentation authoring  application, it does very well a single job: presenting. To create the slides, you can use [LaTeX](http://www.latex-project.org/) and [Beamer](http://latex-beamer.sourceforge.net/), or export a PDF file from OpenOffice.org, or (put your favorite slide authoring tool here). Impressive is [packaged in Debian](http://packages.debian.org/impressive), and probably in other operating systems as well, so using it requires no effort at all.

Impressive has very nice features, such as an overview screen displaying thumbnails of all slides, zooming, highlighting specific areas of the slides, a spotlight that follows the mouse and even the possibility of scripting your presentations with arbitrary Python code.

Another great impressive feature is its collection of transition effects.I've just sent out a small contribution, adding a new transition effect: fade in/fade out: the current slide is faded out to black, and then faded the black in the next slide.

![Impressive-fadein-fadeout](/oldposts/images/impressive-fadein-fadeout.png?1274024321)

This seems to be the default transition effect in [Apple's Keynote](http://en.wikipedia.org/wiki/Keynote_%28presentation_software%29), and I must admit that it is very cool and I *wanted* it. :-)

So, with the [patch](/oldposts/files/impressive-fadeoutfadein.diff) I've just sent out, I hope to contribute with making impressive a little more impressive than it already is.

**update(17/05):** I already got a reply from Impressive's author Martin Fiedler, and the transition was already [included](http://impressive.svn.sourceforge.net/viewvc/impressive?view=rev&revision=32) in the subversion repository. His implementation is a lot better than mine. :-)

