---
old: true
title: "Chegando para o fisl10"
created_at: "2009-06-23 17:27 -3"
kind: article
tags: [fisl, colivre, noosfero]
---

[![](http://fisl.softwarelivre.org/10/www/files/banners/fullbanner1.gif)](http://fisl.softwarelivre.org/10/www/ "FISL10")

Já estou em Porto Alegre. :-)

O fisl10 promete: a [Colivre](http://www.colivre.coop.br/ "Colivre - Cooreativa de Tecnoologias Livres") está decendo em peso, vamos ter um estande na mostra de soluções para demonstrar nossos produtos e serviços. Vamos dar um enfoque especial ao [Noosfero](http://www.noosfero.org/ "Projeto Noosfero, Redes Sociais com Software Livre"), que hoje é a plataforma do [softwarelivre.org](/ "Software Livre Brasil") (entre outros sites). Estaremos também vendendo exemplares impressos do livro "[Software Livre, Cultura Hacker e Ecossistema da Colaboração](http://softwarelivre.org/livro)".

Vou dar duas palestras:

- ["Conheça o Noosfero - um software livre para redes sociais"](http://fisl.softwarelivre.org/10/papers/pub/programacao/411),  junto com [Vicente](http://softwarelivre.org/vicente). Nesta palestra vamos apresentar o projeto, suas funcionalidades, casos de sucesso e como colaborar com o projeto.

- ["Qualidade de Código: mantendo seu projeto de software livre sob controle"](http://fisl.softwarelivre.org/10/papers/pub/programacao/446). Nesta palestra eu vou falar sobre qualidade de projeto de software, no contexto em que eu venho trabalhando na minha pesquisa de doutorado. Além de discutir noções iniciais sobre alguns atributos de qualidade (tamanho, acoplamento, coesão e separação de interesses), vou demonstrar [a ferramenta na qual eu venho trabalhando](http://github.com/terceiro/egypt) e discutir boas práticas de projeto (os [princípios SOLID](http://butunclebob.com/ArticleS.UncleBob.PrinciplesOfOod), em especial), exemplificando com projetos de software livre com os quais eu estou familiarizado.

