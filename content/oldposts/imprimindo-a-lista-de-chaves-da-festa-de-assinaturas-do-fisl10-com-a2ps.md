---
old: true
title: "Imprimindo a lista de chaves da festa de assinaturas do fisl10 com a2ps"
created_at: "2009-06-23 19:13 -3"
kind: article
---

Makefile que eu fiz pra arrumar a lista de chaves da [festa de assinaturas do fisl10](http://wiki.softwarelivre.org/KSP/AnuncioFISL10KSP) num PDF, com duas colunas. Dá 4 páginas, fazendo frente e verso dá pra usar apenas duas folhas de papel. Eu tentei outros layouts, dava menos páginas mas a fonte não ficava legível pra mim. Você pode tentar variar isso trocando o -2 na chamada ao a2ps por -3, -4, etc.


```language-make
ksp10-keylist-print.pdf: ksp10-keylist-print.ps
	ps2pdf $< $@

ksp10-keylist-print.ps: ksp10-keylist-print.txt
	iconv -f utf8 -t iso88591 $< | a2ps -2 -o $@ --no-header -

view: ksp10-keylist-print.pdf
	evince $<

clean:
	rm -f ksp10-keylist-print.ps ksp10-keylist-print.pdf
```

O resultado eu não vou mostrar, porque segundo as instruções, você tem que **imprimir você mesmo** a lista de chaves.

No final das contas essa é uma forma interessante de imprimir qualquer arquivo de texto puro, o [a2ps](http://www.gnu.org/software/a2ps/) é muito bom especialmente pra imprimir código-fonte (não que eu recomende sair imprimindo código-fonte, ou que isso seja muito útil, mas ...). Aparentente ele não reconhece UTF-8, eu tentei `--encoding=utf-8` `--encoding=utf8` e não rolou. Mas apesar disso é uma ferramenta fantástica.

