---
old: true
title: "Mitigando Condições Adversas na Aplicação de Métodos Ágeis: o caso do projeto Noosfero"
created_at: "2009-10-11 20:52 -3"
kind: article
tags: [noosfero]
---

Slides da minha palestra no [Encontro Ágil 2009](http://www.encontroagil.com.br/). disponíveis [aqui](/oldposts/files/mitigando-condicoes-adversas-no-desenvolvimento-agil.odp).

"Mitigando Condições Adversas na Aplicação de Métodos Ágeis: o caso do projeto Noosfero" by [Colivre](http://colivre.coop.br) is licensed under a [Creative Commons Atribuição-Uso Não-Comercial-Vedada a Criação de Obras Derivadas 2.5 Brasil License](http://creativecommons.org/licenses/by-nc-nd/2.5/br/). Permissions beyond the scope of this license may be available at <http://colivre.coop.br>.

