---
old: true
title: "Noosfero Hacking Bar"
created_at: "2010-04-30 09:28 -3"
kind: article
tags: [colivre, noosfero]
---

A [Colivre](http://colivre.coop.br/) convida a todos para o primeiro Noosfero Hacking Bar, no próximo dia 08/05 (um sábado), às 14h. NHB é um evento lúdico, onde nos reuniremos para desenvolver funcionalidades e consertar bugs no [Noosfero](http://noosfero.org/), o seu software livre para redes sociais preferido.

Essa é a oportunidade perfeita para quem quiser começar a contribuir com o Noosfero: toda a equipe vai estar lá para orientar e tirar dúvidas.  Se você participa de uma das redes sociais que rodam Noosfero ([softwarelivre.org](/), [Cirandas](http://cirandas.net/), [Unifreire](http://redesocial.unifreire.org/), [Ponto por Ponto](http://pontoporponto.org/), [Circuito Fora do Eixo](http://foradoeixo.org.br/), entre outras), você vai ter a oportunidade de ter ajuda pra implementar aquela idéia fabulosa que você teve, ou de corrigir um bug que te incomodam.

Queremos fazer esse encontro num espaço descontraído, e por isso nos encontraremos no sushi bar do shopping [Paseo Itaigara](http://www.paseoitaigara.com.br/) (só Flash, vá desculpando aí). Mas fazer um evento desse fora de um ambiente de escritório/laboratório faz com que seja necessário que se leve computadores. Quem não puder levar um computador próprio pode ir também, afinal [programação em par](http://en.wikipedia.org/wiki/Pair_programming) é uma prática de desenvolvimento muito eficiente, e também divertida.

**Mesmo quem não estiver em Salvador pode participar**: estaremos no IRC, nos canais \#noosfero-br (português) e \#noosfero (em inglês) na Freenode.

Como se preparar para o evento:

1.  Dê uma olhada na lista de [bugs e funcionalidade fáceis](http://noosfero.org/Noosfero/EasyToSolve) em aberto, e escolha um(a) que lhe interesse.
2.  Tente já [instalar o ambiente de desenvolvimento](http://noosfero.org/Noosfero/GettingStartedWithNoosferoDevelopment) no seu computador antes do dia do evento pra ganhar tempo. Em caso de dúvidas, pergunta na [lista de discussão](http://listas.softwarelivre.org/cgi-bin/mailman/listinfo/noosfero-br).

**Resumindo:**

-   O quê: Noosfero Hacking Bar
-   Onde: Sushi bar do Shopping Paseo Itaigara (Rua Rubens Guelli, 135, Itaigara), Salvador/BA
-   Quando: 08/05 (Sábado), às 14h

