---
old: true
title: "Um nerd entediado no banheiro"
created_at: "2009-09-29 10:51 -3"
kind: article
---

Pode parar, não é nada disso que você está pensando. :-)

Olha só o que eu encontrei no banheiro do [IM](http://www.im.ufba.br/) agora de manhã:

![FAVOR NÃO USAR A PIA DO BANHEIRO PARA LAVAR PRATOS E TALHERES](/oldposts/images/nerd-no-banheiro.jpg?1254231490)

Pra quem não conseguir ver, eu transcrevo aqui o que o nerd escreveu embaixo da singela solicitação em prol da manutenção da higiene no banheiro:

-   Pratos = A
-   Talheres = B
-   ¬(A∧B) = ¬A ∨ ¬B (pela regra de Moore)
-   ou seja, vc pode lavar só pratos ou só talheres
-   FAIL!

Eu daria os créditos, mas ele não assinou. ;-)

