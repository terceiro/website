---
old: true
title: "Debian e o horário de verão na Bahia e no Tocantins"
created_at: "2012-10-20 12:13 -3"
kind: article
---

À meia noite deste Sábado, uma parte do Brasil vai adiantar os seus relógios em uma hora. De última hora, aconteceram alguma [mudanças](http://www.planalto.gov.br/ccivil_03/_ato2011-2014/2012/decreto/d7826.htm) no horário de verão: a Bahia não vai mais participar, e o Tocantins passa a participar. Existe uma certa polêmica ainda, o Tocantins tentando sair, mas a sua participação no horário de verão está no decreto oficial.

Uma grande parte dos sistemas operacionais, Debian inclusive, utiliza uma base de dados chamada [tzdata](http://www.iana.org/time-zones) para atualizar seus relógios automaticamente com a chegada do horário de verão, e toda vez que os políticos resolvem alterar as regras, essa base de dados precisa ser alterada.

No Debian, Rogério Bastos relatou o [bug \#690606](http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=690606) alertando para a mudança nas regras. Eu preparei um patch fazendo as mudanças necessárias. O Aurelien Jarno, um dos mantenedores do tzdata no Debian, atualizou o pacote incluindo esse patch tanto na distribuição Wheezy (testing) quanto na Squeeze (stable).

Então, pra atualizar as regras de horário de verão num sistema Debian, basta atualizar o seu sistema normalmente (*apt-get update && apt-get dist-upgrade*). No caso do Squeeze (stable), o repositório stable-updates precisa estar habilitado (ele está habilitado por default).

O pacote tzdata oficial ainda não foi atualizado para as mudanças do horário brasileiro de verão, mas o [patch](http://mm.icann.org/pipermail/tz/attachments/20121020/78bc1e8d/brazil-dst-2012-changes.diff) utilizado no Debian já foi [submetido](http://mm.icann.org/pipermail/tz/2012-October/018363.html) para a lista.

