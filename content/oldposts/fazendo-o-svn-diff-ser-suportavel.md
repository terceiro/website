---
old: true
title: "Fazendo o svn diff ser suportável"
created_at: "2009-08-21 21:17 -3"
kind: article
---

Já tem um tempo que eu uso git diariamente, e a opção de colocar cores nas saídas dos comandos é extremamente útil. Especialmente quando você quer ver um diff, isso facilita a sua vida. Daí quando você por algum motivo precisa usar svn de novo, seja pra mandar um patch pra um projeto que ainda usa svn, seja pra um projeto seu que ainda esteja nas trevas, vem aquele diff feio. Seus problemas acabaram:

1) edite o arquivo `~/.subversion/config`, e dentro da seção helpers, diga para o svn usar um comando diferente do default como programa diff:

```
diff-cmd = /home/USERNAME/bin/svn-git-diff
```

(esse programa pode estar em qualquer diretório que esteja no seu PATH)


2) esse svn-git-diff é um script bastante simples. O svn vai chamá-lo com um monte de parâmetros, mas os únicos que importam são o sexto e o sétimo, que indicam o arquivo original e a sua versão modificada.

```language-shell
#!/bin/sh
export PAGER=/bin/cat
git diff --no-index "${6}" "${7}"
```

3) não se esqueça de tornar esse script executável com  `chmod +x /home/USERNAME/bin/svn-git-diff` (o caminho real do arquivo vai depender se onde você o colocou).

E era isso. Agora quando você der `svn diff`, o diff vai sair colorido assim como quando você está verificando diffs (ou commits) no git.

**update:** numa versão mais nova do git (1.6.5.2) é necessário passar `--no-index` pra que o `git diff` funcione fora de um repositório git.

**update 2:** é preciso setar `PAGER` para `/bin/cat` para que o git não faça um pipe para o `less` de cada arquivo individualmente, e sim cuspa todo o diff na tela de vez (porque o subversion vai chamar o script uma vez pra cada arquivo alterado).

