---
old: true
title: "A visual cheat sheet for ANSI color codes"
created_at: "2012-01-28 11:17 -3"
kind: article
---

Now and then I want to output some [ANSI color escape codes](http://en.wikipedia.org/wiki/ANSI_escape_code) from software I write, and I always end up doing some trial-and-error to figure out the exact codes I want. Sometimes it’s overkill to add a dependency on an [existing library](http://packages.debian.org/sid/ruby-term-ansicolor) that already deals with it, or the language I am using does not have one.

There are a lot of listings of the ANSI color codes out there, but I couldn’t find one that matches the actual codes with the resulting effect in a visual way. Even the Wikipedia article has a colored table with the actual colors, but I have to lookup manually which code combination produces which color.

So I spent a few minutes to write a shell script that prints all useful combinations, formatted with themselves. This way I can quickly figure out which exact code I want to achieve the desired effect.

![](/oldposts/images/ansi-color-demo.png)

The code for now is *very simple*:

```language-shell
#!/bin/sh -e

for attr in $(seq 0 1); do
  for fg in $(seq 30 37); do
    for bg in $(seq 40 47); do
      printf "\033[$attr;${bg};${fg}m$attr;$fg;$bg\033[m "
    done
    echo
  done
done
```

Is there a package in Debian that already does that? Would people find it useful to have this packaged?

**update:** it turns out you can find some similar stuff on google images. It was a quick and fun hack, though.

**update 2:** Replacing `echo -n` with `printf` makes the script work independently if /bin/sh is bash or dash. Thanks to *cocci* for pointing that out.

