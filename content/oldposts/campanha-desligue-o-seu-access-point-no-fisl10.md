---
old: true
title: "Campanha: desligue o seu Access Point no FISL10!"
created_at: "2009-06-26 10:21 -3"
kind: article
tags: [fisl]
---

![Network-wireless](/oldposts/images/network-wireless.png?1246022222)Você já deve ter percebido que a rede wireless do FISL funciona muito bem durante o começo da manhã e no final do dia, não é? Pois é, isso é porquê nesse horários os access points dos participantes não estão ligados! Eu não sou nenhum especialista em wireless, mas do pouco que eu entendo sobre rádio, eu sei que não dá pra transmitir tanta coisa na mesma frequência. O centro de eventos da PUCRS tem uma boa estrutura de wireless, mas com tanto AP ligado ela se torna inútil. Eles até tentam mudar o canal do wireless, mas sempre tem mais um tanto de access points em vários canais.

Por isso, estou iniciando a campanha "Desligue o seu Access Point no FISL". Se você quer ter uma rede privada no seu stand, ao menos tire as antenas do AP pra que ele não atrapalhe a rede geral, ou melhor ainda, desligue o wireless do seu AP e use só as portas ethernet dele.

