---
old: true
title: "Upgrades to Jessie, Ruby 2.2 transition, and chef update"
created_at: "2015-07-02 20:26 -3"
kind: article
---

Last month I started to track all the small Debian-related things that I do. My initial motivation was to be concious about how often I spend short periods of time working on Debian. Sometimes it’s during lunch breaks, weekends, first thing in the morning before regular work, after I am done for the day with regular work, or even *during* regular work, since I do have the chance of doing Debian work as part of my regular work occasionally.

Now that I have this information, I need to do something with it. So this is probably the first of monthly updates I will post about my Debian work. Hopefully it won’t be the last.

**Upgrades to Jessie**

I (finally) upgraded my two servers to Jessie. The first one, my home server, is a [Utilite](http://www.compulab.co.il/utilite-computer/web/utilite-overview) which is a quite nice ARM box. It is silent and consumes very little power. The only problem I had with it is that the vendor-provided kernel is too old, so I couldn’t upgrade udev, and therefore couldn’t switch to systemd. I had to force systemv for now, until I can manage to upgrade the kernel and configure uboot to properly boot the official Debian kernel.

On my VPS things are way better. I was able to upgrade nicely, and it is now running a stock Jessie system.

**fixed https on ci.debian.net**

[pabs](http://bonedaddy.net/pabs3/) had let me know on IRC of an issue with the TLS certificate for ci.debian.net, which took me a few iterations to get right. It was missing the intermediate certificates, and is now fixed. You can now enjoy [Debian CI under https](https://ci.debian.net) .

**Ruby 2.2 transition**

I was able to start the [Ruby 2.2 transition](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=789077), which has the goal of switch to Ruby 2.2 on unstable. The first step was updating [ruby-defaults](https://tracker.debian.org/news/690073) adding support to build Ruby packgaes for both Ruby 2.1 and Ruby 2.2. This was followed by updates to gem2deb ([0.18](https://tracker.debian.org/news/689673), [0.18.1](https://tracker.debian.org/news/689674), [0.18.2](https://tracker.debian.org/news/690726), and [0.18.3](https://tracker.debian.org/news/692230)) and [rubygems-integration](https://tracker.debian.org/news/690724) . At this point, after a few rebuild requests only 50 out of 137 packages need to be looked at; some of them just use the default Ruby, so a rebuild once we switch the default will be enough to make it use Ruby 2.2, while others, specially Ruby libraries, will still need porting work or other fixes.

**Updated the Chef stack**

Bringing chef to the very latest upstream release into unstable was quite some work.

I had to update:

-   ruby-columnize (0.9.0-1)
-   ruby-mime-types (2.6.1-1)
-   ruby-mixlib-log 1.6.0-1
-   ruby-mixlib-shellout (2.1.0-1)
-   ruby-mixlib-cli (1.5.0-1)
-   ruby-mixlib-config (2.2.1-1)
-   ruby-mixlib-authentication (1.3.0-2)
-   ohai (8.4.0-1)
-   chef-zero (4.2.2-1)
-   ruby-specinfra (2.35.1-1)
-   ruby-serverspec (2.18.0-1)
-   chef (12.3.0-1)
-   ruby-highline (1.7.2-1)
-   ruby-safe-yaml (1.0.4-1)

In the middle I also had to package a new dependency, ruby-ffi-yajl, which was *very* quickly ACCEPTED thanks to the awesome work of the ftp-master team.

**Random bits**

-   Sponsored a [upload of redir](https://tracker.debian.org/news/689030) by Lucas Kanashiro
-   [chake](https://packages.debian.org/sid/chake), a tool that I wrote for managing servers with chef but without a central chef server, got ACCEPTED into the official Debian archive.
-   [vagrant-lxc](https://packages.debian.org/sid/vagrant-lxc) , a vagrant plugin for using lxc as backend and lxc containters as development environments, was also ACCEPTED into unstable.
-   I got the deprecated ruby-rack1.4 package [removed from Debian](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=788643)

