---
old: true
title: "Participando do projeto Debian - como começar"
created_at: "2013-06-18 21:20 -3"
kind: article
---

![](/oldposts/images/debian-small.png)

O objetivo deste post é informar pessoas interessadas em contribuir com o Debian sobre por onde começar. Existem várias formas de contribuir com o Debian: você pode contribuir com empacotamento/desenvolvimento, *artwork*, triagem de bugs, tradução, documentação, divulgação, suporte a outros usuários, atividades administrativas, organização da presença do Debian em eventos, e por aí vai.

Este post é uma tentativa de dar a minha visão sobre como começar a contribuir com o Debian. É possível que o conteúdo seja um pouco enviesado para contribuição com desenvolvimento/empacotamento, porque é isso que eu faço, mas eu fiz um esforcinho pra ser genérico.

Talvez faltem informações, ao algumas coisas estejam confusas. Fique a vontade pra postar um comentário lá no final.

Coisas pra se ter em mente, antes de qualquer coisa
---------------------------------------------------

### Quem pode colaborar com o Debian

No Debian, a [contribuição de todos é bem-vinda](http://www.debian.org/intro/diversity.en.html). Qualquer pessoa que tenha o conhecimento e habilidade necessários para uma determinada tarefa pode começar a contribuir com o projeto *agora*.

A maioria das atividades no Debian não requer nenhum tipo de permissão especial. A forma exata como a contribuição é feita depende muito da atividade específica, por isso não vou entrar em detalhes aqui.

Uma das poucas atividade que não pode ser feitas por qualquer pessoa é o upload de pacotes. Qualquer pessoa pode manter um pacote no Debian, mas o upload do pacote para os servidores do projeto precisa ser feito por um desenvolvedor oficial (mais detalhes abaixo).

### Comunicação

Praticamente toda comunicação do projeto se dá em [listas de discussão](http://lists.debian.org/) e [canais no IRC](http://wiki.debian.org/IRC). Você provavelmente vai querer se inscrever nas listas de discussão do seu interesse, e frequentar um ou mais canais no IRC.

### O idioma utilizado no projeto Debian é o inglês

Lembra quando te diziam na época da escola que inglês é importante? Pois é. Para participar do Debian não é necessário ser 100% fluente, mas conseguir ler é fundamental, e conseguir escrever, ainda que só o básico, ajuda muito.

Mas não se deixe impedir por deficiências no inglês: participar de um projeto internacional vai melhorar **muito** o seu inglês (experiência própria), então com o tempo você vai se sentir cada vez mais à vontade.

Existe uma [lista de desenvolvimento em português](http://lists.debian.org/debian-devel-portuguese/), que pode ser usada pra tirar dúvidas, mas na minha experiência o trabalho de verdade acontece em inglês.

### Fazendo a lição de casa

Todos temos dúvidas, e como o Debian tem um escopo imenso, é normal que você não saiba alguma coisa. Ninguém sabe tudo. Mas é importante que você *pesquise* antes de perguntar. Primeiro porque existe a probabilidade de alguém já ter tido aquela dúvida antes, e a resposta pra ela pode já existir e estar arquivada. Segundo, porquê as pessoas que estão no projeto a mais tempo acham bem chato responder às mesmas perguntas de novo e de novo.

É possível que você não encontre a resposta para a sua dúvida. Nesse caso, não hesite em perguntar. Uma leitura interessante relacionada é o [How To Ask Questions The Smart Way](http://catb.org/~esr/faqs/smart-questions.html), que dá várias dicas de como fazer perguntas da forma mais eficiente possível. Especialmente numa discussão por email, uma pergunta feitas com todas as informações necessárias para que te seja dada uma resposta pode te economizar vários dias!

Pra contribuições nas áreas de desenvolvimento e empacotamento, você vai querer ler:

-   [Guia do novo mantenedor](http://www.debian.org/doc/manuals/maint-guide/) (clássico, mas mais antigo e talvez obsoleto em algumas partes) e/ou [Tutorial de empacotamento](http://www.debian.org/doc/manuals/packaging-tutorial/packaging-tutorial.pdf "PDF, ~400kb") (mais recente, mais prático, em formato de slides)
-   [Manual de políticas técnicas](http://www.debian.org/doc/debian-policy/). As regras que todo pacote tem que seguir.
-   [Como funciona o sistema de gestão de bugs do Debian](http://www.debian.org/Bugs/)
-   [Referência Debian](http://www.debian.org/doc/manuals/developers-reference/)

Note que os items acima representam *bastante* documentação. Não precisa ler tudo de uma vez só antes de fazer uma contribuição, mas saiba que cedo ou tarde as respostas pra dúvidas que você tem podem estar neles.

Formas de começar
-----------------

Eu diria que existem 2 formas de começar: a primeira é fazer parte de um time existente; a segunda, específica para contribuição com desenvolvimento/empacotamento, é escolher um pacote pra contribuir. Começar se juntando a um time na minha opinião é mais fácil.

### Fazendo parte de uma equipe

Hoje em dia, uma grande parte das atividades do Debian acontecem no contexto de times específicos. No Wiki do projeto há uma [lista de times existentes](http://wiki.debian.org/Teams) que hoje lista mais de 130 times. Existem times de empacotamento de software em linguagens específicas (como a equipe de Ruby, do qual eu faço parte) ;existem times focados em um conjunto de pacotes relacionados, como os times de empacotamento do GNOME, do KDE, do Xfce; existem times transversais, como o time de segurança, o time de publicidade, o time de controle de qualidade, etc. Dá uma olhada lá na [lista de times](http://wiki.debian.org/Teams).

Na minha opinião a melhor forma de começar é fazer parte de um time. A chave aqui é escolher uma equipe com a qual você se identifique, o que ajuda a manter a sua motivação. Se você desenvolve em Perl, dê uma olhada no time de Perl. Se você se interessa por tipografia, dê uma olhada no time de fontes. Se você usa KDE e se interessa pelo KDE mais do que alguém normalmente se interessaria pelo seu desktop, confira o time KDE. Se você não é um(a) desenvolvedor(a), procure um dos vários times que lidam com outros tipos de atividades que podem te interessar.

Escolhido o time, você provavelmente vai querer assinar a(s) lista(s) de discussão do time, frequentar o canal do time do IRC, e começar a entender o que o time faz e como ele funciona. Se o time tiver uma lista de tarefas, tente atacar um item da lista. Se você tiver sorte os itens podem até estar classificados em níveis de dificuldade.

Se a equipe for relacionada a empacotamento, você vai querer aprender empacotamento. Acho que uma boa forma de começar a aprender é instalar o pacote [packaging-tutorial](http://packages.debian.org/sid/packaging-tutorial) e abrir `/usr/share/doc/packaging-tutorial/packaging-tutorial.pdf` no seu leitor de PDF favorito. Verifique se existem bugs nos pacotes do time; escolha um bug, tente reproduzí-lo no seu sistema local. Se você conseguir reproduzir, tente consertar.

Uma vez que você tenha uma contribuição a um pacote, você vai precisar que essa contribuição seja revisada por alguma outra pessoa do time, e de um desenvolvedor oficial pra fazer o upload do pacote corrigido. Normalmente em times encontrar um desenvolvedor para fazer uploads não deve ser difícil.

### Escolhendo um pacote

Se você não está interessado(a) em contribuir com desenvolvimento ou empacotamento, pule essa sessão. :-)

Outra forma de começar é escolher um pacote pra contribuir. Assim como no caso das equipes, escolha um pacote que seja do seu interesse, ou seja, um pacote que você usa. Pode ser uma aplicação que você usa sempre, uma biblioteca que está instalada no seu sistema como dependência de algum outro pacote. Tente começar com um pacote simples/pequeno.

O Debian tem uma base de dados de pacotes que precisam de ajuda, seja porque o mantenedor atual está sem tempo pra manutenção e quer que alguém passe a ser o mantenedor, seja porque o pacote já está órfão a um tempo, ou seja porque o mantenedor atual queira compartilhar a manutenção com outra(s) pessoa(s). Essa base de dados se chama WNPP, e existe um interface pra pesquisar por esses pacotes em [wnpp.debian.net](http://wnpp.debian.net/).

Uma boa forma de descobrir um pacote que precisa de um pouco de carinho pra você começar é instalar o pacote [devscripts](http://packages.debian.org/sid/devscripts) e executar o comando `wnpp-alert`. Ele vai listar todos os pacotes que estão instalados no seu sistema **e** estão órfãos, ou precisam de co-mantenedores.

Você vai querer aprender sobre empacotamento. Uma boa forma de começar a aprender é instalar o pacote [packaging-tutorial](http://packages.debian.org/sid/packaging-tutorial) e abrir `/usr/share/doc/packaging-tutorial/packaging-tutorial.pdf` no seu leitor de PDF favorito.

Não consigo pensar numa boa forma de escolher um pacote. Eu diria o seguinte:

1.  escolha uns 2 ou 3 pacotes
2.  dê uma olhada na lista de bugs de cada pacotes
3.  escolha um bug de um dos pacotes, tente reproduzí-lo no seu sistema local.
4.  Se você conseguir reproduzir, tente consertar. A forma exata de como consertar vai depender muito do bug, então não posso ser mais específico.
5.  se você não entender o bug, ou não conseguir reproduzir, talvez você queira relatar isso no bug report. Talvez você queira voltar para o passo 3.

Se você escolheu um pacote, leia a [documentação sobre o WNPP](http://www.debian.org/devel/wnpp/), e faça o procedimento pra dizer que você quer adotar o pacote. Verifique se existem novas versões do pacotes lançadas pelo desenvolvedor original (que a gente chama no Debian de *upstream*). Tente atualizar pra essa versão.

Quando você tiver o pacote pronto, você vai precisar encontrar um desenvolvedor oficial pra revisar o pacote. Essa pessoa vai revisar o seu trabalho, eventualmente pedir pra você fazer algumas (ou muitas) correções ou melhorias no pacote, e por fim vai fazer o upload.

Caso você queira começar por um pacote, o grupo de orientação ([‘mentors’](http://mentors.debian.net/)) é um bom lugar pra começar, tanto em termos de orientação como em termos de achar um desenvolvedor pra revisar o seu pacote fazer o upload pra você.

