---
old: true
title: "Debian Day 2012 em Salvador"
created_at: "2012-08-19 21:31 -3"
kind: article
---

O evento foi muito bom, e teve uma participação razoável. Não fizemos uma contagem oficial, mas em alguns momentos acho que tínhamos umas 30 pessoas. Só de certificados eu assinei 12. As discussões foram interessantes, e espero que alguém tenha se empolgado para começar a contribuir com o Debian, e com o software livre em geral.

Seguem alguns links:

-   [Fotos tiradas pelo Fabio Nogueira](https://docs.google.com/folder/d/0BzulLGr_xbjGdlJpMXNkOEVYcXM/edit?pli=1)
-   [slides de abertura](/oldposts/files/ddssa2012-abertura.pdf)
-   slides da palestra de Alex, [De volta as origens… Um overview sobre a relação entre o Debian e o Ubuntu](http://www.slideshare.net/alexoslabs/de-volta-as-origens-um-overview-sobre-a-relao-entre-o-debian-e-o-ubuntu)
-   slides da minha palestra, [Debian: 19 anos e contando](/oldposts/files/ddssa2012-terceiro.pdf)
-   [lista de certificados de participação emitidos](http://people.debian.org/~terceiro/ddssa2012.txt) – conforme descrito no corpo dos certificados

