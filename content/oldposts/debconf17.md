---
old: true
title: "Debconf17"
created_at: "2017-08-14 17:27 -3"
kind: article
tags: [debconf, autopkgtest, debian-ci, patterns]
---

I’m back from Debconf17.

I gave a [talk](https://debconf17.debconf.org/talks/34/) entitled “Patterns for Testing Debian Packages”, in which I presented a collection of 7 patterns I documented while pushing the [Debian Continuous Integration](https://ci.debian.net/) project, and were published [in a 2016 paper](/2017/03/17/patterns-for-testing-debian-packages/). [Video recording](http://meetings-archive.debian.net/pub/debian-meetings/2017/debconf17/patterns-for-testing-debian-packages.vp8.webm) and a [copy of the slides](https://annex.debconf.org/debconf-share/debconf17/slides/34-patterns-for-testing-debian-packages.pdf) are available.

I also hosted the [ci/autopkgtest BoF](https://debconf17.debconf.org/talks/35/) session, in which we discussed issues around the usage of autopkgtest within Debian, the CI system, etc. [Video recording is available](http://meetings-archive.debian.net/pub/debian-meetings/2017/debconf17/ci-autopkgtest-bof.vp8.webm).

Kudos for the Debconf video team for making the recordings available so quickly!

