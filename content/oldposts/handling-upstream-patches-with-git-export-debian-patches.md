---
old: true
title: "Handling upstream patches with git-export-debian-patches"
created_at: "2011-08-13 22:03 -3"
kind: article
---

These days I briefly discussed with a fellow Debian developer about how to maintain upstream patches in Debian packages with Git, what brought me to rethink a little about my current practices. What I usually do is pretty much like point 4 in Raphael's post ["4 tips to maintain a “3.0 (quilt)” Debian source package in a VCS"](https://raphaelhertzog.com/2010/11/18/4-tips-to-maintain-a-3-0-quilt-debian-source-package-in-a-vcs/):  I make commits in the Debian packaging branch, or in a separate branch that is merged into the Debian packaging branch. Then I add the `single-debian-patch` option to `debian/source/options` so that a single Debian patch is generated, and include a patch header that points people interested in the individual changes to the public Git repository where they were originally done.

My reasoning for doing so was the following: most upstream developers will hardly care enough to come check the patches applied against their source in Debian, so it's not so important to have a clean source package with separated and explained patches. But then there is the people who will actually care about the patches: other distribution developers. Not imposing a specific VCS on them to review the patches applied in Debian is a nice thing to do.

Then I wrote a script called git-export-debian-patches ([download](/oldposts/files/git-export-debian-patches)), which was partly inspired by [David Bremner's script](http://www.cs.unb.ca/~bremner/blog/posts/git-classify/). It exports all commits in the Debian packaging branch that do not touch files under debian/ and were not applied upstream to debian/patches. The script also creates an appropriate debian/patches/series files. The script is even smart enough to detect patches that were later reverted in the Debian branch and exclude them (and the commit that reverted them) from the patch list.

The advantage I see over gbp-pq is that I don't need to rebase (and thus lose history) to have a clean set of patches. The advantage over the [gitpkg quilt-patches-deb-export-hook hook](http://www.cs.unb.ca/~bremner/blog/posts/yagq/) is that I don't need to explicitly say which ranges I want: every change that is merged in master, was not applied upstream and was not reverted gets listed as a patch. To be honest I don't have any experience with either gbp-pq or gitpkg and these advantages were based on what I read, so please leave a (nice ;-)) comment if I said something stupid.

I am looking forward to receive feedback about the tool, specially about potential corner cases in which it would break. For now I have tested it in a [package](http://anonscm.debian.org/gitweb/?p=pkg-ruby-extras/cucumber.git;a=summary) with simple changes agains upstream source, and it seems fine.

