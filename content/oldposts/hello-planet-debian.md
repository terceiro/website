---
old: true
title: "Hello, Planet Debian"
created_at: "2011-07-22 23:58 -3"
kind: article
tags: [debian]
---

Hello, world! So, now this blog is being syndicated on Planet Debian, and I decided to write something to introduce myself.

I was born in [Salvador, Bahia, Brazil](http://en.wikipedia.org/wiki/Salvador%2C_Bahia). I am married to Josy, a wonderful person; we do not have any kids yet, but anyone can notice we both discretely drooling when close to small children.

I have a passion for building things, and making things work. Programming happens to be the better building toy that someone already invented.

I became am official Debian Developer a little more than one week ago, but I have been working in the project for some time already. In the very beginning I was involved with the Perl group, but now almost all my work is in the Ruby team.

I am a PhD student at Federal University of Bahia under supervision of professor [Christina Chavez](http://wiki.dcc.ufba.br/LES/ChristinaFlach). My research involves three topics I am just fascinated about: Free ("and Open Source") Software, Software Design and Empirical Software Engineering. I am investigating whether it is possible to explain the variation in Structural Complexity by analyzing developer attributes. I want to answer questions such as "do developers with more experience in a project produce more complex code?", or "do developers that focus on a single part of the project produce more complex code than developers who work on the entire project?".

As part of my PhD research I have been working on [analizo](http://analizo.org/), a multi-language source code analysis and visualization toolkit. I plan to write specifically about it in a later opportunity.

My other job is running my own company, [Colivre](http://colivre.coop.br/), together with several other great people. Colivre is a cooperative, which basically means that everyone who works there is also an owner of the company. We provide web solutions, in special social networking environments, social media websites and the like. Working on (and with) free software is our premise at Colivre, and most of our work involves either [Noosfero](http://noosfero.org/) or [Foswiki](http://foswiki.org/).

Right now I am taking some time off from Colivre to be able to concentrate on (and finish!) my PhD. Until late September I live at the beautiful Vancouver, BC, Canada, where I am currenly a visiting reasearcher at the [Software Practices Lab](http://www.cs.ubc.ca/labs/spl/) at [UBC](http://www.ubc.ca/) under supervision of professor [Gail Murphy](http://www.cs.ubc.ca/~murphy/).

So that's it, I hope to post interesting stuff here and to get your feedback whenever you find it's worth. I also maintain a [microblog at identi.ca](http://identi.ca/terceiro), where you'll find shorter (and more frequent) updates.

