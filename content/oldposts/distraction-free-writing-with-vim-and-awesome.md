---
old: true
title: "Distraction-free writing with vim and awesome"
created_at: "2010-06-04 15:57 -3"
kind: article
---

I've recently learn about distraction-free text editing [from Lucas](http://lucasr.org/2010/05/27/distraction-free-blogging/), and really liked the idea. Turns outs it is pretty easy to implement it with my beloved [vim](http://www.vim.org/) text editor.

Removing all distraction while using the console vim is trivial. I just need to switch my [awesome](http://awesome.naquadah.org/) workspace to full-screen layout, and that's it. The terminal window running vim will take the entire screen space, and then it's me and vim. No menus, no buttons, nothing.

Achieving the same effect with gvim is a bit more tricky: besides putting the awesome workspace to use full-screen layout, you have to turn off both the menus and the toolbar. To do that, I mapped the F11 and F12 keys to turn the menus off and on, respectively, by adding the following lines to my \~/.gvimrc:

    map <F11> :set guioptions-=m<CR>:set guioptions-=T<CR>
    map <F12> :set guioptions+=m<CR>:set guioptions+=T<CR>

![Distraction-free-gvim](/oldposts/images/distraction-free-gvim.png?1275676999)

You can check out the result in the above screeshot. Yes, that is my entire screen.

The tip for toggling menu and toolbars came from [the vim wiki](http://vim.wikia.com/wiki/Hide_toolbar_or_menus_to_see_more_text).

You ask me: what's the point of running gvim if you are disabling the menus and the toolbar? Several reasons: the fonts look nicer in gvim, the colors are better than the console ones, and I can turn the menu back on if I need (for example to do something I do not remember the command for).

