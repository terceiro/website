---
old: true
title: "Debian Ruby Sprint 2016 - day 2: Japanese cuisine, bug fixes, and Mini Cheese&Wine Party"
created_at: "2016-03-02 12:56 -3"
kind: article
tags: [debian, ruby]
---

Day 1 ended with dinner at a [Yamato](http://www.restauranteyamato.com.br/), my preferred Japanese restaurant in the city. Curitiba has a very large Japanese community, and lots of Japanese restaurants. Yamato, however, is the only one were you will stumble upon senior Japanese people, probably first or second generation immigrants, what I guess says something about its authenticity.

![](/oldposts/images/yamato.jpg)

Right after breaking for lunch, but before actually going out, we made what so far is official group photo (I might try again as the shot was not a really good one).

![](/oldposts/images/group-photo.jpg)

Of course the most interesting part was the actual work that was done, and day 2 list is not less impressive than the day before:

-   ruby-albino 1.3.3-4 (#813644) @sbadia
-   \#816256: ruby-versionomy: FTBFS: `require’: cannot load such file — blockenspiel/unmixer_mri (LoadError)
-   investiage ruby2.1 removal
-   upload ruby-beautify
-   upload ruby-aws-sdk
-   made ruby-blockenspiel arch:all again
-   filed RC bug against pcs depending on ruby2.1-dev
-   ruby-dev in experimental now supports `pkg-config ruby` so packages can use pkg-config to build against the current default Ruby
-   ruby-libxml (some tests skipped)
-   fixed FTBFS #804794 in subtle delaying ruby2.1 removal (now in DELAYED/2)
-   mailed debian-ruby and ask people to test ruby2.3 as default
-   \#816162: ruby-zentest: FTBFS: Failure: TestZenTest#test_testcase9
-   ruby-sshkit 1.9.0~rc1-1 (795118)
    -   capistrano 3.4.0-1 + ruby-sshkit 1.9.0~rc1-1 => http://paste.debian.net/plain/410699
-   filed for ruby-patron removal
-   updated and uploaded ruby-pygments.rb updated + patch for tests (fix #812940)
-   discussed dependency cycle resolution
    -   will keep cycle between ruby2.3 and rake (as they really need each other)
    -   will break cycle from pure ruby packages to ruby when they are depended on by ruby2.3
    -   for same package set, enable Multi-Arch: foreign when needed
-   uploaded ruby-power-assert (M-A foreign, break ruby cycle)
-   uploaded ruby-did-you-mean (M-A foreign, break ruby cycle)
-   uploaded ruby-minitest (M-A foreign, break ruby cycle)
-   uploaded ruby-test-unit (M-A foreign, break ruby cycle)
-   uploaded rake (M-A foreign)
-   uploaded ruby-net-telnet (M-A foreign)
-   discussed removal of libruby metapackage, filed bug against dh-make-drupal (only rdep) #816417
-   uploaded yard (Closes: #812809)
-   rerun failed builds
-   closed #798539 (not applicable on current version of ruby-webmock)
-   closed #816321 fixed by ruby2.3 upload
-   uploaded ruby-cliver (by @lucasmoura)
-   uploaded ruby-celluloid-supervision ( closes: #810770)
-   uploaded ruby-celluloid-fsm ( closes: #815107)
-   uploaded ruby-raindrops
-   investigated status of dhelp, ruby-mysql again
-   ruby-http-form-data (1.0.1+gemwatch-1) @sbadia
-   nmu ohcount (DELAYED/2)
-   updated trocla to newest upstream, fixing #816257
-   reassign #816358 from ruby-safe-yaml to ruby2.3
-   close #816120 (transient bug in ruby-default-value-for, waiting for ruby-sqlite3 binNMU)
-   upload updated ruby-rc4
-   uploaded new upstream version of pry, fixing FTBFS with ruby2.3
-   ruby-rb-inotify , required to new ruby-listen( need ruby-rb-inotify >= 0.9.7)
-   applied patch from upstream to ruby2.3 for #816358
-   uploaded new upstream version for coderay supporting ruby2.3

On Monday Cédric told us that he and Sebastien had brought a bottle of French wine and some smelly French cheeses, and suggested that in the best Debian tradition we should have a Mini Cheese and Wine Party™. Sure thing! Luckily there is a farmer’s market 2 blocks from home on Tuesdays mornings, where I usually buy my fruits, vegetables, and cheese & friends, so the timing was perfect. I went shopping early in the morning, and bought a few things, and was back before it was the time to go to UTFPR. After the day-long hacking session we stopped by another store nearby to buy a few extra bottles of wine and other snacks.

At night, in my place, I ended up playing cheese master.

![](/oldposts/images/cheese-master.jpg)

There was enough food that at the end we were all *very* full.

![](/oldposts/images/cheese-and-wine-party-table.jpg)

And with the spokesperson task of the day done, off to hacking I am!

