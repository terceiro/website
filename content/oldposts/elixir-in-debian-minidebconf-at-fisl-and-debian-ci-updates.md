---
old: true
title: "Elixir in Debian, MiniDebconf at FISL, and Debian CI updates"
created_at: "2015-08-05 01:13 -3"
kind: article
tags: [debian, fisl, ruby]
---

[In June I started keeping track](/2015/07/02/upgrades-to-jessie-ruby-22-transition-and-chef-update/) of my Debian activities, and this is my July update.

**Elixir in Debian**

[Elixir](http://elixir-lang.org/) is a functional language built on top of the Erlang virtual machine. If features imutable data structures, interesting concurrency primitives, and everything else that Erlang does, but with a syntax inspired by Ruby what makes it much more aproachable in my opinion.

Those interested in Elixir for Debian are encouraged to hang around in \#debian-elixir on the OFTC IRC servers. There are still a lot of things to figure out, for example how packaging Elixir libraries and applications is going to work.

**MiniDebconf at FISL, and beyond**

I helped organize a [MiniDebconf](https://wiki.debian.org/DebianEvents/br/2015/MiniDebconfFISL) at this year’s FISL, in Porto Alegre on the 10th of July. The whole program was targetted at getting more people to participate in Debian, so there were talks about translation, packaging, and a few other more specific topics.

I myself gave two talks: one about Debian basics, “What is Debian, and how it works”, and second one on “packaging the free software web”, which I will also give at [Debconf15](http://debconf15.debconf.org/) later this month.

The recordings are available (all talks in Portuguese) [at the Debian video archive](http://meetings-archive.debian.net/pub/debian-meetings/2015/mini-debconf-fisl/) thanks to Holger Levsen.

We are also organizing a [new MiniDebconf in October](https://wiki.debian.org/DebianEvents/br/2015/MiniDebconfLatinoware) as part of the [Latinoware](http://latinoware.org/) schedule.

**Ruby**

We are in the middle of a [transition](https://release.debian.org/transitions/html/ruby2.2.html) to switch to Ruby 2.2 as default in Debian unstable, and we are almost there. The Ruby transition is now on hold while [GCC 5 one](https://release.debian.org/transitions/html/libstdc++6.html) is going on, but will be picked up as soon as were are done with GCC 5.

ruby-defaults has been uploaded to experimental for those that want to try having Ruby 2.2 as default before that change hits unstable. I myself have been using Ruby 2.2 as default for several weeks without any problem so far, including using vagrant on a daily basis and doing all my development on sid with it.

I started taking notes about [Ruby interpreter transitions](https://wiki.debian.org/Teams/Ruby/InterpreterTransitions) work to make sure that knowledge is registered.

I have uploaded minor security updates of both [ruby2.1](https://tracker.debian.org/news/701396) and [ruby2.2](https://tracker.debian.org/news/701394) to unstable. They both reached testing earlier today.

I have also [fixed](https://tracker.debian.org/news/697703) another [bug](https://bugs.debian.org/786763) in redmine, which I hope to get into stable as well as soon as possible.

gem2deb has seen several improvements through versions [0.19](https://tracker.debian.org/news/698275), [0.20](https://tracker.debian.org/news/699499), [0.20.1](https://tracker.debian.org/news/699947) and [0.20.2](https://tracker.debian.org/news/701578).

I have updated a few packages:

-   [ruby-rubymail](https://tracker.debian.org/news/694910)
-   [ruby-ferret](https://tracker.debian.org/news/698034)
-   [ruby-omniauth](https://tracker.debian.org/news/698259)
-   [ruby-hashie](https://tracker.debian.org/news/698258)
-   [ruby-rack-accept](https://tracker.debian.org/news/699447)
-   [chef-zero](https://tracker.debian.org/news/698268)
-   [nailgun-agent](https://tracker.debian.org/news/698665)
-   [ruby-serialport](https://tracker.debian.org/news/698716)
-   [ruby-gnome2](https://tracker.debian.org/news/698717)
-   [ruby-mysql2](https://tracker.debian.org/news/699285)
-   [ruby-dataobjects-postgres](https://tracker.debian.org/news/699949)
-   [ruby-standalone](https://tracker.debian.org/news/700007)
-   [thin](https://tracker.debian.org/news/700872)
-   [ruby-stringex](https://tracker.debian.org/news/701694)
-   [ruby-i18n](https://tracker.debian.org/news/701693)

Two NEW packages, [ruby-rack-contrib](https://tracker.debian.org/news/698262) and [ruby-grape-logging](https://tracker.debian.org/news/701355) ,were ACCEPTED into the Debian archive. Kudos to the ftp-master team who are doing an awesome job reviewing new packages really fast.

**Debian Continuous Integration**

This month I have made good progress with the changes needed to make debci work as a distributed system with one master/scheduler node and as many worker nodes (running tests) as possible.

While doing my tests, I have submitted a [patch to lxc](https://lists.linuxcontainers.org/pipermail/lxc-devel/2015-July/012003.html) and updated [autodep8](https://tracker.debian.org/news/699950) in unstable. At some point I plan to upload both autodep8 and autopkgtest to jessie-backports.

**Sponsoring**

I have sponsored a few packages:

-   [ruby-rack-mount](https://tracker.debian.org/news/698263), [ruby-grape-entity](https://tracker.debian.org/news/698317), and [ruby-grape](https://tracker.debian.org/news/698320) for Hleb Valoshka.
-   [redir](https://tracker.debian.org/news/689030) and [tmate](https://tracker.debian.org/news/698913) [twice](https://tracker.debian.org/news/701152) for Lucas Kanashiro.
-   [lxc to wheezy-backports](https://lists.debian.org/debian-backports-changes/2015/08/msg00037.html) for Christian Seiler.

