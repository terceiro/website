---
old: true
title: "Chamada de grupos de usuários Debian no Brasil"
created_at: "2012-03-23 22:17 -3"
kind: article
---

Um tempo atrás o servidor de listas do CIPSGA, onde ficavam as listas
dos grupos de usuários Debian, subiu no telhado.

A partir de hoje vamos recriar as listas dos grupos de
usuários na infraestrutura do próprio Debian. Pra isso, vamos utilizar o projeto [debian-br no alioth](http://alioth.debian.org/projects/debian-br) e criar as listas lá dentro.

A lista principal, usada pra articulação dos grupos de usuários regionais (ou seja, a lista antes conhecida como “debian-br”), vai ser a [debian-br-geral](https://lists.alioth.debian.org/mailman/listinfo/debian-br-geral)

O padrão de nomes para as listas regionais vai ser o seguinte: debian-br-gud-`$uf`, onde `$uf` é a sigla do estado. Por exemplo, as listas para o Debian-RS e Debian-BA já foram criadas, e são chamadas debian-br-gud-rs debian-br-gud-ba, respectivamente.

Para solicitar a criação de novas listas, favor criar um [novo tíquete](http://alioth.debian.org/tracker/?group_id=30064) no projeto debian-br, do tipo “Support Request”.

Infelizmente, não se tem backup sequer da lista de e-mails inscritos; por isso, além de recriar as listas vai ser necessário que todo mundo se inscreva novamente. Depois da criação da lista do seu estado, favor espalhar a notícia!

