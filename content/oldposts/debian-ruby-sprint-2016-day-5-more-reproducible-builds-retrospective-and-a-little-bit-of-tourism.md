---
old: true
title: "Debian Ruby Sprint 2016 - day 5: More Reproducible Builds, Retrospective, and A Little Bit of Tourism"
created_at: "2016-03-05 16:21 -3"
kind: article
tags: [debian, ruby]
---

Earlier today I was made aware by Holger of [the results](https://tests.reproducible-builds.org/unstable/amd64/stats_meta_pkg_state_maint_pkg-ruby-extras-maintainers.png) of our reproducibility efforts during the sprint. I would like to thank Lunar for pinging us about the issue, and Holger for pointing me to updated results. The figure below depicts a stacked area chart where the X axis is time and the green area is reproducible packages. Red is packages that fail to build, and Orange are unreproducible packages

[![](/oldposts/images/reproducible.png)](https://tests.reproducible-builds.org/unstable/amd64/stats_meta_pkg_state_maint_pkg-ruby-extras-maintainers.png)

I was able to book accommodation for the sprint attendees very close to both my place and the sprint venue, what was very useful but also had this downside of them not being able to see much of city. As the final day of the sprint was getting closer, we decided to have a different lunch to allow them to see one of the most famous local landmarks, the botanical gardens.

So we headed down to the botanical gardens, grabbed a few items for lunch at the park coffee shop, and set out to visit this very beautiful place. I have to say that there is the place were I usually take every visitor I have. We were joined by Gioavani who had just arrived for the [the MiniDebconf](http://br2016.mini.debconf.org/) on the following weekend.

![](/oldposts/images/botanical-gardens.jpg)

The final lists of accomplishments of the day was again very impressive

-   r10k 2.1.1-2
-   run massive update on team repositories
    -   bump Standards-Version
    -   fix Vcs-\* fields
    -   drop version in gem2deb build-dependency
    -   set debhelper compatibility level to 9
    -   update the default ruby-tests.rake
-   day 4 report
-   uploaded ruby-faraday-middleware-multi-json 0.0.6-2
-   uploaded ruby-powerpack 0.1.1-2
-   uploaded ruby-contracts 0.13.0-1
-   uploaded ruby-chef-config 12.7.2-1 (NEW)
-   uploaded ruby-foreigner \#808530 and asked it removal from the NEW queue (was already ROMed)
-   filled for RM ruby-opengraph-parser (\#816752)
-   new how-can-i-help version developed and uploaded
-   uploaded ruby-romkan to unstable (from exp)
-   uploaded ruby-rinku to unstable (from exp)
-   uploaded ruby-ole to unstable (from exp)
-   uploaded ruby-net-ldap to unstable (from exp)
-   uploaded ruby-rack-mobile-detect to unstable (from exp)
-   uploaded gem2deb 0.28 to help with reproducible builds: filenames are now sorted
-   uploaded rails 2:4.2.5.2-2 with packaging improvements
    -   run unit tests during the build and on CI
    -   apply upstream patch to fix ActiveRecord breakage under Ruby 2.3
-   pushed a ton of tags for existing uploads
-   merged improvements to the team master repository
    -   review/cleanup the contents of the repository
    -   improved helper scripts to automate the workflow (upload, build, new-upstream, etc)
-   followed up on ruby2.3 transition, filed \#816698 against subversion because of ftbfs on mips, mipsel
-   put ruby-cocoon into a better state
-   uploaded ruby-plist
-   gem2deb: gem2tgz will now create foo.gemspec (easier to patch) instead of metadata.yml
-   gemwatch: ditto
-   close \#794139 jekyll bug (unreproducible)
-   close \#798934 ruby-ffi-rzmq bug (unreproducible)
-   closed ftbfs \#816586 \#800057 \#784699 as unreproducible
-   reassigned \#760952 \#680297 to ruby2.3 (from ruby2.2)
-   investigated how to list packages with non-buildd-binary uploads
-   ScottK has removed ruby2.1 from unstable!

By the end of the afternoon I asked everyone to fill out a simple retrospective list, what we can use later to make future sprints better and better. Below are the results we got.

What was good:

-   restricted room hours actually made for a nice rhythm (did not apply for a long time…)
-   very good food
-   very cheap food!
-   longer period makes the effort of travel more worthwhile
-   many participants and longer sprint than usual allowing more work to be done
-   good preparation with clear goals, make the sprint usefull
-   patience with the less experienced participants
-   RFS very fast
-   Antônio is an excellent host
-   You all are so helpful
-   great dinners

What could be better:

-   room too close to the street, too much vehicle noise, but sometimes nice music
-   more coffee \^W meat
-   could know more portugues so ordering food would have been easier
-   debian infra could have not been down during the sprint

The night ended at [Bar do Alemão](http://www.bardoalemaocuritiba.com.br/) (“The German’s Bar”). Both their beer and their food are very good, but I don’t have enough elements to vouch for their authenticity. :) We were joined by Giovani (who we also met earlier in the botanic gardens), and by Paulo and Daniel who are organizing the MiniDebconf.

![](/oldposts/images/bar-do-alemao.jpg)

And that is the end of this year’s Debian Ruby team sprint. I hope we do it all over again next year.

