---
old: true
title: "How to make a grad student freak out"
created_at: "2012-05-20 10:43 -3"
kind: article
tags: [phd]
---

So that grad student has just submitted his thesis and you are
invited to review it. With these quick tips, you can test the
limits of the student’s willpower, and make his degree a little
more deserved by putting him up for some constructive mind games.

![](/oldposts/images/how-to-make-a-grad-student-freak-out.png)

