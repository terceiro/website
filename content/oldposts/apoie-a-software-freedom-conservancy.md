---
old: true
title: "Apoie a Software Freedom Conservancy"
created_at: "2015-12-02 16:02 -3"
kind: article
tags: [conservancy]
---

A [Software Freedom Conservancy](https://sfconservancy.org/) é uma organização sem fins lucrativos que fornece “personalidade jurídica” a projetos de software livre, o que possibilita que os projetos recebam doações, emitam recibos sem precisarem passarem pelo processo árduo de formalizarem as suas próprias organizações.

![Projetos membros da Conservancy: QEMU, git, Inkscape, Samba, e vários outros](/oldposts/images/conservancy-member-project-logos.png "Projetos membros da Conservancy: QEMU, git, Inkscape, Samba, e vários outros")

Uma outra atividade muito importante que a Conservancy desempenha é a de combater violações da GPL para os projetos membros. Por exemplo ela no momento está numa [batalha judicial contra a VMWare](https://sfconservancy.org/copyleft-compliance/vmware-lawsuit-faq.html) onde a mesma é acusada de violar da GPL no Linux (o kernel, como você já sabe) na distribuição de um de seus produtos proprietários de virtualização.

Na última Debconf em Agosto deste ano, a Software Freedom Conservancy [anunciou um programa](https://sfconservancy.org/news/2015/aug/17/debian/) onde membros do Debian podem escolher delegar à Conservancy os direitos de garantir o cumprimento da GPL nos seus projetos.

Garantir o cumprimento da GPL é uma tarefa árdua, e precisa de recursos para poder pagar as pessoas. E pode te trazer inimigos poderosos: por exemplo, em função da ação contra a VMWare a Conservancy teve palestras em eventos canceladas na última hora, e perdeu patrocinadores.

Para dar continuidade às suas atividades, a Conservancy está com [uma campanha](https://sfconservancy.org/supporter/) para ter o apoio de *indivíduos*, de forma que os patrocinadores corporativos tenham menos peso no seu financiamento. Se você se importa com a GPL e pode contribuir, por favor [faça a sua parte](https://sfconservancy.org/supporter/)!

[![Torne-se um apoiador da Conservancy](/oldposts/images/conservancy-supporter-badge.png "Torne-se um apoiador da Conservancy")](https://sfconservancy.org/supporter/)

