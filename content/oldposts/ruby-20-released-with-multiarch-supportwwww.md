---
old: true
title: "Ruby 2.0 released, with multiarch support^W^W^W^W"
created_at: "2013-02-24 15:31 -3"
kind: article
tags: [debian, ruby]
---

[Ruby 2.0 was released](http://www.ruby-lang.org/en/news/2013/02/24/ruby-2-0-0-p0-is-released/) today. This new version of the language brings some very interesting features, and according to the core team, an effort has been made to keep source level compartibility with Ruby 1.9.

Debian packaging [is under way](http://anonscm.debian.org/gitweb/?p=collab-maint/ruby2.0.git;a=summary) and should hit NEW soon. During the last few days I gave more attention to getting the new multiarch support [fixed](http://bugs.ruby-lang.org/issues/7874) [upstream](http://bugs.ruby-lang.org/issues/7874) than to the packaging bits, but the remaining packaging work should be pretty much about housekeeping.

Next steps from a Debian point of view (after Wheezy is out) include:

-   add Ruby 2.0 support in gem2deb (should be trivial).
-   check what packages need fixing to support Ruby 2.0, and which are broken beyond repair.
-   figure out how to better exploit a multiarch-enabled Ruby.

Now let’s get back to fixing [RC bugs](http://udd.debian.org/bugs.cgi?release=wheezy_and_sid&amp;patch=ign&amp;merged=ign&amp;done=ign&amp;fnewerval=7&amp;rc=1&amp;sortby=id&amp;sorto=asc&amp;ctags=1&amp;ctags=1&amp;cdeferred=1) and getting Wheezy released. :-)

**UPDATE 2013-03-06:** actually the multiarch support is broken in 2.0.0, and the bugs I reported were only fixed in trunk. I will probably backport those fixes in the Debian package.

