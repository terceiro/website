---
old: true
title: "testing build reproducibility with debrepro"
created_at: "2016-09-03 16:58 -3"
kind: article
---

Earlier today I was handling a [reproducibility bug](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=829362) and decided I had to try a reproducibility test by myself. I tried [reprotest](https://packages.debian.org/reprotest), but I was being hit by a disorderfs issue and I was not sure whether the problem was with reprotest or not (at this point I cannot reproduce that anymore).

So I decided to hack a simple script to that, and it works. I even [included it in devscripts](https://anonscm.debian.org/cgit/collab-maint/devscripts.git/commit/?id=f3fd7952b8d38438f193076521f9067d742f9c94) after writing a manpage. Of course reprotest is more complete, extensible, and supports arbitrary virtualization backends for doing the more dangerous/destructive variations (such as changing the hostname and other things that require root) but for quick tests `debrepro` does the job.

Usage examples:

```
$ debrepro                                 # builds current directory
$ debrepro /path/to/sourcepackage          # builds package there
$ gbp-buildpackage --git-builder=debrepro  # can be used with vcs wrappers as well
```

`debrepro` will do two builds with a few variations between them, including `$USER`, `$PATH`, timezone, locale, umask, current time, and will even build under disorderfs if available. Build path variation is also performed because by definition the builds are done in different directories. If diffoscope is installed, it will be used for deep comparison of non-matching binaries.

If you are interested and don’t want to build devscripts from source or wait for the next release, you can just [grab the script](https://anonscm.debian.org/cgit/collab-maint/devscripts.git/plain/scripts/debrepro.sh?id=f3fd7952b8d38438f193076521f9067d742f9c94), save it as “debrepro” somewhere on your `$PATH` and make it executable.

