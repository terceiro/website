---
old: true
title: "Computação Brasil #27: Software Livre"
created_at: "2015-06-24 17:29 -3"
kind: article
---

A [Computação Brasil](http://www.sbc.org.br/index.php?option=com_content&amp;view=category&amp;layout=blog&amp;id=245&amp;Itemid=197) é revista publicada pela Sociedade Brasileira de Computação (SBC), e publica edições temáticas cobrindo assuntos importantes na Computação. A ultima edição, de número 27, é dedicada ao Software Livre.

Citando o editorial, escrito pelo professor Paulo Roberto Freire Cunha, presidente da SBC:

> Software Livre é hoje um assunto importantíssimo para toda a sociedade, ultrapassando a questão ideológica e tornando-se um ecossistema complexo e de interesse global, que inclui pesquisa científica, educação, tecnologia, segurança, licença de uso e políticas públicas.
>
> Um ponto fundamental é o impacto que ele gera no mercado, atuando hoje como um facilitador para milhares de startups em todo o mundo. Por isso, tem um papel essencial num país como o Brasil, onde o sucesso do empreendedorismo e da inovação tecnológica está diretamente ligado à redução de custos, à criatividade e à flexibilidade.
>
> Para destacar o assunto, a revista traz diversos especialistas que mostrarão as mais recentes análises e pesquisas sobre Software Livre, com o propósito de estimular o leitor a fazer uma reflexão sobre o futuro do desenvolvimento de plataformas com código aberto e suas aplicações em diferentes frentes.
>

Eu e o [Paulo](http://softwarelivre.org/paulormm) escrevemos juntos 2 textos para esta edição. O primeiro, com co-autoria da professora [Christina Chavez](http://www.dcc.ufba.br/~flach), que foi minha orientadora de doutorado, é sobre Controle de Qualidade em projetos de software livre. O segundo, em autoria com o [Rodrigo Maia](http://rodmaia.net/), é sobre a [reformulação do Software Público Brasileiro](https://portal.softwarepublico.gov.br/), projeto no qual trabalhamos juntos desde a metade e 2014.

A [versão online](http://www.sbc.org.br/index.php?option=com_flippingbook&amp;view=book&amp;id=22) da revista é meio tosca, se como eu você não tiver Flash instalado (não tenho idéia de como seja a versão com Flash). Mas a [versão em PDF](http://www.sbc.org.br/downloads/CB2015/02-2015_18.06.pdf) é bem razoável.

