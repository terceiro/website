---
old: true
title: "rmail: reviving upstream maintaince"
created_at: "2015-02-15 13:37 -3"
kind: article
---

It is always fun to write new stuff, and be able to show off that shiny new piece of code that just come out of your brilliance and/or restless effort. But the world does not spin based just on shiny things; for free software to continue making the world work, we also need the dusty, and maybe and little rusty, things that keep our systems together. Someone needs to make sure the rust does not take over, and that these venerable but useful pieces of code keep it together as the ecosystem around them evolves. As you know, Someone is probably the busiest person there is, so often you will have to take Someone’s job for yourself.

`rmail` is a Ruby library able to parse, modify, and generate MIME mail messages. While handling transitions of Ruby interpreters in Debian, it was one of the packages we always had to fix for new Ruby versions, to the point where the Debian package has accumulated [quite a few patches](http://anonscm.debian.org/cgit/pkg-ruby-extras/ruby-rubymail.git/tree/debian/patches?id=debian/1.0.0-2). The situation became ridiculous.

It was considered to maybe drop it from the Debian archive, but dropping it would mean either also dropping [feed2imap](https://github.com/feed2imap/feed2imap) and [sup](http://supmua.org/) or porting both to other mail library.

Since doing this type of port is always painful, I decided instead to do something about the sorry state in which rmail was on the upstream side.

The reasons why it was not properly maintained upstream does not matter: people lose interest, move on to other projects, are not active users anymore; that is normal in free software projects, and instead of blaming upstream maintainers in any way we need to thank them for writing us free software in the first place, and step up to fix the stuff we use.

I got in touch with the people listed as owner for the package on rubygems.org, and got owner permission, which means I can now publish new versions myself.

With that, I cloned [the repository](https://github.com/matta/rubymail) where the original author had imported the latest code uploaded to rubygems and had started to receive contributions, but that repository was inactive for more than one year. It had already got some contributions from the sup developers which never made it in a new rmail release, so the sup people started using their own fork called “rmail-sup”.

Already in [my repository](https://github.com/terceiro/rmail), I have imported all the patches that still made sense from the Debian repository, did a bunch of updates, mainly to modernize the build system, and did a [1.1.0 release to rubygems.org](https://rubygems.org/gems/rmail). This release is pretty much compatible with 1.0.0, but since I did not test it with Ruby versions older than than one in my work laptop (2.1.5), I bumped the minor version number as warning to prospective users still on older Ruby versions.

In this release, the test suite passes 100% clean, what always gives my mind a lot of comfort:

```
$ rake
/usr/bin/ruby2.1 -I"lib:." -I"/usr/lib/ruby/vendor_ruby" "/usr/lib/ruby/vendor_ruby/rake/rake_test_loader.rb" "test/test*.rb"
Loaded suite /usr/lib/ruby/vendor_ruby/rake/rake_test_loader
Started
...............................................................................
...............................................................................
........

Finished in 2.096916712 seconds.

166 tests, 24213 assertions, 0 failures, 0 errors, 0 pendings, 0 omissions, 0 notifications
100% passed

79.16 tests/s, 11546.95 assertions/s
```

And in the new release I have just uploaded to the Debian experimental suite (1.1.0-1), I was able to drop **all of the patches** and just use the upstream source as is.

So that’s it: if you use rmail for anything, consider testing version 1.1.0-1 from Debian experimental, or 1.1.0 from rubygems.org if you into that, and report any bugs to the \[github repository\](https://github.com/terceiro/rmail). My only commitment for now is keep it working, but if you want to add new features I will definitively review and merge them.

