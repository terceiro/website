---
old: true
title: "Qualidade de Código: mantendo seu projeto de software sob controle"
created_at: "2009-06-27 18:26 -3"
kind: article
---

Essa foi a minha palestra no fisl10. Os slides estão disponíveis para download [aqui](/oldposts/files/fisl10/qualidade-de-codigo.odp).

A palestra estava cheia., mas eu acho que no final das contas a palestra pode ter ficado teórica demais. Se alguém tiver comentários, eu ficaria muito feliz em recebê-los aqui nesse post.

