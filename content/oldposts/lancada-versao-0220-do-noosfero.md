---
old: true
title: "Lançada versão 0.22.0 do Noosfero"
created_at: "2010-02-11 16:13 -3"
kind: article
tags: [noosfero]
---

É com grande prazer que gostaríamos de anunciar o lançamento da [](/oldposts/images/noosfero-0.22.0-small.png?1265919065)  
*Novo tema padrão do Noosfero*

### Para usuários

-   **Novo bloco de slideshow** ([\#1357](http://colivre.coop.br/Noosfero/ActionItem1357 "Action item #1357"), [\#1358](http://colivre.coop.br/Noosfero/ActionItem1358 "Action item #1358")).  Agora você pode ter blocos que mostram suas fotos, e você ainda pode escolher se quer exibí-las na ordem ou em ordem aleatória, e se quer exibir ou não controles de navegação (anterior/pause/próxima).

-   **Melhor navegação em galerias de imagens** ([\#1367](http://colivre.coop.br/Noosfero/ActionItem1367 "Action item #1367")). Agora você pode navegar entre as imagens de uma galeria de imagens através de convenientes botões "anterior" e "próxima", mesmo fora do modo de slideshow em tela cheia.

-   **Convite para entrar em comunidades** ([\#1275](http://colivre.coop.br/Noosfero/ActionItem1275 "Action item #1275")). Os administradores de comunidades podem convidar por e-mail os seus amigos para entrarem em suas comunidades. Assim como na funcionalidades de convidar amigos, você pode tanto informar manualmente uma lista de endereços de e-mail ou pesquisa na sua lista de contatos dos principais serviços de webmail. Para convidar, basta clicar no novo ícone na listagem de membros da comunidade, ou em "Painel de Controle" → "Gerenciar membros".

-   **Perfil privados** ([\#1273](http://colivre.coop.br/Noosfero/ActionItem1273 "Action item #1273")). Agora perfis (usuários ou comunidades) privados são listados em resultados de busca e em outros lugares. Quando alguém tenta visualizar o conteúdo daquele perfil sem ser um mebro (ou amigo, no caso de perfis de usuários), uma mensagem interessante é exibida e o visitante tem a possibilidade se solicitar entrada na comunidade (ou adicionar como amigo, no caso de perfil de usuários).

    **Página de perfil melhorada**. Agora o perfil dos usuários é exibido num estilo muito mais interessante e com mais informações úteis.

-   **Documentação online** ([\#1359](http://colivre.coop.br/Noosfero/ActionItem1359 "Action item #1359"), [\#1360](http://colivre.coop.br/Noosfero/ActionItem1360 "Action item #1360")). O Noosfero agora conta com infra-estrutura para exibir a sua própria documentação online. Os usuários podem acessar minharede/doc no Noofero e ler a documentação fornecida no seu idioma nativo. Por enquanto, existe documentação das seguintes funcionalidades:

    -   Gestão de conteúdo : postar no blog, adicionar imagens numa galeria, edição avançada de texto (inserção de imagens e links), gerenciar seu conteúdo, criar um blog, escrever um artigo.
    -   Funcionalidades de usuário: Sair do Sistema, Remover Amigos, Enviar mensagens, Remover comentários, Aceitar Amigos, Editar configurações do usuário, Comentar artigos, Adicionar Amigos, Registro de novo usuário, Entrar numa comunidade, Entrar no sistema.
    -   Funcionalidades para empreendimentos: Desabilitar o empreendimento, Ativar o Empreendimento, Gerenciar e Adicionar membros ao empreendimento, Editar configurações do empreendimento.
    -   Navegação: Buscando no sistema, Encontrando pessoas, Encontrando produtos e serviços,  Encontrando Empreendimentos, Encontrando comunidades, Busca Avançada

    Se você ficar com vontade de escrever documentação para o Noosfero, sinta-se à vontade para nos contactar.

-   **A tradução para Alemão foi atualizada**, novamente graças ao Ronny Kursawe da [Faculdade de Ciência da Computação da Universidade Tecnológica de Dresden](http://www.inf.tu-dresden.de/)
-   **A tradução para Porrtuguês foi atualizada**
-   Foram feitos também diversas pequenas **melhorias de usabilidade**, como por exemplo no diálogo de inslusão d enovos blocos e no diálogo de login/logout.

Para desenvolvedores

-   Foi **adicionado suporte para testar temas específicos** em modo de desenvolvimento. Se você passar um parâmetro `theme` pela URL, Noosfero vai exibir o tema indicado ao invés do tema atual do ambiente. Por exemplo, `http://localhost:3000/?theme=mytheme` vai exibir a homepage do Noosfero usando o tema "mytheme" ao invés do tema atualmente configurado para o ambiente.

-   **O comando `rake test` agora roda também os testes no selenium** através do [xvfb](http://packages.debian.org/lenny/xvfb). Assim nosso servidor de integração contínua poderá rodar sempre **todos** os teste.

-   Talvez a funcionalidade mais importante para desenvolvedores seja o  **novo tema padrão**. Baseado no tema do [Software Livre Brasil](/), o novo tema fornece uma experiência inicial muito melhor. Na verdade existem dois novos temas, o tema "base" define a formatação de todo o sistema, e o tema "noosfero" apenas herda toda essa formatação e inclui o logo "Noosfero" na barra superior. Desenvolvedores podem usa uma abordagem parecida para os seus próprios temas: nós recomandamos reusar a maioria da formatação de temanhos e positionamento do tema base e apenas mudar cores, imagens e fundos nos seus temas. Desta forma o trabalho de criar um tema é bastante reduzido e há menos risco de que o tema não forneça o estilo necessário para a interface de usuário do Noosfero. Confira o código do tema noosfero para ver como fazer.

    **Novo script "quick start" para Debian.** Usuário de Debian agora tem um um script simples que instala automaticamente tudo que é necessário para rodar uma instância de desenvolvimento do Noosfero. Basta baixar o código, rodar  `./script/quick-start-debian` e apontar o navegador para [http://localhost:3000](http://localhost:3000/) e começar a testar o Noosfero! Esta facilidade é dada aos usuários Debian porque nós usamos Debian na Colivre. Encorajamos a comunidade a contribuir scripts parecidos para ajudar usuários de outros sistemas a iniciar a contribuir com o Noosfero.

### Sobre o projeto Noosfero

O [Noosfero](http://noosfero.org/) é uma plataforma web para redes sociais que possui as funcionalidades de Blog, e-Portfolios, RSS, discussão temática e agenda de eventos num mesmo sistema! O projeto é liderado pela [Colivre](http://colivre.coop.br/), e é licenciado sob a [GNU Affero General Public License](http://www.gnu.org/licenses/agpl.html).

