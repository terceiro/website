---
old: true
title: "Lançado Noosfero 0.23.0!"
created_at: "2010-04-14 10:00 -3"
kind: article
tags: [noosfero]
---

É com alegria que anunciamos o lançamento da versão 0.23.0 do seu mais querido
software livre para redes sociais. :-)

Novas funcionalidades (para usuários)
-------------------------------------

Esta versão traz várias melhorias de usabilidade, o que torna a experiência inicial do usuário muito melhor.

-   **Link visível para feeds RSS ([\#1475](http://colivre.coop.br/Noosfero/ActionItem1475 "Action item #1475"))**. Agora o Noosfero vai mostrar um ícone de feed RSS ao lado da descrição do blog. Este link vai ajudar usuários cujo browser não exibe ícones de feed na barra de endereço, como por exemplo usuários de plataformas embarcadas (smartphones) e de quiosques de internet usando tela cheia
-   **Melhorias na caixa de notificação ([\#1474](http://colivre.coop.br/Noosfero/ActionItem1474 "Action item #1474"))**. Alteramos o estilo padrão da caixa de notificação para que ela seja mais visível, e ao mesmo tempo continue sem atrapalhar.
-   **Layout com largura fixa ([\#1449](http://colivre.coop.br/Noosfero/ActionItem1449 "Action item #1449"))**. O tema padrão agora tem largura fixa. Dessa forma é mais fácil antecipar como o Noosfero vai aparecer para os usuários indenpendente da sua resolução de tela. Nós encorajamos a todos que estejam fazendo temas a fazerem o mesmo.
-   **Desbloqueio de empreendimentos ([\#1402](http://colivre.coop.br/Noosfero/ActionItem1402 "Action item #1402"))**. Quando um empreendimento é bloqueado após uma tentativa não sucedidade de ativação, o adminitrador do ambiente poderá agora desbloquear o empreendimento diretamente através da interface do sistema.
-   **Registro em configurações multi-ambiente  ([\#1448](http://colivre.coop.br/Noosfero/ActionItem1448 "Action item #1448"))**. Com essa mudança, agora é possível ter usuários com mesmo login ou e-mail em ambientes diferentes no mesmo servidor.
-   Por último, mas não menos importante, agradecemos à [Ynternet.org](http://ynternet.org/) por financiar várias melhorias de usabilidade. 

Melhorias de infra-estrutura
----------------------------

-   **Otimização para os clientes** **([\#1425](http://colivre.coop.br/Noosfero/ActionItem1425 "Action item #1425"))**. Temos conduzido estudos de desempenho do Noosfero, e uma das coisas em que decidimos trabalhar foi na otimização do HTML gerado pelo Noosfero. Reduzindo o úmero de arquivos externos referenciados pelo HTML (folhas de estilo e arquivos javascript), e adicionando configurações no servidor web que instruam os clientes a fazer cache local desses arquivos, conseguimos melhorar nossa nota de **E** para **B** na escala do [YSlow](http://developer.yahoo.com/yslow/).
-   **Deixando** **[Ruby-GetText](http://www.yotabanana.com/hiki/ruby-gettext.html) em favor de [fast_gettext](http://github.com/grosser/fast_gettext) na infra-estrutura de tradução ([\#1315](http://colivre.coop.br/Noosfero/ActionItem1315 "Action item #1315"))**. Nós temos lutado contra problemas na tradução que eram difíceis, ou mesmo impossíveis, de reproduzir localmente. Em sites de alto tráfego, parece que algumas instâncias de mongrel ficavam travadas num único idioma e os usuários eram surpreendidos por mensagens em idiomas diferentes daquele de sua preferência. Nossa hipótese é de que esse problema era causado pela falta de *thread-safety*(\*) tanto no Rails 2.1 como no Ruby-GetText (pelo menos na versão no Debian estável). Nós então substituímos Ruby-Gettext com fast\_gettext. Além de ser *thread-safe(\*)*, fast\_gettext é bem mais rápida e consome menos memória do que Ruby-GetText. Nós ainda usamos a Ruby-GetText para a gestão das traduções, porém. Como nós nunca conseguimos produzir os problemas na tradução localmente, não podemos ter certeza de que isso vai resolver o problema. Mas nossa análise cuidadosa demonstrou analiticamente que o problema será de fato resolvido.

(\*) NT: infelizmente não conseguimos uma boa tradução para *thread-safety* e *thread-safe*. O conceito está relacionado à capacidade de um software de atender requisições simultâneas sem que uma interfira na outra.

Sobre o projeto Noosfero
------------------------

O [Noosfero](http://noosfero.org/) é uma plataforma web livre para redes sociais que possui as funcionalidades de Blog, e-Portfolios, RSS, discussão temática e agenda de eventos num mesmo sistema! O Noosfero utiliza a linguagem de programação *[Ruby](http://www.ruby-lang.org/pt/)* com *[framework Rails](http://www.rubyonrails.com.br/)* e, portanto, suporta bancos de dados, PostgreSQL, MySQL, SQLite entre [outros](http://wiki.rubyonrails.org/rails/pages/DatabaseDrivers).

Mais informações sobre o projeto Noosfero podem ser encontradas no seu site oficial: [noosfero.org](http://noosfero.org/). Informações sobre suporte comercial e serviços relacionados podem ser obtidas com a [Colivre](http://colivre.coop.br/).

