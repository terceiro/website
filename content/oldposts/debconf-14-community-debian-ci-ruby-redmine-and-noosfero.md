---
old: true
title: "DebConf 14: Community, Debian CI, Ruby, Redmine, and Noosfero"
created_at: "2014-09-01 22:46 -3"
kind: article
tags: [debconf, debian, debian-ci, redmine, ruby, noosfero]
---

This time, for personal reasons I wasn’t able to attend the full DebConf, which started on the Saturday August 22nd. I arrived at Portland on the Tuesday the 26th by noon, at the 4th of the conference. Even though I would like to arrive earlier, the loss was alleviated by the work of the amazing DebConf video team. I was able to follow remotely most of the sessions I would like to attend if I were there already.

As I will say to everyone, DebConf is for sure the best conference I have ever attended. The technical and philosophical discussions that take place in talks, BoF sessions or even unplanned ad-hoc gathering are deep. The hacking moments where you have a chance to pair with fellow developers, with whom you usually only have contact remotely via IRC or email, are precious.

That is all great. But definitively, catching up with old friends, and making new ones, is what makes DebConf so special. Your old friends are your old friends, and meeting them again after so much time is always a pleasure. New friendships will already start with a powerful bond, which is being part of the Debian community.

Being only 4 hours behind my home time zone, jetlag wasn’t a big problem during the day. However, I was waking up too early in the morning and consequently getting tired very early at night, so I mostly didn’t go out after hacklabs were closed at 10PM.

Despite all of the discussion, being in the audience for several talks, other social interactions and whatnot, during this DebConf I have managed to do quite some useful work.

### debci and the Debian Continuous Integration project

I gave a talk where I discussed past, present, and future of debci and the Debian Continuous Integration project. The [slides](/oldposts/files/debconf14/debci-debian-ci.pdf) are available, as well as the [video recording](http://meetings-archive.debian.net/pub/debian-meetings/2014/debconf14/webm/debci_and_the_Debian_Continuous_Integration_project.webm). One thing I want you to take away is that there is a difference between debci and the Debian Continuous Integration project:

-   [debci](http://packages.debian.org/debci) is a platform for Continuous Integration specifically tailored for the Debian repository and similar ones. If you work on a Debian derivative, or otherwise provides Debian packages in a repository, you can use debci to run tests for your stuff.
    -   a (very) few thinks in debci, though, are currently hardcoded for Debian. Other projects using it would be a nice and needed peer pressure to get rid of those.
-   [Debian Continuous Integration](http://ci.debian.net/) is Debian’s instance of debci, which currently runs tests for all packages in the unstable distribution that provide `autopkgtest` support. It will be expanded in the future to run tests on other suites and architectures.

A few days before DebConf, Cédric Boutillier managed to extract `gem2deb-test-runner` from `gem2deb`, so that `autopkgtest` tests can be run against any Ruby package that has tests by running `gem2deb-test-runner --autopkgtest`. `gem2deb-test-runner` will do the right thing, make sure that the tests don’t use code from the source package, but instead run them against the installed package.

Then, right after my talk I was glad to discover that the Perl team is also working on a similar tool that will automate running tests for their packages against the installed package. We agreed that they will send me a whitelist of packages in which we could just call that tool and have it do The Right Thing.

We might be talking here about getting `autopkgtest` support (and consequentially continuous integration) for free for almost ~~2000~~ 4000 packages. The missing bits for this to happen are:

-   making debci use a whitelist of packages that, while not having the appropriate `Testsuite: autopkgtest` field in the Sources file, could be assumed to have `autopkgtest` support by calling the right tool (`gem2deb-test-runner` for Ruby, or the Perl team’s new tool for Perl packages).
-   make the `autopkgtest` test runner assume a corresponding, implicit, `debian/tests/control` when it not exists in those packages

During a few days I have mentored Lucas Kanashiro, who also attended DebConf, on writing a patch to add support for email notifications in debci so maintainers can be pro-actively notified of status changes (pass/fail, fail/pass) in their packages.

I have also started hacking on the support for distributed workers, based on the initial work by Martin Pitt:

-   updated the `amqp` branch against the code in the master branch.
-   added a `debci enqueue` command that can be used to force test runs for packages given on the command line.
-   I sent [a patch for librabbitmq](https://github.com/alanxz/rabbitmq-c/pull/209) that adds support for limiting the number of messages the server will send to a connected client. With this patch applied, the debci workers were modified to request being sent only 1 message at a time, so late workers will start to receive packages to process as soon as they are up. Without this, a single connected worker would receive all messages right away, while a second worker that comes up 1 second later would sit idle until new packages are queued for testing.

### Ruby

I had some discusion with [Christian](http://zeha.at/) about making Rubygems install to `$HOME` by default when the user is not `root`. We discussed a few implementation options, and while I don’t have a solution yet, we have a better understanding of the potential pitfalls.

The Ruby BoF session on Friday produced a few interesting discussions. Some take away point include, but are not limited to:

-   Since the last DebConf, we were able to remove all obsolete Ruby interpreters, and now only have Ruby 2.1 in unstable. Ruby 2.1 will be the default version in Debian 8 (*jessie*).
-   There is user interest is being able to run the interpreter from Debian, but install everything else from Rubygems.
-   We are lacking in all the documentation-related goals for jessie that were proposed at the previous DebConf.

### Redmine

I was able to make [Redmine](http://packages.debian.org/redmine) work with the Rails 4 stack we currently have in unstable/testing. This required using a snapshot of the still unreleased version 3.0 based on the `rails-4.1` branch [in the upstream Subversion repository](https://svn.redmine.org/redmine/sandbox/rails-4.1) as source.

I am a little nervous about using a upstream snapshot, though. According to the "roadmap of the project ":http://www.redmine.org/projects/redmine/roadmap the only purpose of the 3.0 release will be to upgrade to Rails 4, but before that happens there should be a 2.6.0 release that is also not released yet. 3.0 should be equivalent to that 2.6.0 version both feature-wise and, specially, bug-wise. The only problem is that we don’t know what that 2.6.0 looks like yet. According to the roadmap it seems there is not much left in term of features for 2.6.0, though.

The updated package is not in unstable yet, but will be soon. It needs more testing, and a good update to the documentation. Those interested in helping to test Redmine on jessie before the freeze please get in touch with me.

### Noosfero

I gave a lighting talk on [Noosfero](http://noosfero.org/), a platform for social networking websites I am upstream for. It is a Rails appplication licensed under the AGPLv3, and there are [packages for wheezy](http://download.noosfero.org/debian/wheezy/). You can checkout the [slides](/oldposts/files/debconf14/noosfero.pdf) I used. Video recording is not available yet, but should be soon.

That’s it. I am looking forward to DebConf 15 at Heidelberg. :-)

