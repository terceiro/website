---
old: true
title: "Papo Livre Podcast, episodio #0"
created_at: "2017-05-23 13:00 -3"
kind: article
tags: [papolivre]
---

Podcasts têm sido um dos meus passatempos favoritos a um tempo. Eu acho que é um formato muito interssante, por dois motivos.

**Primeiro**, existem muitos podcasts com conteúdo de altíssima qualidade. Meu feed atualmente contém os seguintes (em ordem de assinatura):

-   [The Changelog](https://changelog.com/podcast)
-   [Request for Commits](https://changelog.com/rfc)
-   [FLOSS Weekly](https://twit.tv/shows/floss-weekly)
-   [Ruby Rogues](https://devchat.tv/ruby-rogues)
-   [Free as in Freedom](http://faif.us/)
-   [Joe Rogan Podcast](http://joerogan.net/podcasts/)
-   [Waking Up with Sam Harris](https://www.samharris.org/podcast)
-   [Nerdcast](https://jovemnerd.com.br/nerdcast/)
-   [Café Brasil](http://www.portalcafebrasil.com.br/)
-   [Anticast](http://anticast.com.br/podcast/anticast/)
-   [Mamilos](http://www.b9.com.br/podcasts/mamilos/)
-   [Ultrageek](http://www.redegeek.com.br/podcast/)
-   [Invisibilia](http://www.npr.org/podcasts/510307/invisibilia)
-   [Decrépitos](http://decrepitos.com/)
-   [PodProgramar](https://mundopodcast.com.br/podprogramar/)

Parece muito, e é. Ultimamente eu notei que estava ouvindo episódios com várias semanas de atraso, e resolvi priorizar episódios cujo tema me interessam muito e/ou que dizem respeito a temas da atualidade. Além disso desencanei de tentar escutar **tudo**, e passei a aceitar que vou deletar alguns itens sem escutar.

**Segundo**, ouvir um podcast não exige que você pare pra dar atenção total. Por exemplo, por conta de uma lesão no joelho que me levou a fazer cirurgia reconstrução de ligamento, eu estou condenado a fazer musculação para o resto da minha vida, o que é *um saco*. Depois que eu comecei a ouvir podcasts, eu *tenho vontade* de ir pra academia, porque agora isso representa o meu principal momento de ouvir podcast. Além disso, toda vez que eu preciso me deslocar sozinho pra qualquer lugar, ou fazer alguma tarefa chata mas necessária como lavar louça, eu tenho companhia.

Fazia um tempo que eu tinha vontade de fazer um podcast, e ontem oficialmente esse projeto se tornou realidade. Eu, [Paulo Santana](http://phls.com.br) e [Thiago Mendonça](http://acesso.me/) estamos lançando o [Podcast Papo Livre](https://papolivre.org/) onde discutiremos software livre em todos os seus aspectos.

No [primeiro episódio](https://papolivre.org/0/ "#0"), partindo da notícia sobre a vinda de Richard Stallman ao Brasil nas próximas semanas, discutimos as origens e alguns conceitos fundamentais do software livre.

