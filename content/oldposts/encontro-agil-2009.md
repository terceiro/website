---
old: true
title: "Encontro Ágil 2009"
created_at: "2009-10-01 17:27 -3"
kind: article
tags: [colivre, noosfero]
---

[![Encontro Ágil 2009](http://www.encontroagil.com.br/images/badget2009.png "Encontro Ágil 2009")](http://www.encontroagil.com.br) [](http://www.encontroagil.com.br)

O [Encontro Ágil](http://www.encontroagil.com.br/) é um dos principais eventos de métodos ágeis do Brasil.

No ano passado eu fiquei com vontade de ir, mas não deu. Esse ano não só eu vou, como vou fazer uma palestra sobre a experiência da [Colivre](http://colivre.coop.br/) com métodos ágeis, em especial no [Noosfero](https://gitlab.com/noosfero/noosfero). Na palestra eu vou descrever como é o processo de desenvolvimento na Colivre, como ele é diferente do processo ágil tradicional em função de várias peculiaridades nossas, do produto e dos clientes, e quais medidas a gente tomou e está tomando pra atenuar as dificuldades que essas peculiaridades trazem para um processo ágil. No site do evento tem um [resumo da palestra](http://www.encontroagil.com.br/principal/agenda1.jsf#palestra_antonio), dê uma olhada. ;-)

