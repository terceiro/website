---
old: true
title: "DebConf15, testing debian packages, and packaging the free software web"
created_at: "2015-08-30 19:12 -3"
kind: article
tags: [debconf]
---

This is my August update, and by the far the coolest thing in it is Debconf.

Debconf15
---------

I don’t get tired of saying it is the best conference I ever attended. First it’s a mix of meeting both new people and old friends, having the chance to chat with people whose work you admire but never had a chance to meet before. Second, it’s always quality time: an informal environment, interesting and constructive presentations and discussions.

This year the venue was again very nice. Another thing that was very nice was having so many kids and families. This was no coincidence, since this was the first DebConf in which there was organized childcare. As the community gets older, this a very good way of keeping those who start having kids from being alienated from the community. Of course, not being a parent yet I have no idea how actually hard is to bring small kids to a conference like DebConf. ;-)

I presented two talks:

-   **Tutorial: Functional Testing of Debian packages**, where I introduced the basic concepts of DEP-8/autopkgtest, and went over several examples from my packages giving tips and tricks on how to write functional tests for Debian packages.
    -   [Video recording](http://meetings-archive.debian.net/pub/debian-meetings/2015/debconf15/Tutorial_functional_testing_of_Debian_packages.webm) (webm, ~470MB)
    -   [slides](http://annex.debconf.org/debconf-share/debconf15/slides/173-tutorial-functional-testing-of-debian-packages.pdf) (PDF)
-   **Packaging the Free Software Web for the end user**, where I presented the motivation for, and the current state of [shak](https://gitlab.com/shak), a project I am working on to make it trivial for end users to install server side applications in Debian. I spent quite some hacking time during DebConf finishing a prototype of the shak web interface, which was demonstrated live in the talk (of course, as usual with live demos, not everything worked :-)).
    -   [Video recording](http://meetings-archive.debian.net/pub/debian-meetings/2015/debconf15/Packaging_the_free_software_web_for_the_end_user.webm) (webm, ~450MB)
    -   [slides](http://annex.debconf.org/debconf-share/debconf15/slides/174-packaging-the-free-software-web.pdf) (PDF)

There was also the now traditional Ruby BoF, where discussed the state and future of the Ruby ecosystem in Debian; and an *in promptu* Ruby packaging workshop where we introduced the basics of packaging in general, and Ruby packaging specifically.

Besides shak, I was able to hack on a few cool things during DebConf:

-   debci [has been updated](https://tracker.debian.org/news/705444) with a first version of the code to produce britney hints files that block packages that fail their tests from migrating to testing. There are some issues to be sorted out together with the release team to make sure we don’t block packages unecessarily, e.g. we don’t want to block packages that never passed their test suite — most the test suite, and not the package, is broken.
-   while hacking I ended up [updating jquery](https://tracker.debian.org/news/706879) to the newest version in the 1.x series, and in fact adopting it I guess. This allowed me to drop the embedded jquery copy I used to have in the shak repository, and since then I was able to [improve the build](https://tracker.debian.org/news/708523) to produce an output that is identical, except for a build timestamp inside a comment and a few empty lines, to the one produced by upstream, without using grunt.

Miscellaneous updates
---------------------

-   [Rails 4.2 in unstable](https://wiki.debian.org/Teams/Ruby/Rails4.2): in order to support Diaspora (currently in experimental), and an upcoming Gitlab package (WIP). This requires quite some updates, NEW packages, and also making sure that Redmine is updated to a new upstream version. I did a few updates as part of this effort:
    -   rails 2:4.2.3-3
    -   ruby-arel 6.0.3-1
    -   ruby-coffee-script 2.4.1-1
    -   ruby-coffee-script-source 1.9.1.1-1
    -   ruby-commander 4.3.5-1
    -   ruby-execjs 2.4.0-1
    -   ruby-globalid 0.3.6-1
    -   ruby-jbuilder 2.3.1-1
    -   ruby-jquery-rails 4.0.4-2
    -   ruby-minitest 5.8.0-1
    -   ruby-multi-json 1.11.2-1
    -   ruby-rack-test 0.6.3-1
    -   ruby-sass-rails 5.0.3-1
    -   ruby-spring 1.3.6-1
    -   ruby-sprockets 3.3.0-1~exp2
    -   ruby-sprockets-rails 2.3.2-1~exp1
    -   ruby-sqlite3 1.3.10-1
    -   ruby-turbolinks 2.5.3-1
-   [rerun](https://packages.debian.org/sid/rerun) (NEW), a tool to launch commands and restart them on filesystem change. Very useful when writing sinatra/rack applications.
-   vagrant: [new upstream relaese](https://tracker.debian.org/news/704807), supporting VirtualBox 5.0
-   pinpoint: [new upstream release](https://tracker.debian.org/news/702727), ported to clutter-gst-3.0
-   chake: [new upstream release](https://tracker.debian.org/news/703099)
-   gem2deb: [new release](https://tracker.debian.org/news/705442) with several improvements, and a [bug fix followup](https://tracker.debian.org/news/705744)
-   chef: [fix installation of initscripts](https://tracker.debian.org/news/707798)
-   pry: [fixed imcompatibility with new ruby-slop](https://tracker.debian.org/news/705748) (RC bug)
-   foodcritic: [fixed test suite run during build](https://tracker.debian.org/news/707794) (RC bug)
-   library updates:
    -   [ruby-grape-logging](https://tracker.debian.org/news/702681)
    -   [ruby-hashie](https://tracker.debian.org/news/702726) (2 RC bugs)
    -   [ruby-listen](https://tracker.debian.org/news/703189): new upstream release, fixed test suite (RC bug)
    -   [ruby-rspec-retry](https://tracker.debian.org/news/703187): new upstrean release, fixed test suite (RC bug)
    -   [ruby-dbf](https://tracker.debian.org/news/703201): new upstream release (sponsored, work by Christopher Baines)
    -   [ruby-bootstrap-sass](https://tracker.debian.org/news/703348): new upstream release + fixed to work on non-Rails apps
    -   [ruby-rails-dom-testing](https://tracker.debian.org/news/705538) (NEW, dependency for rails 4.2)
    -   [ruby-rails-deprecated-sanitizer](https://tracker.debian.org/news/705535) (NEW, dependency for rails 4.2)
    -   [ruby-rmagick](https://tracker.debian.org/news/707821) new upstream release
    -   [ruby-uglifier](https://tracker.debian.org/news/707988) new upstream release
    -   [ruby-cri](https://tracker.debian.org/news/708814) (RC bug)
        -   I was making source+arch:all uploads for a while, but this was my first ever source-only [upload](https://buildd.debian.org/status/logs.php?pkg=ruby-cri&amp;ver=2.7.0-2) of an architecture-independent package to Debian, following the [recent developments](https://lists.debian.org/debian-devel-announce/2015/08/msg00007.html) on the topic.

