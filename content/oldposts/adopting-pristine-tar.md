---
old: true
title: "Adopting pristine-tar"
created_at: "2016-05-22 14:02 -3"
kind: article
tags: [debian, pristine-tar]
---

As of yesterday, I am the new maintainer of [pristine-tar](https://packages.debian.org/pristine-tar). As it is the case for most of [Joey Hess](http://joeyh.name/)’ creations, it is an extremely useful tool, and used in a very large number of Debian packages which are maintained in git.

[My first upload](https://tracker.debian.org/news/769872) was most of a terrain recognition nature: I did some housekeeping tasks, such as making the build idempotent and making sure all binaries are built with security hardening flags, and wrote a few automated test cases to serve as build-time and run-time regression test suite. No functional changes have been made.

As Joey [explained when he orphaned it](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=737871), there are a few technical challenges involved in making sure pristine-tar stays useful in the future. Although I did read some of the code, I am not particularly familiar with the internals yet, and will be more than happy to get co-maintainers. If you are interested, please get in touch. The [source git repository](http://anonscm.debian.org/cgit/collab-maint/pristine-tar.git) is right there.

