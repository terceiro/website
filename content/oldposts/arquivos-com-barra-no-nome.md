---
old: true
title: "arquivos com barra no nome"
created_at: "2009-08-27 17:00 -3"
kind: article
---

Como em todo sistema operacional que merece alguma forma de respeito a barra ("/") é usada como separador de diretórios, normalmente a gente não consegue criar arquivos com uma barra no nome. Mas nesse post eu quero mostrar que **dá** pra ter arquivos com barra no nome:

```
terceiro@morere:/tmp/slash> ls -l
total 0
terceiro@morere:/tmp/slash> touch filename∕with∕slashes
terceiro@morere:/tmp/slash> ls -l
total 0
-rw-r--r-- 1 terceiro terceiro 0 Ago 27 16:59 filename∕with∕slashes
terceiro@morere:/tmp/slash>
```

Qual o truque? Simples, basta usar o caractere Unicode 0x2215 ("DIVISION SLASH") no lugar da barra ASCII normal! Basta abrir o gucharmap ("Mapa de caracteres" nos menus que seguem o padrão freedesktop), procurar por ele no Sistema de escrita "Comum" (lado esquerdo da janela), dar um duplo clique e daí copiar o caracter na caixa de texto no rodapé e colá-lo no seu terminal, ou no seu gerenciador de arquivos desktop, tanto faz.

![Gucharmap-division-slash](/oldposts/images/gucharmap-division-slash.png?1251404161)

 

Aí você me pergunta: qual a utilidade disso? Bom, eu quero rodar um programa pra um conjunto de branches num repositório git e salvar a saída padrão e e a saído de erro  de cada execução em arquivos com o nome do branch, mas esses branches normalmente teem barras no nome. Como eu quero todos os arquivos de resultado no mesmo diretório, então no meu script eu troco as barras ASCII por essa barra especial. Nas fontes em que eu testei os dois caracteres são idênticos visualmente, mas por baixo dos panos eles são diferentes.

