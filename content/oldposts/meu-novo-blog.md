---
old: true
title: "meu novo blog"
created_at: "2009-06-16 19:10 -3"
kind: article
tags: [noosfero]
---

Depois de bastante tempo [usando o TWiki](http://wiki.softwarelivre.org/Blogs/AntonioTerceiro) para o meu blog, estou de mudança para o [Software Livre Brasil](http://softwarelivre.org/terceiro). Apesar do blog no TWiki ser muito legal e prático, eu preciso divulgar mais o [Noosfero](http://www.noosfero.org/), projeto no qual eu venho trabalhando já há 2 anos, e também provar do meu próprio remédio usando o troço no dia-a-dia. Aqui também tem comentários, algo que eu e todo mundo mais que usava blog [lá](http://wiki.softwarelivre.org/Blogs) sentia falta

Você vai notar que todos meus posts antigos estão aqui também. Não, eu não copiei e colei todos eles. :--) Eu usei uma funcionalidade muito interessante do Noosfero, que é a de popular um blog através de um feed RSS ou Atom. Para isso, use a opção "configurar blog" no seu painel de controle, e marque a opção "Obter posts de um feed externo".

![Noosfero-feed-externo](/oldposts/images/noosfero-feed-externo.png?1245189906)

Coloque o *endereço* do feed no campo correpondente. Note que esse não é o endereço do blog, mas sim do seu feed RSS ou Atom! Quem estiver migrando pode deixar marcada a opção "Obter posts apenas uma vez", mas quem quiser que o Noosfero fique acompanhando o blog externo, é só marcar a opção "Obter posts periodicamente".

Pra quem está migrando, uma dica importante: certifique-se de usar um feed que traz **todos** os seus posts, senão apenas os posts que estão *atualmente* no seu feed serão importados (normalmente os últimos 10 ou 20, a depender da plataforma de blog que você usa).

