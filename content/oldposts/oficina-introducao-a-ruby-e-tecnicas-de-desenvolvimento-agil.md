---
old: true
title: "Oficina: Introdução a Ruby e Técnicas de Desenvolvimento Ágil"
created_at: "2009-06-19 00:11 -3"
kind: article
---

![Ruby-logo2](/oldposts/images/ruby-logo2.png?1245381355)Durante o [III Encontro Nordestino de Software Livre / VI Festival Software Livre da Bahia](http://wiki.softwarelivre.org/Festival4) eu fiz essa oficina, cujo objetivo era de "introduzir os principais elementos da linguagem de programação Ruby, bem como de apresentar técnicas relacionadas a Desenvolvimento Ágil de Software, como Desenvolvimento Dirigido por Testes e Refatoração". Ainda que tenha sido bastante focada na prática, eu fiquei de postar o material produzido nela.

Estou bastante atrasado com isso, mas antes tarde do que nunca:

- [oficina-ruby.odp](/oldposts/files/festival4/oficina-ruby.odp): micro-apresentação de slides que serviu mais pra nos guiar durante a oficina do que pra ter conteúdo propriamente dito.

- [oficina-ruby.tar.gz](/oldposts/files/festival4/oficina-ruby.tar.gz): código-fonte produzido: uma implementação bastante simples de planilha eletrônica, com suporte a fórmulas simples como referência, soma, multiplicação e soma de intertvalo de células. Este foi o exemplo usado para praticar TDD, Refatoração e a linguagem em si.

