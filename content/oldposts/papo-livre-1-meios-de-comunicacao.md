---
old: true
title: "Papo Livre #1 - meios de comunicação"
created_at: "2017-06-06 12:46 -3"
kind: article
tags: [papolivre]
---

Acabamos de lançar mais um episódio do Papo Livre: [\#1 – meios de comunicação](https://papolivre.org/1/).

Neste episódio eu, Paulo Santana e Thiago Mendonça discutimos os diversos meios de comunicação em comunidades de software livre. A discussão começa pelos meios mais “antigos”, como IRC e listas de discussão e chega aos mais “modernos”, passo pelo meio livre e meio proprietário Telegram, e chega à mais nova promessa nessa área, Matrix (e o seu cliente mais famoso/viável, Riot).

