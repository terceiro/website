---
old: true
title: "Debian CI updates for September 2016"
created_at: "2016-09-07 22:07 -3"
kind: article
tags: [debian-ci]
---

[debci 1.4](https://tracker.debian.org/news/795502) was released just a few days ago. Among general improvements, I would like to highlight:

-   pretty much every place in the web UI that mentions a PASS or a FAIL also displays the tested package version. This was suggested to me on IRC by [Holger](http://layer-acht.org/thinking/)
-   I also tried to workaround an instability when setting up the LXC containers used for the tests, where the test bed process setup would finish without failure even though some steps in the middle of it failed. This caused the very final step for the debci-specific setup to fail, so there was no `debci` user inside the container, which caused tests to fail because that user was missing. Before that was fixed I was always keeping an eye on this issue, fixing the issue by hand, and re-triggering the affected packages by hand, so as far I can tell there is no package whose status has been permanently affected by this.
-   Last, but not least, this release brings an interesting contribution by Gordon Ball, which is keeping track of different failure states. debci will now let you know whether a currently failing package has always failed, has passed in a previous version, or if the same version that is currently failing has previously passed.

[ci.debian.net](https://ci.debian.net/) has been upgraded to `debci` 1.4 just after that. At the same time I have also upgraded `autodep8` and `autopkgtest` to their latest versions, available in jessie-backports. This means that it is now safe for Debian packages to assume the changes in [autopkgtest 4.0](http://www.piware.de/2016/06/autopkgtest-4-0-simplified-cli-deprecating-adt/) are available, in special the `$AUTOPKGTEST_*` environment variables.

In other news, for several weeks there were had issues with tests not being scheduled when they should have. I was just assuming that the issue was due to the existing test scheduler, `debci-batch`, being broken. Today I was working on a new implementation that is going to be a lot faster, I started to hit a similar issue on my local tests, and finally realized what was wrong. The fact is that `debci-batch` stores the timestamp of the last time a package has been scheduled to run, and it there are no test result after that timestamp, it assumes the package is still in the queue to be tested, and does not schedule it again. It turns out that a few weeks ago, during maintainance work, I had cleared the queue, discarding all jobs that were there, but forgot to reset those timestamps, so when `debci-batch` came around again, it checked the timestamp of the last request and did not make new requests because there was no test result after that timestamp! I cleared all those timestamps, and the system should now go back to normal.

That is it for now. I you want to contribute to the Debian CI project and want to get in touch, you can pop up on the `#debci` channel on the OFTC IRC network, or mail the [autopkgtest-devel](http://lists.alioth.debian.org/cgi-bin/mailman/listinfo/autopkgtest-devel) mailing list.

