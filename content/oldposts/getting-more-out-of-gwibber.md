---
old: true
title: "Getting more out of Gwibber"
created_at: "2010-07-06 10:25 -3"
kind: article
---

Gwibber has frustrated me as a microblog reader. A minor issue I have with it is that sometimes I just can't post/reply through it. I don't know whether it is related to the microblogging services have crashed, which is not Gwibber's fault, but it would be nice if if at least warned the user with a "could not send your post" error message.

But the major issue I have with it is that I cannot see enough back in time: since I definitively do not check it all the time, I often lose posts that were not done in the last 24 hours. I tried to solve this by unfollowing people that post a lot, but it did not solve the problem.

I've had read that the [Twitter API](http://apiwiki.twitter.com/Twitter-API-Documentation) , which is supported by [StatusNet](http://status.net/) at [identi.ca](http://identi.ca/), supports a `count` argument, which indicated how many posts will be provided, and that it defaults to 20.

Then I did what every programmer should do: look at the source code. I looked inside the implementation of identi.ca account support in Gwibber source code, and found out that it already uses this parameter, although there is no user interface for setting it. Every account has a `receive_count` attribute that defines how many posts will be fetched from the servers, and it defaults to 20.

While there is no UI for `receive_count`, you'll need to set this attribute directly in gconf. I used `gconf-editor` for that, but there are a couple of other options out there. Go to apps/gwibber/accounts, find your accounts, and add a new integer key/value pair there, with key "receive\_count", and the value you want to.

 

![setting receive\_count with gconf-editor](/oldposts/images/gwibber-receive-count_display.png?1278422350)

