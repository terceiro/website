---
old: true
title: "Thesis submitted"
created_at: "2012-02-28 21:44 -3"
kind: article
tags: [phd]
---

Last Friday, after 5 long years, I have finally submitted my PhD thesis. It was
quite a relief, more or less as if an elephant was taken off my back.

An English title for my thesis would be *Structural Complexity Characterization
in Software Systems*. Here is an abstract:

> This thesis proposes a theory to characterize structural complexity in
> software systems. This theory aims to identify (i) the contribution of
> several factors to the structural complexity variation and (ii) the
> effects of structural complexity in software projects.
> Possible factors in the structural complexity variation include: human
> factors, such as general experience of the developers and their
> familiarity with the different parts of the system; factors related to
> the changes performed on the system, such as size variation and change
> diffusion; and organizational factors, such as the maturity of the
> software development process and the communication structure of the
> project.
>
> Effects of structural complexity include higher effort, and consequently
> higher cost, in software comprehension and maintenance activities.
> To test the validity of the proposed theory, four empirical studies were
> performed, mining data from free software project repositories. We
> analyzed historical data from changes performed in 13 systems from
> different application domains and written in different programming
> languages.
>
> The results of these studies indicated that all the factors studied
> influenced the structural complexity variation significantly in at least
> one of the projects, but different projects were influenced by different
> sets of factors. The models obtained were capable of describing up to
> 93% of the structural complexity variation in the projects analyzed.
>
> **Keywords:**
>
> Structural Complexity,
> Software Maintainance,
> Human factors in Software Engineering,
> Mining Software Repositories,
> Theories in Software Engineering,
> Empirical Software Engineering,
> Free/Open Source Software Projects.

Those who read Portuguese can check out [details about the thesis](https://people.debian.org/~terceiro/tese.git/).

Most of the studies discussed in the thesis are presented in English in [papers](/publications) I have published during the last years.

My defense is going to be on March 23rd. If you happen to be at Salvador at that day, please feel cordially invited.

