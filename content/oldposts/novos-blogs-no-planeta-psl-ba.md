---
old: true
title: "Novos blogs no Planeta PSL-BA"
created_at: "2010-04-27 13:14 -3"
kind: article
---

Alguns dias atrás lancei uma [chamada de blogs](http://listas.dcc.ufba.br/pipermail/psl-ba/2010-April/020536.html) para o [Planeta PSL-BA](http://planeta.ba.softwarelivre.org/), o agregador de blogs na Bahia ligados a software livre. Temos  13 novos blogs, aos quais eu desejo boas-vindas e espero que possamos compartilhar várias idéias. São eles:

-   [Camila Oliveira](http://camilaoliveira.net/)
-   [Joenio Costa](http://softwarelivre.org/joenio)
-   [José Francisco Teles](http://log.zehh.com.br/)
-   [Jamson Átila](http://jamsux.wordpress.com/)
-   [Fábio Nogueira](http://blog.fnogueira.com.br/)
-   [Cleuber Silva](http://bitinvitro.blogspot.com/)
-   [Pontão JuntaDados](http://juntadados.org/)
-   [Leandro Nunes](http://www.leandronunes.net/)
-   [Gabriel "Pnordico" Menezes](http://pnord.gentoobr.org/)
-   [Ataliba Neto](http://atalibaneto.wordpress.com/)
-   [Live Blue](http://liveblue.wordpress.com/)
-   [Wilson Baião](http://softwarelivre.org/baiao)
-   [Leonardo Couto Conrado](http://conteudoopensource.blogspot.com/)

Se você é baiano e/ou está na Bahia, trabalha com software livre e quer ter o seu blog agregado no Planeta PSL-BA, é só seguir as instruções na [chamada](http://listas.dcc.ufba.br/pipermail/psl-ba/2010-April/020536.html).

**atualização (28/04):** adicionado Leandro Nunes.

**atualizacão (02/05):** adicionado Gabriel "Pnordico" Menezes

**atualização (03/05):** adicionado Ataliba Neto

**atualização (13/05):** adicionado Live Blue

**atualização (14/05):** adicionado Wilson Baião

**atualização (25/05):** adicionado Leonardo Couto Conrado

