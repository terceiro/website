---
old: true
title: "Debian Ruby Sprint 2016 - day 1"
created_at: "2016-02-29 20:44 -3"
kind: article
tags: [debian, ruby]
---

This year’s [Debian Ruby team sprint](https://wiki.debian.org/Teams/Ruby/Meeting/Brazil2016) started today here at Curitiba. Everyone arrived fine, and we started working at the meeting room we have booked for the week at Curitiba campus of the [Federal Technical University of Paraná](http://utfpr.edu.br/ "UTPFR"). The room is at the “Department of Business and Community Relations”, what makes a lot of sense! :-)

![](/oldposts/images/utfpr.jpg)

The day started with a quick setup, with a simple 8-port switch, and a couple of power strips. It tooks us a few minutes to figure what was blocked or not on the corporate network, and almost everyone who needs connections that are usually blocked in such environments already had their VPN setups so we were able to get started right after that.

![](/oldposts/images/ruby-sprint-setup.jpg)

We are taking notes live on [mozilla’s piblic etherpad site](https://public.etherpad-mozilla.org/p/Debian-Ruby-Curitiba-2016)

Today we accomplished quite a lot:

-   analyzed the pending issues for the ruby2.3 transition issues, categorizing the missing packages into “needs a binNMU now”, “needs a binNMU after switching the default to ruby2.3”, and “broken”.
-   ruby-defaults uploaded to experimental switching to ruby2.3 as default, and dropping support for ruby2.2
-   ruby-gsl fixed to build against GSL 2.x (was blocking ruby2.3 transition)
-   \#816253: ruby-fast-gettext: fix FTBFS issue and import a new upstream
-   ruby-aws: several issues fixed
-   ruby-binding-of-caller: fixed rubygems-integration
-   fixed ruby-kakasi-ffi (again)
-   made ruby-blockenspiel arch:any again
-   ruby-fast-gettext 1.0.0-1 (fix ftbfs 816253)
-   ruby-aws-sdk: new upstream, debcheck fix and several bumps
-   ruby-fcgi – dropped transitional packages + refreshed packaging with -w
-   ruby-sshkit 1.8.0
-   ruby-beautify: new upstream and few minor fixes
-   asciidoctor (new version sponsored)
-   capistrano 3.4.0 (ftbfs #795724, #802734)
-   \#816254: ruby-packetfu: FTBFS: expected NameError with “uninitialized constant PacketFu::FakePacket”, got #
-   updated rake to 10.5.0; making it **not** include -I/usr/lib/ruby/vendor_ruby when running tests
-   triaged an closed open bugs on ruby-httpclient that do not apply anymore.
-   updated ruby-httpclient to get rid of warnings in apt-listbugs under Ruby 2.3
-   ruby-packetfu (fix ftbfs #816254) by @kanashiro
-   investigated extension/rdepends build failing with ruby2.3-only
-   applied upstream patch to ruby to fix extension ftbfs when extension uses c++
-   ruby-albino 1.3.3-4 (#813644)
-   basic user-level testing using ruby2.3 as default:
    -   chef WORKS mostly; it seems ohai segfaults some times
    -   rails autopkgtest FIXED `Could not find gem binding_of_caller (>= 0.7.2)’, which is required by gem ‘web-console (~> 2.0)’, in any of the sources.`
    -   vagrant WORKS
    -   redmine autopkgtest WORKS
    -   apt-listbugs FIXED warnings (e.g. try `apt-listbugs list $pkg`); caused by ruby-httpclient
    -   nanoc WORKS
-   capistrano 3.4.0-1 (#795724, #802734) by @sbadia + @terceiro

