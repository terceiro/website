---
old: true
title: "Debian Ruby Sprint 2016 - day 4: Steady Progress, Deferred Spring Cleaning, and Capital Sins"
created_at: "2016-03-04 13:42 -3"
kind: article
tags: [debian, ruby]
---

As the day 4 of the Debian Ruby team sprint in Curitiba unfolded, we have now fixed a total of more than 70 build failure bugs, managed to almost finish the Ruby 2.3 transition to be good to migrate into testing, and bootstrapped some documentation that will help new contributors get up to speed with Ruby packaging faster.

We have also requested the removal of several packages that are either severely outdated, abandoned upstream, beyond repair, utterly wrong, or in some cases, all of the above.

The full list of work items finished yesterday is:

-   filed for RM ruby-rubysl-test-unit (no rdeps, duplicates ruby-test-unit)
-   filed for RM ruby-literati (no rdeps left)
-   uploaded ruby-certificate-authority 0.1.6-2
-   uploaded ruby-pdf-reader 1.4 (Closes: FTBFS \#795763)
-   uploaded ruby-http 1.0.2-1 (Closes: \#795752)
-   raise severity of FTBFS bugs with ruby2.3
-   day 3 report
-   NMU pcs and upload to DELAYED/2 to remove dependency on ruby-monkey-lib and build-dependency on ruby2.1-dev
-   upload ruby-parslet (Closes: \#795046)
-   initial documentation of packaging workflow with updated helper scripts in the team repo – https://wiki.debian.org/Teams/Ruby/Workflow
-   ask for the removal of ruby-rspec-longrun
-   ruby-opengraph-parser – upstream bug; upstream unresponsive; asked uploader about removal (no rdeps)
-   fix ruby-celluloid-io for ruby2.3
-   fixed ruby-buff-extension to work on ruby2.3
-   uploaded newer ruby-varia-model upstream to work with newer ruby-hashie
-   ask for the removal of mdpress
-   uploaded ruby-buff-config, ruby-semverse, ruby-buff-ruby-engine, ruby-buff-ignore, ruby-buff-shell-out to remove spork
-   filed for RM spork (obsolete, broken)
-   filed for RM ruby-gsl on failing archs
-   upload ruby-solve (Closes: \#816359, thanks zeha!)
-   update ruby-standalone to work properly with ruby2.3 (needs to remove the rake binary)
-   upload ruby-memfs
-   ruby-rack (1.6.4-3) (ROM ruby-memcache-client)
-   ruby-parslet 1.7.1-1 \#795046
-   upload ruby-listen
-   upload ruby-clockwork
-   upload ruby-bio with a patch to avoid transient FTBFS because of undefined class names
-   fixed ruby-fakeweb ftbfs
-   cleaned up old repositories on git.d.o
-   investigated PET status (appears to somewhat work, but mostly decaying)
-   uploaded ruby-ogginfo
-   upload ruby-libxml (agan!)
-   upload racc 1.4.14-1
-   upload ruby-sidekiq-cron
-   how-can-i-help updated
-   racc 1.4.14-1 (not ruby-racc ;-))

We also managed to flirt with 2 [capital sins](https://en.wikipedia.org/wiki/Seven_deadly_sins). For those who care about these things, which I don’t (but I still care about you), I guess 2 out of 7 still means we are good? :-)

I few people that I will not name complained that they hadn’t had enough steak on the previous night, so we set out to visit a traditional all-you-can-eat Brazilian steakhouse (“churrascaria”). I made a reservation at [Jardins Grill](http://www.jardinsgrill.com.br/) and there you have gluttony. I am pretty sure that “not enough steak” wasn’t an issue last night. You can see how happy, despite being full to almost the point of being sick, everyone was.

![](/oldposts/images/jardins-grill.jpg)

A disjunct set of people, who I will also not name, were very disappointed to find out that [the ruby-tinder package](https://packages.debian.org/sid/ruby-tinder) has absolutely nothing to do with [Tinder](https://www.gotinder.com/) but were still very active on the later. Maybe Friday night we will have to split the group into a lust-free family party and a Tinder party.

