---
old: true
title: "Chegando no ENECOMP 2009"
created_at: "2009-09-05 18:50 -3"
kind: article
tags: [colivre, noosfero]
---

Estou participando do ENECOMP 2009.

Hoje fiz minha primeira palestra, com o títiulo "Criando um empreendimento para trabalhar com software livre: lições aprendidas na Colivre", onde eu procurei discutir as duas grandes motivações que nos levaram a criar a [Colivre](http://colivre.coop.br/), que são:

1.  Criar um empreendimento para trabalhar com software livre, e fazendo software livre.
2.  Fazer isso de uma forma que fosse, na nossa visão, compatível com os valores do software livre: democracia, colaboração entre pares, entre outros.

Essas vontades explodiram quando um grupo ligado ao movimento do Software Livre, do qual eu fazia parte, encontrou um grupo ligado ao movimento da [Economia Solidária](http://pt.wikipedia.org/wiki/Economia_Solid%C3%A1ria), e daí percebemos que esses movimentos eram as duas faces de uma mesma moeda. Dessa união nasceu a Colivre.

Durante a palestra eu abordei dois temas: quais são as possibilidades de serviços possíveis para trabalhar com software livre; e a discussão sobre a natureza do trabalho, e como (na minha visão) o trabalho cooperativo pode ser considerado uma forma de trabalho livre, enquanto o trabalho assalariado é considerado um trabalho proprietário.

Os [slides da palestra](/oldposts/files/criando-um-empreendimento-para-trabalhar-com-software-livre-licoes-aprendidas-na-colivre.odp) estão disponíveis.

Amanhã eu vou fazer outra palestra, onde vou apresentar o projeto [Noosfero](https://gitlab.com/noosfero/noosfero).

