---
title: Publications
---

# Publications

This is a list of my publications. Unless otherwise noted, they are
available under the [CreativeCommons Attribution-No Derivative Works 3.0
Unported](http://creativecommons.org/licenses/by-nd/3.0/) license. My full
academic CV, including but not limited to publications, is available at the
[Lattes platform](http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id=K4778915Z2).


## 2020

- WEN, M. ; SIQUEIRA, R. ; LAGO, N. P. ; CAMARINHA, D. ; TERCEIRO, A. ; KON,
  FABIO ; MEIRELLES, Paulo . **Leading successful government-academia
  collaborations using FLOSS and agile values**. JOURNAL OF SYSTEMS AND
  SOFTWARE, p. 110548, 2020.
  [DOI: 10.1016/j.jss.2020.110548](http://dx.doi.org/10.1016/j.jss.2020.110548)

## 2018

- Kanashiro, L., Ribeiro, A., Silva, D., Meirelles, P. and Terceiro, A. (2018).
  **Predicting Software Flaws with Low Complexity Models based on Static Analysis
  Data**. Journal of Information Systems Engineering & Management , 3(2), 17.
  [DOI: 10.20897/jisem.201817](https://doi.org/10.20897/jisem.201817)

## 2017

- SILVA, G. ; REIS, L. ; TERCEIRO, A. ; MEIRELLES, Paulo ; KON, F. .
  **Implementing Federated Social Networking: Report from the Trenches**.  In:
  International Symposium on Open Collaboration, 2017, Galway, Ireland.
  Proceedings of the 13th International Symposium on Open Collaboration
  (OpenSym). New York: ACM-Published, 2017.
- MEIRELLES, Paulo; WEN, M. ; TERCEIRO, A. ; SIQUEIRA, R. ; Kanashiro, L. ;
  NERI, H. R. . **Brazilian Public Software Portal: an integrated platform for
  collaborative development**. In: International Conference on Open Source
  Systems, 2017, Galway, Ireland.  Proceedings of the 13th International
  Symposium on Open Collaboration (OpenSym), 2017.
- Kanashiro, L. ; RIBEIRO, A. C. ; SILVA, D. C. A. ; MEIRELLES, Paulo ;
  TERCEIRO, ANTONIO. **A Study on Low Complexity Models to Predict Flaws in the
  Linux Source Code**. In: The 12th Iberian Conference on Information Systems and
  Technologies, 2017, Lisboa. Proceedings of the 12th Iberian Conference on
  Information Systems and Technologies.  Singapore: Singapore: Research
  Publishing Services, 2017. p.  972-977.

## 2016

- Terceiro, A. 2016. **Patterns for Writing As-Installed Tests for Debian
  Packages**. HILLSIDE Proc. of Latin American Conf. on Pattern Lang. of Prog. 11
  (November 2016), 15 pages. jn 2, 3, Article 1 (November 2016), 15 pages.
  [[PDF](https://gitlab.com/terceiro/installed-tests-patterns/raw/pdf/final/installed-tests-patterns.pdf)]

## 2015


- Antonio Terceiro, Paulo Meirelles, Christina Chavez. **"Práticas de Controle
  de Qualidade em Projetos de Software Livre"**. Computação Brasil, n. 27, 2015. p12
  \[[PDF](http://sbc.org.br/images/flippingbook/computacaobrasil/computa_27/02-2015_18.06.pdf#page=12)\]
- Antonio Terceiro, Rodrigo Maia, Paulo Meirelles. **"Software Público
  Brasileiro: de portal para plataformaintegrada de colaboração"**. Computação
  Brasil, n. 27, 2015. p25.
  \[[PDF](http://sbc.org.br/images/flippingbook/computacaobrasil/computa_27/02-2015_18.06.pdf#page=25)\]
- Filipe de O. Saraiva , Antonio Terceiro. **"An Open Access Repository for the
  Workshop de Software Livre (WSL)"**. In: Workshop de Sotware Livre, 2015.
  \[[PDF](http://wsl.softwarelivre.org/2015/0010/an-open-access-repositor-for-the-workshop-de-software-livre-WSL-wsl-2015.pdf)\]

2012
----

- Antonio Terceiro, Rodrigo Souza, Christina Chavez. **"Free Software
  Patterns"**. In: SugarLoafPlop Latin American Conference on Pattern Languages
  of Programming, 2012, Natal
  \[[PDF](https://gitlab.com/flosspapers/free-software-patterns/-/blob/blob/fsp2012.pdf)\]
- Antonio Terceiro, Manoel Mendonça, Christina Chavez, Daniela S.  Cruzes.
  **"Understanding Structural Complexity Evolution: a Quantitative Analysis"**.
  In: 16th European Conference on Software Maintenance and Reengineering, 2012.
  \[[PDF](https://gitlab.com/flosspapers/sc-factors/-/blob/pdf/csmr2012-terceiro.pdf)\]

## 2011

- Fabio Kon, Paulo Meirelles, Antonio Terceiro, Christina Chavez, Nelson Lago,
  Manoel Mendonça. **"Free and Open Source Software Development and Research:
  Opportunities for Software Engineering"**.  In: Brazilian Symposium on
  Software Engineeering - SBES, 2011.
  \[[PDF](https://gitlab.com/flosspapers/cbsoft2011-sbes25/-/blob/pdf/sbes2011.pdf)\]
- Christina Chavez, Antonio Terceiro, Paulo Meirelles, Fabio Kon, Carlos Santos
  Jr. **"Free/Libre/Open Source Software Development in Software Engineering
  Education: Opportunities and Experiences"**.  In: IV Fórum de Educação em
  Engenharia de Software, SBES 2011.
  \[[PDF](https://gitlab.com/flosspapers/cbsoft2011-fees/-/blob/pdf/final.pdf)\]

## 2010

- Paulo Meirelles, Carlos Santos Jr., Antonio Terceiro, João Miranda, Christina
  Chavez e Fabio Kon. "**A Study of the Relationships between Source Code
  Metrics and Attractiveness in Free Software Projects**". In: Brazilian
  Symposium on Software Engineeering - SBES, 2010, Salvador. Proceedings of the
  24th Brazilian Symposium on Software Engineeering, 2010.
  \[[PDF](https://gitlab.com/flosspapers/cbsoft2010-sbes/-/blob/master/sourcecode_attractiveness.pdf)\]
- Antonio Terceiro, Luiz Romário Rios e Christina Chavez. "**An Empirical Study
  on the Structural Complexity Introduced by Core and Peripheral Developers in
  Free Software Projects**". In: Brazilian Symposium on Software Engineeering -
  SBES, 2010, Salvador.  Proceedings of the 24th Brazilian Symposium on
  Software Engineeering, 2010.
  \[[PDF](sbes2010-terceiro.pdf)\]\[[slides](sbes2010-terceiro-slides.pdf)\]
- Antonio Terceiro , Joenio Costa , João Miranda, Paulo Meirelles, Luiz Romário
  Rios, Lucianna Almeida, Christina Chavez, and Fabio Kon; "**Analizo: an
  Extensible Multi-Language Source Code Analysis and Visualization Toolkit**".
  Paper published in the Tools Session of the
  [1st Brazilian Conference on Software](http://wiki.dcc.ufba.br/CBSOFT/WebHome),
  September 2010.
  \[[PDF](http://www.analizo.org/publications/analizo-cbsoft2010-tools.pdf)\]\[[slides](http://www.analizo.org/publications/analizo-cbsoft2010-tools-slides.pdf)\]
- Antonio Terceiro, "**Developers Contribution to Structural Complexity in Free
  Software projects**," in Proceedings of the OSS 2010 Doctoral Consortium, W.
  Scacchi, K. Ven, and J. Verelst, Eds., 2010.
  \[[PDF](dcoss2010-terceiro.pdf)\]\[[slides](dcoss2010-terceiro-slides.pdf)\]

## 2009

- Antonio Terceiro and Christina Chavez, "**Structural Complexity Evolution in
  Free Software Projects: A Case Study**", in QACOS-OSSPL 2009: Proceedings of
  the Joint Workshop on Quality and Architectural Concerns in Open Source
  Software (QACOS) and Open Source Software and Product Lines (OSSPL), M. Ali
  Babar, B. Lundell, and F. van der Linden, Eds., 2009.
  \[[PDF](qacos2009-terceiro.pdf)\]

## 2007

- Cason, D., Rocha, R., TERCEIRO, A. S. A., Barbosa, A., Ramos, E., Galiza, H.
  **Gerenciamento automático de usuários de uma rede acadêmica**. In: Workshop
  Software Livre, 2007, Porto Alegre/RS/Brasil. Anais do Fórum Internacional
  Software Livre, 2007.

## 2004

- TERCEIRO, A. S. A. . **T++, um Mecanismo De Execução para Aplicações Web em
  C++**. In: Workshop de Trabalhos de Iniciação Científica e Graduação
  Bahia/Sergipe, 2004, Feira de Santana/Bahia. Anais do WTICG-BASE 2004,
  p.1-13.

## 2003

- TERCEIRO, A. S. A. . **T++: A Tool for Web Application Development with C++**.
  In: ACM SIGPLAN Student Research Competition and Demo Session at OOPSLA 2003,
  Anaheim/Califórnia. OOPSLA 2003 Companion, 2003.

- TERCEIRO, A. S. A. ; CHAVEZ, Christina. **The T++ Approach to Web Application
  Development**. In: Simpósio Brasileiro de Sistemas Multimídia e Web - WEBMIDIA
  2003, 2003, Salvador/Bahia. Anais do Simpósio Brasileiro de Sistemas
  Multimídia e Web - WEBMIDIA 2003, 2003. p. 551- 554.
