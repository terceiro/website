def feed_articles(lang = nil)
  lang ||= config[:language]
  sorted_articles.reject { |a| (a[:language] && a[:language] != lang) }.first(5)
end
