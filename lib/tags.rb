class Tags < Nanoc::DataSource

  identifier :tags

  MAX_WEIGHT = 4
  MIN_WEIGHT = 1

  def weight(i, min, max)
    if i > min
      ((MAX_WEIGHT * (i - min)).to_f / (max - min).to_f).ceil
    else
      MIN_WEIGHT
    end
  end

  def items
    tags = Hash.new(0)
    min = 0
    max = 0
    Dir.glob('content/*posts/*.md').each do |article|
      data = YAML.load_file(article)
      Array(data['tags']).each do |tag|
        tags[tag] += 1
        min = [tags[tag], min].min
        max = [tags[tag], max].max
      end
    end
    tags.map do |tag, count|
      new_item(
        File.read('layouts/tag.html.erb'),
        {
          kind: 'tag',
          tag: tag,
          count: count,
          weight: weight(count, min, max),
          title: "Posts tagged with <u>#{tag}</u>",
          feed_path: "/tag/#{tag}/feed.xml",
        },
        Nanoc::Identifier.new("/tag/#{tag}.html.erb")
      )
    end + tags.map do |tag, count|
      new_item(
        File.read('layouts/tag_feed.xml.erb'),
        {
          kind: 'tag-feed',
          tag: tag,
          count: count,
          title: "Posts tagged with \"#{tag}\"",
        },
        Nanoc::Identifier.new("/tag/#{tag}/feed.xml.erb")
      )
    end
  end

end

def tags
  @items.select { |item| item[:kind] == 'tag' }
end

def tag_feed(tag)
  items_with_tag(tag[:tag]).sort_by { |item| item[:created_at] }.reverse
end
