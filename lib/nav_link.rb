def active_link?(target)
  target === @item.path
end

def active(target)
  active_link?(target) && 'active' || ''
end

def nav_link(text, target)
  classes = ['nav-item']
  if active_link?(target)
    classes << 'active'
  end
  [
    "<li class='#{classes.join(' ')}'>",
    link_to(text, target, class: 'nav-link'),
    "</li>",
  ].join
end
