include Nanoc::Helpers::Blogging
include Nanoc::Helpers::LinkTo
include Nanoc::Helpers::Rendering
include Nanoc::Helpers::Tagging
include Nanoc::Helpers::Text

def post_date(post)
	Time.parse(post[:created_at]).strftime('%B %d, %Y')
end
