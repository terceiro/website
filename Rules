#!/usr/bin/env ruby

ignore '/vendor/**/*.scss'
ignore '/vendor/**/*.txt'
ignore '/vendor/**/*.md'

compile '/**/*.md' do
  filter :rdiscount
  filter :colorize_syntax, default_colorizer: :rouge
  layout '/default.*'
  filter :relativize_paths, type: :html
  if item[:kind] == 'article'
    date = Time.parse(item.attributes[:created_at]).strftime('%Y/%m/%d')
    slug = File.basename(@item.identifier.without_ext)
    write '/%s/%s/index.html' % [date, slug]
  end
end

compile '/**/*.scss' do
  filter :sass, syntax: :scss
  write @item.identifier.without_ext + '.css'
end

compile '/**/*.xml.erb' do
  filter :erb
  write @item.identifier.without_ext
end

compile '/**/*.html.erb' do
  filter :erb
  filter :relativize_paths, type: :html
  layout '/default.*'
end

route '/**/*.{html,md,html.erb}' do
  if item.identifier =~ '/index.*'
    '/index.html'
  else
    item.identifier.to_s.sub(%r{\.(html|md|html\.erb)$}, '') + '/index.html'
  end
end

compile '/**/*' do
  write item.identifier.to_s
end

layout '/**/*', :erb
